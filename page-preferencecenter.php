<?php
/**
 * Template Name: Preference Center
 * The template for the preference center
 *
 * @package WaterBrook Multnomah
 */

get_header();
 ?>
<?php
$guid = $_GET['guid'];
$request = new RHAjax;
$preferences = $request->get_email_preferences( $guid );
$programs = $preferences->Programs;
$active_programs = array();
$preferences = $preferences->Subscriber;
foreach( $programs as $program ) {
  if( $program->Active === 1) {
    array_push( $active_programs, $program->ProgramId);
  }
}
?>
  <div class="container bg" id="pc-wrapper">
  <main class="main preference-center">
  <h3>Preference Center</h3>
    <p>Please update your newsletter preferences</p>
    <hr>
    
    <form name="preference" class="form form-stacked form-preferences">
      <div class="row grid">
        <div class="span_6 form-group">
          <label class="strong" for="Email">Email Address<span class="red">*</span></label>
          <input type="email" class="form-control js-email" id="Email" placeholder="Email" 
                  <?php if($preferences->Email) { echo 'value="' . $preferences->Email . '"'; } ?>>
        </div>
      </div>

      <div class="row grid">
        <div class="span_6 form-group">
          <label class="strong" for="FirstName">First Name</label>
          <input type="text" class="form-control js-field" id="FirstName" placeholder="First Name" <?php if($preferences->FirstName) { echo 'value="' . $preferences->FirstName . '"'; } ?>>
        </div>
        <div class="span_6 form-group">
          <label class="strong" for="LastName">First Name</label>
          <input type="text" class="form-control js-field" id="LastName" placeholder="Last Name" <?php if($preferences->LastName) { echo 'value="' . $preferences->LastName . '"'; } ?>>
        </div>
      </div>
      <p class="small"><span class="red">*</span> indicates required field</p>

      <hr>

      <h3>Newsletters</h3>
      <div class="row" id="newsletters">
          <label class="sub">
              <input class="js-sub" type="checkbox" name="newsletters[]" value="50401" 
                          <?php if( in_array( 50401, $active_programs ) ) { echo 'checked'; } ?> >
                          <strong>WaterBrook Multnomah</strong>
                          <p>General News and Updates</p>
              
          </label>
          <label class="sub">
              <input class="js-sub" type="checkbox" name="newsletters[]" value="50402" 
                          <?php if( in_array( 50402, $active_programs ) ) { echo 'checked'; } ?> >                          <strong>Books for the Journey</strong>
                          <p>Monthly newsletter featuring Christian Living, best-selling, devotionals, and other Christian non-fiction titles.</p>
              
          </label>
          <label class="sub">
              <input class="js-sub" type="checkbox" name="newsletters[]" value="50403" 
                          <?php if( in_array( 50403, $active_programs ) ) { echo 'checked'; } ?> >                          <strong>Church Connections</strong>
                          <p>Monthly newsletter featuring new and best-selling church leadership, small group resources, curriculum, DVDs, and books appropriate for church-wide campaigns</p>
              
          </label>
          <label class="sub">
              <input class="js-sub" type="checkbox" name="newsletters[]" value="50404" 
                          <?php if( in_array( 50404, $active_programs ) ) { echo 'checked'; } ?> >                          <strong>Fiction Reads</strong>
                          <p>Quarterly newsletter featuring new and best-selling Christian Fiction</p>
              
          </label>
          <label>
              <input id="unsubscribe" type="checkbox" name="unsubscribe" />
                          <strong>Unsubscribe from all emails</strong>
              
          </label>
      </div>

      
      <input type="hidden" name="guid" value="<?php echo $guid; ?>">
      <input type="hidden" name="nonce" value="<?php echo wp_create_nonce('wbm-preferences-signup'); ?>">
      <input type="hidden" name="ajaxurl" value="http://<?php echo $_SERVER['SERVER_NAME']; ?>/wp-admin/admin-ajax.php">
      <button id="submit" type="submit" class="btn btn-default">Submit</button>


    </form>

    <div class="alert alert-danger js-error" style="display:none;"></div>
    <div class="alert alert-success js-success" style="display:none;"><i class="fa fa-check"></i>Thanks! Your Preferences have been updated.</div>

    </main>
    
  </div><!-- /.container -->

 <?php get_footer(); ?>
