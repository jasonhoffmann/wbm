(function($) {

$( "#invserchbtn" ).click(function() {
  $( ".search-field" ).toggle( "fast", function() {
    $( "#invserchbtn" ).hide();
  });
});

$(document).on('ready', function(e) {
	var hash = window.location.hash;
	if( hash === '#opn' && $('main').hasClass('book') ) {
		var firstDropdown = $('.book-formats').find('.dropdown-toggle').first();
		firstDropdown.trigger('click');
	}
	
	if( hash === '#opnebook' && $('main').hasClass('book') ) {
		var ebookDropdown = $('.book-formats').find('[data-format="ebook"]').first();
		ebookDropdown.trigger('click');
	}
})

})(jQuery);