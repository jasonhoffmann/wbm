/*!
 * Bootstrap v3.3.5 (http://getbootstrap.com) - DROPDOWN + TABS
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 */

/*!
 * Generated using the Bootstrap Customizer (http://getbootstrap.com/customize/?id=d3cbacd29a9a71f9d7d5)
 * Config saved to config.json and https://gist.github.com/d3cbacd29a9a71f9d7d5
 */
if("undefined"==typeof jQuery)throw new Error("Bootstrap's JavaScript requires jQuery");+function(t){"use strict";var e=t.fn.jquery.split(" ")[0].split(".");if(e[0]<2&&e[1]<9||1==e[0]&&9==e[1]&&e[2]<1)throw new Error("Bootstrap's JavaScript requires jQuery version 1.9.1 or higher")}(jQuery),+function(t){"use strict";function e(e){var a=e.attr("data-target");a||(a=e.attr("href"),a=a&&/#[A-Za-z]/.test(a)&&a.replace(/.*(?=#[^\s]*$)/,""));var n=a&&t(a);return n&&n.length?n:e.parent()}function a(a){a&&3===a.which||(t(r).remove(),t(i).each(function(){var n=t(this),r=e(n),i={relatedTarget:this};r.hasClass("open")&&(a&&"click"==a.type&&/input|textarea/i.test(a.target.tagName)&&t.contains(r[0],a.target)||(r.trigger(a=t.Event("hide.bs.dropdown",i)),a.isDefaultPrevented()||(n.attr("aria-expanded","false"),r.removeClass("open").trigger("hidden.bs.dropdown",i))))}))}function n(e){return this.each(function(){var a=t(this),n=a.data("bs.dropdown");n||a.data("bs.dropdown",n=new o(this)),"string"==typeof e&&n[e].call(a)})}var r=".dropdown-backdrop",i='[data-toggle="dropdown"]',o=function(e){t(e).on("click.bs.dropdown",this.toggle)};o.VERSION="3.3.5",o.prototype.toggle=function(n){var r=t(this);if(!r.is(".disabled, :disabled")){var i=e(r),o=i.hasClass("open");if(a(),!o){"ontouchstart"in document.documentElement&&!i.closest(".navbar-nav").length&&t(document.createElement("div")).addClass("dropdown-backdrop").insertAfter(t(this)).on("click",a);var s={relatedTarget:this};if(i.trigger(n=t.Event("show.bs.dropdown",s)),n.isDefaultPrevented())return;r.trigger("focus").attr("aria-expanded","true"),i.toggleClass("open").trigger("shown.bs.dropdown",s)}return!1}},o.prototype.keydown=function(a){if(/(38|40|27|32)/.test(a.which)&&!/input|textarea/i.test(a.target.tagName)){var n=t(this);if(a.preventDefault(),a.stopPropagation(),!n.is(".disabled, :disabled")){var r=e(n),o=r.hasClass("open");if(!o&&27!=a.which||o&&27==a.which)return 27==a.which&&r.find(i).trigger("focus"),n.trigger("click");var s=" li:not(.disabled):visible a",d=r.find(".dropdown-menu"+s);if(d.length){var l=d.index(a.target);38==a.which&&l>0&&l--,40==a.which&&l<d.length-1&&l++,~l||(l=0),d.eq(l).trigger("focus")}}}};var s=t.fn.dropdown;t.fn.dropdown=n,t.fn.dropdown.Constructor=o,t.fn.dropdown.noConflict=function(){return t.fn.dropdown=s,this},t(document).on("click.bs.dropdown.data-api",a).on("click.bs.dropdown.data-api",".dropdown form",function(t){t.stopPropagation()}).on("click.bs.dropdown.data-api",i,o.prototype.toggle).on("keydown.bs.dropdown.data-api",i,o.prototype.keydown).on("keydown.bs.dropdown.data-api",".dropdown-menu",o.prototype.keydown)}(jQuery),+function(t){"use strict";function e(e){return this.each(function(){var n=t(this),r=n.data("bs.tab");r||n.data("bs.tab",r=new a(this)),"string"==typeof e&&r[e]()})}var a=function(e){this.element=t(e)};a.VERSION="3.3.5",a.TRANSITION_DURATION=150,a.prototype.show=function(){var e=this.element,a=e.closest("ul:not(.dropdown-menu)"),n=e.data("target");if(n||(n=e.attr("href"),n=n&&n.replace(/.*(?=#[^\s]*$)/,"")),!e.parent("li").hasClass("active")){var r=a.find(".active:last a"),i=t.Event("hide.bs.tab",{relatedTarget:e[0]}),o=t.Event("show.bs.tab",{relatedTarget:r[0]});if(r.trigger(i),e.trigger(o),!o.isDefaultPrevented()&&!i.isDefaultPrevented()){var s=t(n);this.activate(e.closest("li"),a),this.activate(s,s.parent(),function(){r.trigger({type:"hidden.bs.tab",relatedTarget:e[0]}),e.trigger({type:"shown.bs.tab",relatedTarget:r[0]})})}}},a.prototype.activate=function(e,n,r){function i(){o.removeClass("active").find("> .dropdown-menu > .active").removeClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded",!1),e.addClass("active").find('[data-toggle="tab"]').attr("aria-expanded",!0),s?(e[0].offsetWidth,e.addClass("in")):e.removeClass("fade"),e.parent(".dropdown-menu").length&&e.closest("li.dropdown").addClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded",!0),r&&r()}var o=n.find("> .active"),s=r&&t.support.transition&&(o.length&&o.hasClass("fade")||!!n.find("> .fade").length);o.length&&s?o.one("bsTransitionEnd",i).emulateTransitionEnd(a.TRANSITION_DURATION):i(),o.removeClass("in")};var n=t.fn.tab;t.fn.tab=e,t.fn.tab.Constructor=a,t.fn.tab.noConflict=function(){return t.fn.tab=n,this};var r=function(a){a.preventDefault(),e.call(t(this),"show")};t(document).on("click.bs.tab.data-api",'[data-toggle="tab"]',r).on("click.bs.tab.data-api",'[data-toggle="pill"]',r)}(jQuery);

/*
    A simple jQuery modal (http://github.com/kylefox/jquery-modal)
    Version 0.5.8
*/
!function(a){var b=null;a.modal=function(c,d){a.modal.close();var e,f;if(this.$body=a("body"),this.options=a.extend({},a.modal.defaults,d),this.options.doFade=!isNaN(parseInt(this.options.fadeDuration,10)),c.is("a"))if(f=c.attr("href"),/^#/.test(f)){if(this.$elm=a(f),1!==this.$elm.length)return null;this.open()}else this.$elm=a("<div>"),this.$body.append(this.$elm),e=function(a,b){b.elm.remove()},this.showSpinner(),c.trigger(a.modal.AJAX_SEND),a.get(f).done(function(d){b&&(c.trigger(a.modal.AJAX_SUCCESS),b.$elm.empty().append(d).on(a.modal.CLOSE,e),b.hideSpinner(),b.open(),c.trigger(a.modal.AJAX_COMPLETE))}).fail(function(){c.trigger(a.modal.AJAX_FAIL),b.hideSpinner(),c.trigger(a.modal.AJAX_COMPLETE)});else this.$elm=c,this.open()},a.modal.prototype={constructor:a.modal,open:function(){var b=this;this.options.doFade?(this.block(),setTimeout(function(){b.show()},this.options.fadeDuration*this.options.fadeDelay)):(this.block(),this.show()),this.options.escapeClose&&a(document).on("keydown.modal",function(b){27==b.which&&a.modal.close()}),this.options.clickClose&&this.blocker.click(a.modal.close)},close:function(){this.unblock(),this.hide(),a(document).off("keydown.modal")},block:function(){var b=this.options.doFade?0:this.options.opacity;this.$elm.trigger(a.modal.BEFORE_BLOCK,[this._ctx()]),this.blocker=a('<div class="jquery-modal blocker"></div>').css({top:0,right:0,bottom:0,left:0,width:"100%",height:"100%",position:"fixed",zIndex:this.options.zIndex,background:this.options.overlay,opacity:b}),this.$body.append(this.blocker),this.options.doFade&&this.blocker.animate({opacity:this.options.opacity},this.options.fadeDuration),this.$elm.trigger(a.modal.BLOCK,[this._ctx()])},unblock:function(){this.options.doFade?this.blocker.fadeOut(this.options.fadeDuration,function(){a(this).remove()}):this.blocker.remove()},show:function(){this.$elm.trigger(a.modal.BEFORE_OPEN,[this._ctx()]),this.options.showClose&&(this.closeButton=a('<a href="#close-modal" rel="modal:close" class="close-modal '+this.options.closeClass+'">'+this.options.closeText+"</a>"),this.$elm.append(this.closeButton)),this.$elm.addClass(this.options.modalClass+" current"),this.center(),this.options.doFade?this.$elm.fadeIn(this.options.fadeDuration):this.$elm.show(),this.$elm.trigger(a.modal.OPEN,[this._ctx()])},hide:function(){this.$elm.trigger(a.modal.BEFORE_CLOSE,[this._ctx()]),this.closeButton&&this.closeButton.remove(),this.$elm.removeClass("current"),this.options.doFade?this.$elm.fadeOut(this.options.fadeDuration):this.$elm.hide(),this.$elm.trigger(a.modal.CLOSE,[this._ctx()])},showSpinner:function(){this.options.showSpinner&&(this.spinner=this.spinner||a('<div class="'+this.options.modalClass+'-spinner"></div>').append(this.options.spinnerHtml),this.$body.append(this.spinner),this.spinner.show())},hideSpinner:function(){this.spinner&&this.spinner.remove()},center:function(){this.$elm.css({position:"fixed",top:"50%",left:"50%",marginTop:-(this.$elm.outerHeight()/2),marginLeft:-(this.$elm.outerWidth()/2),zIndex:this.options.zIndex+1})},_ctx:function(){return{elm:this.$elm,blocker:this.blocker,options:this.options}}},a.modal.prototype.resize=a.modal.prototype.center,a.modal.close=function(a){if(b){a&&a.preventDefault(),b.close();var c=b.$elm;return b=null,c}},a.modal.resize=function(){b&&b.resize()},a.modal.isActive=function(){return b?!0:!1},a.modal.defaults={overlay:"#000",opacity:.75,zIndex:1,escapeClose:!0,clickClose:!0,closeText:"Close",closeClass:"",modalClass:"modal",spinnerHtml:null,showSpinner:!0,showClose:!0,fadeDuration:null,fadeDelay:1},a.modal.BEFORE_BLOCK="modal:before-block",a.modal.BLOCK="modal:block",a.modal.BEFORE_OPEN="modal:before-open",a.modal.OPEN="modal:open",a.modal.BEFORE_CLOSE="modal:before-close",a.modal.CLOSE="modal:close",a.modal.AJAX_SEND="modal:ajax:send",a.modal.AJAX_SUCCESS="modal:ajax:success",a.modal.AJAX_FAIL="modal:ajax:fail",a.modal.AJAX_COMPLETE="modal:ajax:complete",a.fn.modal=function(c){return 1===this.length&&(b=new a.modal(this,c)),this},a(document).on("click.modal",'a[rel="modal:close"]',a.modal.close),a(document).on("click.modal",'a[rel="modal:open"]',function(b){b.preventDefault(),a(this).modal()})}(jQuery);

(function($) {

	function openPopup(e, href) {
		var top = (screen.availHeight - 500) / 2;
		var left = (screen.availWidth - 500) / 2;

		var popup = window.open(
			href,
			'social',
			'width=550,height=420,left='+ left +',top='+ top +',location=0,menubar=0,toolbar=0,status=0,scrollbars=1,resizable=1'
		);

		if(popup) {
			popup.focus();
			e.preventDefault();
			return false;
		}

		return true;
	}

	$('.js-menu').on('click', function(e) {
		if($('.js-wrapper').hasClass('m-show')) {
			$('.js-wrapper').removeClass('m-show');
		} else {
			$('.js-wrapper').addClass('m-show');
		}
	});

	$('.js-excerpt').on('click', function(e) {
		e.preventDefault();
		window.open($(this).attr('href'), 'Excerpt', 'width=600,height=800');
	});

	$('.js-social').on('click', function(e) {
		var href = $(this).attr('href');
		openPopup(e, href);
	});

	$('#showmore-nr').toggle(
	  function() {
	    $( '#new-releases .book-list.m-new-releases' ).addClass( "selected" );
	    $( '#showmore-nr #more' ).css( 'display', 'none' );
	    $( '#showmore-nr #less' ).css( 'display', 'inline' );
	  }, function() {
	    $( '#new-releases .book-list.m-new-releases' ).removeClass( "selected" );
	    $( '#showmore-nr #more' ).css( 'display', 'inline' );
	    $( '#showmore-nr #less' ).css( 'display', 'none' );
	  }
	);

	$('#showmore-bs').toggle(
	  function() {
	    $( '#best-sellers .book-list.m-new-releases' ).addClass( "selected" );
	    $( '#showmore-bs #more' ).css( 'display', 'none' );
	    $( '#showmore-bs #less' ).css( 'display', 'inline' );
	  }, function() {
	    $( '#best-sellers .book-list.m-new-releases' ).removeClass( "selected" );
	    $( '#showmore-bs #more' ).css( 'display', 'inline' );
	    $( '#showmore-bs #less' ).css( 'display', 'none' );
	  }
	);

	$('#showmore-cs').toggle(
	  function() {
	    $( '#coming-soon .book-list.m-new-releases' ).addClass( "selected" );
	    $( '#showmore-cs #more' ).css( 'display', 'none' );
	    $( '#showmore-cs #less' ).css( 'display', 'inline' );
	  }, function() {
	    $( '#coming-soon .book-list.m-new-releases' ).removeClass( "selected" );
	    $( '#showmore-cs #more' ).css( 'display', 'inline' );
	    $( '#showmore-cs #less' ).css( 'display', 'none' );
	  }
	);

	function getQueryString(sParam){
	    var sPageURL = window.location.search.substring(1);
	    var sURLVariables = sPageURL.split('&');
	    for (var i = 0; i < sURLVariables.length; i++){
	        var sParameterName = sURLVariables[i].split('=');
	        if (sParameterName[0] == sParam)
	        {
	            return sParameterName[1];
	        }
	    }
	}


	// Function to activate the tab
	function activateTab() {
		if(window.location.hash) {
			var activeTab = $('[href="' + window.location.hash.replace('/', '') + '"]');
			activeTab && activeTab.tab('show');

		}
	}

	// Change hash when a tab changes
	// Handle shown event using delegation
	$('#tab-hash a').on('click', function (e) {
		var tab = $(this).attr('href').replace('#', '');
	    window.location.hash = tab;

	});

	if(document.getElementById("tab-hash")!=null){
		activateTab();
		setTimeout(function() {
		   window.scrollTo(0, 0);
		 }, 50);

		$(window).on('hashchange', function() {
		    activateTab();
		});
	}

	function setPagination() {
		$('.js-facetpage').on('click', function(e) {
			e.preventDefault();
			var val = $(this).attr('id');
			facetsPlugin.facets("set",
				'URLParams', [ {
						name: "action",
						value: "wbm_facets"
					},
					{
						name: 'search',
						value: searchTerm
					},
					{
						name: 'page',
						value: val
					}
					]);
			facetsPlugin.facets('ajaxReq');
			// $("html, body").animate({ scrollTop: '0' });
		});
	}

	if(document.getElementById("searchCont")!=null){
		var searchTerm;

		var facetsPlugin = $("form#facets").facets({
			ajaxURL : WP_VARS.ajax_url,
			preAJAX : function() {
				searchTerm = $('input#searchTerm').val();
				$('#searchCont').addClass('is-loading');
				return true;
			},
			postAJAX : function(data) {
				$('#searchCont').html(data);
				$('#searchCont').removeClass('is-loading');
				setPagination();
			},
			URLParams : [ {
						name: "action",
						value: "wbm_facets"
						},
						{
							name: 'search',
							value: searchTerm
						}
						]
		});
	}

	var opened;
	$('#toggle-nav').on('click', function(e) {
		e.preventDefault();
		if(!opened) {
			$('#nav').addClass('nav-open');
			opened = true;
		} else {
			$('#nav').removeClass('nav-open');
			opened = false;
		}

	})
})(jQuery);

// var nav = responsiveNav(".js-nav");
