(function($) {

	var Events = {
		el: {
			retailerLink : $('.js-retailer'),
			excerptLink : $('.js-excerpt'),
			socialShare : $('.js-social')
		},

		init: function() {
			this.bindUIActions();
		},

		bindUIActions: function() {
			Events.el.retailerLink.on('click', Events.sendRetailerEvent)
		},

		sendRetailerEvent: function(e) {
			var retailer = $(this).html();
		//utag.link({ "event_type":"affiliate_click", "affiliate_name" : retailer });

		}
	}

	Events.init();
})(jQuery);