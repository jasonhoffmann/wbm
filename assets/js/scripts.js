/**
 * Test for Inline SVG support
 */
function supportsSvg() {
    var div = document.createElement('div');
    div.innerHTML = '<svg/>';
    return (div.firstChild && div.firstChild.namespaceURI) == 'http://www.w3.org/2000/svg';
};

/**
 * iconsFallback - Transform SVGs to PNGs when not supported
 * 
 * @param  string - excludeClass - Name of exclusion class
 */
function iconsFallback(excludeClass) {

    // If browser doesn't support Inline SVG
    if ( !supportsSvg() ) {
        console.log('unsupported');

        // Get all SVGs on the page and how many there are
        var svgs = document.getElementsByTagName("svg"),
            svgL = svgs.length;

        // Loop through all SVGs on the page
        while( svgL-- ) {

            // If SVG isn't the first one, continue ...
            if(svgL > 0) {

                // Get title attribute of SVG
                var svgTitle = svgs[svgL].getAttribute("title");

                // Get all <use> elements from each SVG
                var uses = svgs[svgL].getElementsByTagName("use"),
                    usesL = uses.length;

                // Loop through all <use> elements within an SVG
                while( usesL-- ) {

                    // Get the 'xlink:href' attributes
                    var svgId = uses[usesL].getAttribute("xlink:href");

                    // Remove first character from variable (This removes the #)
                    svgId = svgId.substring(1, svgId.length);

                    // Create New Image
                    var newImg = document.createElement("img");

                    // Assign src attribute
                    newImg.src = WP_VARS.template_dir + "/assets/svg/pngs/" + svgId + ".png";

                    newImg.className = 'icon icon-png';

                    // Assign alt attribute
                    newImg.alt = svgTitle ? svgTitle : '';

                    // Insert new element straight after the SVG in question
                    svgs[svgL].parentNode.insertBefore(newImg, svgs[svgL].nextSibling);
                }

                // Remove all SVG nodes
                svgs[svgL].parentNode.removeChild(svgs[svgL]);
            }
        }
    }
};

// Call fallback function
iconsFallback();
/**
 * @author Kyle Florence <kyle[dot]florence[at]gmail[dot]com>
 * @website https://github.com/kflorence/jquery-deserialize/
 * @version 1.2.1
 *
 * Dual licensed under the MIT and GPLv2 licenses.
 */
(function( jQuery, undefined ) {

var push = Array.prototype.push,
    rcheck = /^(?:radio|checkbox)$/i,
    rplus = /\+/g,
    rselect = /^(?:option|select-one|select-multiple)$/i,
    rvalue = /^(?:button|color|date|datetime|datetime-local|email|hidden|month|number|password|range|reset|search|submit|tel|text|textarea|time|url|week)$/i;

function getElements( elements ) {
    return elements.map(function() {
            return this.elements ? jQuery.makeArray( this.elements ) : this;
        }).filter( ":input:not(:disabled)" ).get();
}

function getElementsByName( elements ) {
    var current,
        elementsByName = {};

    jQuery.each( elements, function( i, element ) {
        current = elementsByName[ element.name ];
        elementsByName[ element.name ] = current === undefined ? element :
            ( jQuery.isArray( current ) ? current.concat( element ) : [ current, element ] );
    });

    return elementsByName;
}

jQuery.fn.deserialize = function( data, options ) {
    var i, length,
        elements = getElements( this ),
        normalized = [];

    if ( !data || !elements.length ) {
        return this;
    }

    if ( jQuery.isArray( data ) ) {
        normalized = data;

    } else if ( jQuery.isPlainObject( data ) ) {
        var key, value;

        for ( key in data ) {
            jQuery.isArray( value = data[ key ] ) ?
                push.apply( normalized, jQuery.map( value, function( v ) {
                    return { name: key, value: v };
                })) : push.call( normalized, { name: key, value: value } );
        }

    } else if ( typeof data === "string" ) {
        var parts;

        data = data.split( "&" );

        for ( i = 0, length = data.length; i < length; i++ ) {
            parts =  data[ i ].split( "=" );
            push.call( normalized, {
                name: decodeURIComponent( parts[ 0 ] ),
                value: decodeURIComponent( parts[ 1 ].replace( rplus, "%20" ) )
            });
        }
    }

    if ( !( length = normalized.length ) ) {
        return this;
    }

    var current, element, j, len, name, property, type, value,
        change = jQuery.noop,
        complete = jQuery.noop,
        names = {};

    options = options || {};
    elements = getElementsByName( elements );

    // Backwards compatible with old arguments: data, callback
    if ( jQuery.isFunction( options ) ) {
        complete = options;

    } else {
        change = jQuery.isFunction( options.change ) ? options.change : change;
        complete = jQuery.isFunction( options.complete ) ? options.complete : complete;
    }

    for ( i = 0; i < length; i++ ) {
        current = normalized[ i ];

        name = current.name;
        value = current.value;

        if ( !( element = elements[ name ] ) ) {
            continue;
        }

        type = ( len = element.length ) ? element[ 0 ] : element;
        type = ( type.type || type.nodeName ).toLowerCase();
        property = null;

        if ( rvalue.test( type ) ) {
            if ( len ) {
                j = names[ name ];
                element = element[ names[ name ] = ( j == undefined ) ? 0 : ++j ];
            }

            change.call( element, ( element.value = value ) );

        } else if ( rcheck.test( type ) ) {
            property = "checked";

        } else if ( rselect.test( type ) ) {
            property = "selected";
        }

        if ( property ) {
            if ( !len ) {
                element = [ element ];
                len = 1;
            }

            for ( j = 0; j < len; j++ ) {
                current = element[ j ];

                if ( current.value == value ) {
                    change.call( current, ( current[ property ] = true ) && value );
                }
            }
        }
    }

    complete.call( this );

    return this;
};

})( jQuery );
/*
 * jQuery Facets Plugin v0.0.9
 * http://srchulo.com/jquery_plugins/jquery_facets.html
 *
 * Copyright 2013, Adam Hopkins
 * http://srchulo.com/
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

(function( $ ){

  var methods = { 
    init : function( options ) { 
      var plugin = this;
      var settings = $.extend( {
       'searchCont'         : '#searchCont', //A selector for where the filtered search results will go. i.e., a div.
       'ajaxURL'            : $(location).attr('href'), //URL to make ajax requests to for filtered results. Defaults to current URL.
       'URLParams'          : new Array(), //extra params for the url, such as the original search query or something. takes array in jQuery's serializeArray() format
                                           //i.e. 
                                           //[ 
                                           //   {
                                           //     "name":"search",
                                           //     "value":"brown jackets"
                                           //     }
                                           // ]
       'ajaxMethod'         : 'GET', // POST or GET
       'bindType'           : 'change',
       'bindTypes'          : new Array(),
       'excludeBindTypes'   : new Array(),
       'generateData'       : function () { 
    
          var settings = this.data("settings");
          var dataArr = remove_empty(this.serializeArray());
    
          //push on any user added params
          for(var k = 0; k < settings.URLParams.length; k++) { 
            dataArr.push(settings.URLParams[k]);
          }   

          return dataArr;
        }, //generates data from the facets form to send server-side!
       'preAJAX'            : function () { return true }, //pre-call ajax function, used for validation etc. returns true or false
       'postAJAX'           : function (data) { $(plugin.data("settings").searchCont).html(data) }, //post-call ajax function
       'preHash'            : function () {}, 
       'postHash'           : function () {}, 
       'hashOptions'        : new Array(), //takes a array of hashes of key value pairs. Must be in jQuery's serializeArray() format
       'hash'               : true, //whether or not to put facet values in url after hashtag #
       'shouldSubmit'       : false, //whether form can submit or not. Default is false.
      }, options);

      //set the settings for this object
      plugin.data("settings",settings);

      //bind each input to the appropriate bind type
      plugin.find(":input").each(function(){

        //find out if we should not bind to this input
        var excludeBindTypes = plugin.data('settings').excludeBindTypes;
        var shouldBind = true;
        for(var k = 0; k < excludeBindTypes.length; k++) { 
          if($(this).is(excludeBindTypes[k])) { 
            shouldBind = false;
            break;
          }   
        }   

        if(!shouldBind)
          return true;

        //use default bindType
        var bindType = plugin.data('settings').bindType;

        //see if this input has a special bindType
        var bindTypes = plugin.data('settings').bindTypes;
        for(var k = 0; k < bindTypes.length; k++) { 
          if($(this).is(bindTypes[k]['selector'])) { 
            bindType = bindTypes[k]['bindType'];
            break; //will get first bindType of selector it matches
          }   
        }   

        $(this).on(bindType, {'plugin': plugin}, methods.ajaxReq);  
      }); 

      if(plugin.data('settings').hash) 
        methods.hashInit.apply(plugin);

      if(!plugin.data('settings').shouldSubmit) { 
        plugin.submit(function(){ return false }); 
      }   

      return plugin;
    },  
    ajaxReq : function(event) { 
      //called by user
      var plugin = this;

      //called by bind event
      if(event != undefined)
          plugin = event.data.plugin;
    
      var settings = plugin.data("settings");
      if(!settings.preAJAX.call())
        return;

      //make ajax call to server for results
      $.ajax({
        type: settings.ajaxMethod,
        url: settings.ajaxURL,
        data: settings.generateData.apply(plugin),
      }).done(settings.postAJAX);

      if(settings.hash)
        return methods.hashURL.call(plugin,true);
    },  
    hashURL : function (plugin,calledByPlugin) { 
      //if called by user
      if(!calledByPlugin)
        plugin = this;

      var settings = plugin.data("settings");
      settings.preHash.call();

      //remove empty valued items from serialization
      //seems complicated, but necessary so we don't get empty selects and text inputs
      //I did a lot of searching, if anyone finds a better way please let me know...
      var arr = plugin.serializeArray();

      //push on any user added params
      for(var k = 0; k < settings.hashOptions.length; k++) { 
        arr.push(settings.hashOptions[k]);
      }   

      var serializeArr = remove_empty(arr);

      var hash = $.param(serializeArr);

      //to prevent scrolling to top of page. Stupid, I know. Let me know if there's a better way.
      if(hash == "" || hash == null || hash == undefined || hash.match(/^\s*$/))
        hash = "nothing";

      window.location.hash = hash;

      settings.postHash.call();

      return hash; //return resulting hash URL
    },  
    hashInit : function () { 
      var hash = window.location.hash;
      hash = hash.replace(/^#/, '');  
      if(hash != "" && hash != undefined && hash != null && hash != "nothing") { 
        this.deserialize(hash); 
        methods.ajaxReq.apply(this);
      }   
    },  
    get : function (key) { 
			return this.data("settings")[key];
		},
    set : function (key,value) { 
			var settings = this.data("settings");
			settings[key] = value;
			this.data("settings",settings);
    },  

  };  

  $.fn.facets = function( method ) { 
    // Method calling logic
    if ( methods[method] ) { 
      return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 )); 
    } else if ( typeof method === 'object' || ! method ) { 
      return methods.init.apply( this, arguments );
    } else {
      $.error( 'Method ' +  method + ' does not exist on jQuery.facetedSearch' );
    }    
  };  

  function remove_empty (arr) { 
    var newArr = new Array();
    for(var k = 0; k < arr.length; k++) {
      if(arr[k]['value'] != "") {
        newArr.push(arr[k]);
      }   
    }   
    return newArr;
  }
})( jQuery );

/*!
 * SlickNav Responsive Mobile Menu v1.0.6
 * (c) 2015 Josh Cope
 * licensed under MIT
 */
;(function ($, document, window) {
    var
    // default settings object.
        defaults = {
            label: 'MENU',
            duplicate: true,
            duration: 200,
            easingOpen: 'swing',
            easingClose: 'swing',
            closedSymbol: '&#9658;',
            openedSymbol: '&#9660;',
            prependTo: 'body',
            appendTo: '',
            parentTag: 'a',
            closeOnClick: false,
            allowParentLinks: false,
            nestedParentLinks: true,
            showChildren: false,
            removeIds: false,
            removeClasses: false,
            removeStyles: false,
			brand: '',
            init: function () {},
            beforeOpen: function () {},
            beforeClose: function () {},
            afterOpen: function () {},
            afterClose: function () {}
        },
        mobileMenu = 'slicknav',
        prefix = 'slicknav';

    function Plugin(element, options) {
        this.element = element;

        // jQuery has an extend method which merges the contents of two or
        // more objects, storing the result in the first object. The first object
        // is generally empty as we don't want to alter the default options for
        // future instances of the plugin
        this.settings = $.extend({}, defaults, options);

        this._defaults = defaults;
        this._name = mobileMenu;

        this.init();
    }

    Plugin.prototype.init = function () {
        var $this = this,
            menu = $(this.element),
            settings = this.settings,
            iconClass,
            menuBar;

        // clone menu if needed
        if (settings.duplicate) {
            $this.mobileNav = menu.clone();
            //remove ids from clone to prevent css issues
            $this.mobileNav.removeAttr('id');
            $this.mobileNav.find('*').each(function (i, e) {
                $(e).removeAttr('id');
            });
        } else {
            $this.mobileNav = menu;

            // remove ids if set
            $this.mobileNav.removeAttr('id');
            $this.mobileNav.find('*').each(function (i, e) {
                $(e).removeAttr('id');
            });
        }

        // remove classes if set
        if (settings.removeClasses) {
            $this.mobileNav.removeAttr('class');
            $this.mobileNav.find('*').each(function (i, e) {
                $(e).removeAttr('class');
            });
        }
        
        // remove styles if set
        if (settings.removeStyles) {
            $this.mobileNav.removeAttr('style');
            $this.mobileNav.find('*').each(function (i, e) {
                $(e).removeAttr('style');
            });
        }

        // styling class for the button
        iconClass = prefix + '_icon';

        if (settings.label === '') {
            iconClass += ' ' + prefix + '_no-text';
        }

        if (settings.parentTag == 'a') {
            settings.parentTag = 'a href="#"';
        }

        // create menu bar
        $this.mobileNav.attr('class', prefix + '_nav');
        menuBar = $('<div class="' + prefix + '_menu"></div>');
		if (settings.brand !== '') {
			var brand = $('<div class="' + prefix + '_brand">'+settings.brand+'</div>');
			$(menuBar).append(brand);
		}
        $this.btn = $(
            ['<' + settings.parentTag + ' aria-haspopup="true" tabindex="0" class="' + prefix + '_btn ' + prefix + '_collapsed">',
                '<span class="' + prefix + '_menutxt">' + settings.label + '</span>',
                '<span class="' + iconClass + '">',
                    '<span class="' + prefix + '_icon-bar"></span>',
                    '<span class="' + prefix + '_icon-bar"></span>',
                    '<span class="' + prefix + '_icon-bar"></span>',
                '</span>',
            '</' + settings.parentTag + '>'
            ].join('')
        );
        $(menuBar).append($this.btn);
        if(settings.appendTo !== '') {
            $(settings.appendTo).append(menuBar);
        } else {
            $(settings.prependTo).prepend(menuBar);
        }
        menuBar.append($this.mobileNav);

        // iterate over structure adding additional structure
        var items = $this.mobileNav.find('li');
        $(items).each(function () {
            var item = $(this),
                data = {};
            data.children = item.children('ul').attr('role', 'menu');
            item.data('menu', data);

            // if a list item has a nested menu
            if (data.children.length > 0) {

                // select all text before the child menu
                // check for anchors

                var a = item.contents(),
                    containsAnchor = false,
                    nodes = [];

                $(a).each(function () {
                    if (!$(this).is('ul')) {
                        nodes.push(this);
                    } else {
                        return false;
                    }

                    if($(this).is("a")) {
                        containsAnchor = true;
                    }
                });

                var wrapElement = $(
                    '<' + settings.parentTag + ' role="menuitem" aria-haspopup="true" tabindex="-1" class="' + prefix + '_item"/>'
                );

                // wrap item text with tag and add classes unless we are separating parent links
                if ((!settings.allowParentLinks || settings.nestedParentLinks) || !containsAnchor) {
                    var $wrap = $(nodes).wrapAll(wrapElement).parent();
                    $wrap.addClass(prefix+'_row');
                } else
                    $(nodes).wrapAll('<span class="'+prefix+'_parent-link '+prefix+'_row"/>').parent();

                if (!settings.showChildren) {
                    item.addClass(prefix+'_collapsed');
                } else {
                    item.addClass(prefix+'_open');
                }

                item.addClass(prefix+'_parent');

                // create parent arrow. wrap with link if parent links and separating
                var arrowElement = $('<span class="'+prefix+'_arrow">'+(settings.showChildren?settings.openedSymbol:settings.closedSymbol)+'</span>');

                if (settings.allowParentLinks && !settings.nestedParentLinks && containsAnchor)
                    arrowElement = arrowElement.wrap(wrapElement).parent();

                //append arrow
                $(nodes).last().after(arrowElement);


            } else if ( item.children().length === 0) {
                 item.addClass(prefix+'_txtnode');
            }

            // accessibility for links
            item.children('a').attr('role', 'menuitem').click(function(event){
                //Ensure that it's not a parent
                if (settings.closeOnClick && !$(event.target).parent().closest('li').hasClass(prefix+'_parent')) {
                        //Emulate menu close if set
                        $($this.btn).click();
                    }
            });

            //also close on click if parent links are set
            if (settings.closeOnClick && settings.allowParentLinks) {
                item.children('a').children('a').click(function (event) {
                    //Emulate menu close
                    $($this.btn).click();
                });

                item.find('.'+prefix+'_parent-link a:not(.'+prefix+'_item)').click(function(event){
                    //Emulate menu close
                        $($this.btn).click();
                });
            }
        });

        // structure is in place, now hide appropriate items
        $(items).each(function () {
            var data = $(this).data('menu');
            if (!settings.showChildren){
                $this._visibilityToggle(data.children, null, false, null, true);
            }
        });

        // finally toggle entire menu
        $this._visibilityToggle($this.mobileNav, null, false, 'init', true);

        // accessibility for menu button
        $this.mobileNav.attr('role','menu');

        // outline prevention when using mouse
        $(document).mousedown(function(){
            $this._outlines(false);
        });

        $(document).keyup(function(){
            $this._outlines(true);
        });

        // menu button click
        $($this.btn).click(function (e) {
            e.preventDefault();
            $this._menuToggle();
        });

        // click on menu parent
        $this.mobileNav.on('click', '.' + prefix + '_item', function (e) {
            e.preventDefault();
            $this._itemClick($(this));
        });

        // check for enter key on menu button and menu parents
        $($this.btn).keydown(function (e) {
            var ev = e || event;
            if(ev.keyCode == 13) {
                e.preventDefault();
                $this._menuToggle();
            }
        });

        $this.mobileNav.on('keydown', '.'+prefix+'_item', function(e) {
            var ev = e || event;
            if(ev.keyCode == 13) {
                e.preventDefault();
                $this._itemClick($(e.target));
            }
        });

        // allow links clickable within parent tags if set
        if (settings.allowParentLinks && settings.nestedParentLinks) {
            $('.'+prefix+'_item a').click(function(e){
                    e.stopImmediatePropagation();
            });
        }
    };

    //toggle menu
    Plugin.prototype._menuToggle = function (el) {
        var $this = this;
        var btn = $this.btn;
        var mobileNav = $this.mobileNav;

        if (btn.hasClass(prefix+'_collapsed')) {
            btn.removeClass(prefix+'_collapsed');
            btn.addClass(prefix+'_open');
        } else {
            btn.removeClass(prefix+'_open');
            btn.addClass(prefix+'_collapsed');
        }
        btn.addClass(prefix+'_animating');
        $this._visibilityToggle(mobileNav, btn.parent(), true, btn);
    };

    // toggle clicked items
    Plugin.prototype._itemClick = function (el) {
        var $this = this;
        var settings = $this.settings;
        var data = el.data('menu');
        if (!data) {
            data = {};
            data.arrow = el.children('.'+prefix+'_arrow');
            data.ul = el.next('ul');
            data.parent = el.parent();
            //Separated parent link structure
            if (data.parent.hasClass(prefix+'_parent-link')) {
                data.parent = el.parent().parent();
                data.ul = el.parent().next('ul');
            }
            el.data('menu', data);
        }
        if (data.parent.hasClass(prefix+'_collapsed')) {
            data.arrow.html(settings.openedSymbol);
            data.parent.removeClass(prefix+'_collapsed');
            data.parent.addClass(prefix+'_open');
            data.parent.addClass(prefix+'_animating');
            $this._visibilityToggle(data.ul, data.parent, true, el);
        } else {
            data.arrow.html(settings.closedSymbol);
            data.parent.addClass(prefix+'_collapsed');
            data.parent.removeClass(prefix+'_open');
            data.parent.addClass(prefix+'_animating');
            $this._visibilityToggle(data.ul, data.parent, true, el);
        }
    };

    // toggle actual visibility and accessibility tags
    Plugin.prototype._visibilityToggle = function(el, parent, animate, trigger, init) {
        var $this = this;
        var settings = $this.settings;
        var items = $this._getActionItems(el);
        var duration = 0;
        if (animate) {
            duration = settings.duration;
        }

        if (el.hasClass(prefix+'_hidden')) {
            el.removeClass(prefix+'_hidden');
             //Fire beforeOpen callback
                if (!init) {
                    settings.beforeOpen(trigger);
                }
            el.slideDown(duration, settings.easingOpen, function(){

                $(trigger).removeClass(prefix+'_animating');
                $(parent).removeClass(prefix+'_animating');

                //Fire afterOpen callback
                if (!init) {
                    settings.afterOpen(trigger);
                }
            });
            el.attr('aria-hidden','false');
            items.attr('tabindex', '0');
            $this._setVisAttr(el, false);
        } else {
            el.addClass(prefix+'_hidden');

            //Fire init or beforeClose callback
            if (!init){
                settings.beforeClose(trigger);
            }

            el.slideUp(duration, this.settings.easingClose, function() {
                el.attr('aria-hidden','true');
                items.attr('tabindex', '-1');
                $this._setVisAttr(el, true);
                el.hide(); //jQuery 1.7 bug fix

                $(trigger).removeClass(prefix+'_animating');
                $(parent).removeClass(prefix+'_animating');

                //Fire init or afterClose callback
                if (!init){
                    settings.afterClose(trigger);
                } else if (trigger == 'init'){
                    settings.init();
                }
            });
        }
    };

    // set attributes of element and children based on visibility
    Plugin.prototype._setVisAttr = function(el, hidden) {
        var $this = this;

        // select all parents that aren't hidden
        var nonHidden = el.children('li').children('ul').not('.'+prefix+'_hidden');

        // iterate over all items setting appropriate tags
        if (!hidden) {
            nonHidden.each(function(){
                var ul = $(this);
                ul.attr('aria-hidden','false');
                var items = $this._getActionItems(ul);
                items.attr('tabindex', '0');
                $this._setVisAttr(ul, hidden);
            });
        } else {
            nonHidden.each(function(){
                var ul = $(this);
                ul.attr('aria-hidden','true');
                var items = $this._getActionItems(ul);
                items.attr('tabindex', '-1');
                $this._setVisAttr(ul, hidden);
            });
        }
    };

    // get all 1st level items that are clickable
    Plugin.prototype._getActionItems = function(el) {
        var data = el.data("menu");
        if (!data) {
            data = {};
            var items = el.children('li');
            var anchors = items.find('a');
            data.links = anchors.add(items.find('.'+prefix+'_item'));
            el.data('menu', data);
        }
        return data.links;
    };

    Plugin.prototype._outlines = function(state) {
        if (!state) {
            $('.'+prefix+'_item, .'+prefix+'_btn').css('outline','none');
        } else {
            $('.'+prefix+'_item, .'+prefix+'_btn').css('outline','');
        }
    };

    Plugin.prototype.toggle = function(){
        var $this = this;
        $this._menuToggle();
    };

    Plugin.prototype.open = function(){
        var $this = this;
        if ($this.btn.hasClass(prefix+'_collapsed')) {
            $this._menuToggle();
        }
    };

    Plugin.prototype.close = function(){
        var $this = this;
        if ($this.btn.hasClass(prefix+'_open')) {
            $this._menuToggle();
        }
    };

    $.fn[mobileMenu] = function ( options ) {
        var args = arguments;

        // Is the first parameter an object (options), or was omitted, instantiate a new instance
        if (options === undefined || typeof options === 'object') {
            return this.each(function () {

                // Only allow the plugin to be instantiated once due to methods
                if (!$.data(this, 'plugin_' + mobileMenu)) {

                    // if it has no instance, create a new one, pass options to our plugin constructor,
                    // and store the plugin instance in the elements jQuery data object.
                    $.data(this, 'plugin_' + mobileMenu, new Plugin( this, options ));
                }
            });

        // If is a string and doesn't start with an underscore or 'init' function, treat this as a call to a public method.
        } else if (typeof options === 'string' && options[0] !== '_' && options !== 'init') {

            // Cache the method call to make it possible to return a value
            var returns;

            this.each(function () {
                var instance = $.data(this, 'plugin_' + mobileMenu);

                // Tests that there's already a plugin-instance and checks that the requested public method exists
                if (instance instanceof Plugin && typeof instance[options] === 'function') {

                    // Call the method of our plugin instance, and pass it the supplied arguments.
                    returns = instance[options].apply( instance, Array.prototype.slice.call( args, 1 ) );
                }
            });

            // If the earlier cached method gives a value back return the value, otherwise return this to preserve chainability.
            return returns !== undefined ? returns : this;
        }
    };
}(jQuery, document, window));

/*
 * validate.js 2.0.1
 * Copyright (c) 2011 - 2015 Rick Harrison, http://rickharrison.me
 * validate.js is open sourced under the MIT license.
 * Portions of validate.js are inspired by CodeIgniter.
 * http://rickharrison.github.com/validate.js
 */
(function(r,t,l){var u={required:"The %s field is required.",matches:"The %s field does not match the %s field.","default":"The %s field is still set to default, please change.",valid_email:"The %s field must contain a valid email address.",valid_emails:"The %s field must contain all valid email addresses.",min_length:"The %s field must be at least %s characters in length.",max_length:"The %s field must not exceed %s characters in length.",exact_length:"The %s field must be exactly %s characters in length.",
greater_than:"The %s field must contain a number greater than %s.",less_than:"The %s field must contain a number less than %s.",alpha:"The %s field must only contain alphabetical characters.",alpha_numeric:"The %s field must only contain alpha-numeric characters.",alpha_dash:"The %s field must only contain alpha-numeric characters, underscores, and dashes.",numeric:"The %s field must contain only numbers.",integer:"The %s field must contain an integer.",decimal:"The %s field must contain a decimal number.",
is_natural:"The %s field must contain only positive numbers.",is_natural_no_zero:"The %s field must contain a number greater than zero.",valid_ip:"The %s field must contain a valid IP.",valid_base64:"The %s field must contain a base64 string.",valid_credit_card:"The %s field must contain a valid credit card number.",is_file_type:"The %s field must contain only %s files.",valid_url:"The %s field must contain a valid URL.",greater_than_date:"The %s field must contain a more recent date than %s.",less_than_date:"The %s field must contain an older date than %s.",
greater_than_or_equal_date:"The %s field must contain a date that's at least as recent as %s.",less_than_or_equal_date:"The %s field must contain a date that's %s or older."},v=function(a){},w=/^(.+?)\[(.+)\]$/,g=/^[0-9]+$/,x=/^\-?[0-9]+$/,m=/^\-?[0-9]*\.?[0-9]+$/,q=/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,y=/^[a-z]+$/i,z=/^[a-z0-9]+$/i,A=/^[a-z0-9_\-]+$/i,B=/^[0-9]+$/i,C=/^[1-9][0-9]*$/i,D=/^((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){3}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})$/i,
E=/[^a-zA-Z0-9\/\+=]/i,F=/^[\d\-\s]+$/,G=/^((http|https):\/\/(\w+:{0,1}\w*@)?(\S+)|)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?$/,H=/\d{4}-\d{1,2}-\d{1,2}/,f=function(a,c,b){this.callback=b||v;this.errors=[];this.fields={};this.form=this._formByNameOrNode(a)||{};this.messages={};this.handlers={};this.conditionals={};a=0;for(b=c.length;a<b;a++){var d=c[a];if((d.name||d.names)&&d.rules)if(d.names)for(var n=0,f=d.names.length;n<f;n++)this._addField(d,d.names[n]);else this._addField(d,d.name);else console.warn("validate.js: The following field is being skipped due to a misconfiguration:"),
console.warn(d),console.warn("Check to ensure you have properly configured a name and rules for this field")}var e=this.form.onsubmit;this.form.onsubmit=function(a){return function(b){try{return a._validateForm(b)&&(e===l||e())}catch(c){}}}(this)},p=function(a,c){var b;if(0<a.length&&("radio"===a[0].type||"checkbox"===a[0].type))for(b=0,elementLength=a.length;b<elementLength;b++){if(a[b].checked)return a[b][c]}else return a[c]};f.prototype.setMessage=function(a,c){this.messages[a]=c;return this};
f.prototype.registerCallback=function(a,c){a&&"string"===typeof a&&c&&"function"===typeof c&&(this.handlers[a]=c);return this};f.prototype.registerConditional=function(a,c){a&&"string"===typeof a&&c&&"function"===typeof c&&(this.conditionals[a]=c);return this};f.prototype._formByNameOrNode=function(a){return"object"===typeof a?a:t.forms[a]};f.prototype._addField=function(a,c){this.fields[c]={name:c,display:a.display||c,rules:a.rules,depends:a.depends,id:null,element:null,type:null,value:null,checked:null}};
f.prototype._validateForm=function(a){this.errors=[];for(var c in this.fields)if(this.fields.hasOwnProperty(c)){var b=this.fields[c]||{},d=this.form[b.name];d&&d!==l&&(b.id=p(d,"id"),b.element=d,b.type=0<d.length?d[0].type:d.type,b.value=p(d,"value"),b.checked=p(d,"checked"),b.depends&&"function"===typeof b.depends?b.depends.call(this,b)&&this._validateField(b):b.depends&&"string"===typeof b.depends&&this.conditionals[b.depends]?this.conditionals[b.depends].call(this,b)&&this._validateField(b):this._validateField(b))}"function"===
typeof this.callback&&this.callback(this.errors,a);0<this.errors.length&&(a&&a.preventDefault?a.preventDefault():event&&(event.returnValue=!1));return!0};f.prototype._validateField=function(a){var c,b,d=a.rules.split("|"),f=a.rules.indexOf("required"),I=!a.value||""===a.value||a.value===l;c=0;for(ruleLength=d.length;c<ruleLength;c++){var e=d[c];b=null;var h=!1,k=w.exec(e);if(-1!==f||-1!==e.indexOf("!callback_")||!I)if(k&&(e=k[1],b=k[2]),"!"===e.charAt(0)&&(e=e.substring(1,e.length)),"function"===
typeof this._hooks[e]?this._hooks[e].apply(this,[a,b])||(h=!0):"callback_"===e.substring(0,9)&&(e=e.substring(9,e.length),"function"===typeof this.handlers[e]&&!1===this.handlers[e].apply(this,[a.value,b,a])&&(h=!0)),h){k=this.messages[a.name+"."+e]||this.messages[e]||u[e];h="An error has occurred with the "+a.display+" field.";k&&(h=k.replace("%s",a.display),b&&(h=h.replace("%s",this.fields[b]?this.fields[b].display:b)));var g;for(b=0;b<this.errors.length;b+=1)a.id===this.errors[b].id&&(g=this.errors[b]);
e=g||{id:a.id,display:a.display,element:a.element,name:a.name,message:h,messages:[],rule:e};e.messages.push(h);g||this.errors.push(e)}}};f.prototype._getValidDate=function(a){if(!a.match("today")&&!a.match(H))return!1;var c=new Date;a.match("today")||(a=a.split("-"),c.setFullYear(a[0]),c.setMonth(a[1]-1),c.setDate(a[2]));return c};f.prototype._hooks={required:function(a){var c=a.value;return"checkbox"===a.type||"radio"===a.type?!0===a.checked:null!==c&&""!==c},"default":function(a,c){return a.value!==
c},matches:function(a,c){var b=this.form[c];return b?a.value===b.value:!1},valid_email:function(a){return q.test(a.value)},valid_emails:function(a){a=a.value.split(/\s*,\s*/g);for(var c=0,b=a.length;c<b;c++)if(!q.test(a[c]))return!1;return!0},min_length:function(a,c){return g.test(c)?a.value.length>=parseInt(c,10):!1},max_length:function(a,c){return g.test(c)?a.value.length<=parseInt(c,10):!1},exact_length:function(a,c){return g.test(c)?a.value.length===parseInt(c,10):!1},greater_than:function(a,
c){return m.test(a.value)?parseFloat(a.value)>parseFloat(c):!1},less_than:function(a,c){return m.test(a.value)?parseFloat(a.value)<parseFloat(c):!1},alpha:function(a){return y.test(a.value)},alpha_numeric:function(a){return z.test(a.value)},alpha_dash:function(a){return A.test(a.value)},numeric:function(a){return g.test(a.value)},integer:function(a){return x.test(a.value)},decimal:function(a){return m.test(a.value)},is_natural:function(a){return B.test(a.value)},is_natural_no_zero:function(a){return C.test(a.value)},
valid_ip:function(a){return D.test(a.value)},valid_base64:function(a){return E.test(a.value)},valid_url:function(a){return G.test(a.value)},valid_credit_card:function(a){if(!F.test(a.value))return!1;var c=0,b=0,d=!1;a=a.value.replace(/\D/g,"");for(var f=a.length-1;0<=f;f--)b=a.charAt(f),b=parseInt(b,10),d&&9<(b*=2)&&(b-=9),c+=b,d=!d;return 0===c%10},is_file_type:function(a,c){if("file"!==a.type)return!0;var b=a.value.substr(a.value.lastIndexOf(".")+1),d=c.split(","),f=!1,g=0,e=d.length;for(g;g<e;g++)b.toUpperCase()==
d[g].toUpperCase()&&(f=!0);return f},greater_than_date:function(a,c){var b=this._getValidDate(a.value),d=this._getValidDate(c);return d&&b?b>d:!1},less_than_date:function(a,c){var b=this._getValidDate(a.value),d=this._getValidDate(c);return d&&b?b<d:!1},greater_than_or_equal_date:function(a,c){var b=this._getValidDate(a.value),d=this._getValidDate(c);return d&&b?b>=d:!1},less_than_or_equal_date:function(a,c){var b=this._getValidDate(a.value),d=this._getValidDate(c);return d&&b?b<=d:!1}};r.FormValidator=
f})(window,document);"undefined"!==typeof module&&module.exports&&(module.exports=FormValidator);
/*!
 * Bootstrap v3.3.5 (http://getbootstrap.com) - DROPDOWN + TABS
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 */

/*!
 * Generated using the Bootstrap Customizer (http://getbootstrap.com/customize/?id=d3cbacd29a9a71f9d7d5)
 * Config saved to config.json and https://gist.github.com/d3cbacd29a9a71f9d7d5
 */
if("undefined"==typeof jQuery)throw new Error("Bootstrap's JavaScript requires jQuery");+function(t){"use strict";var e=t.fn.jquery.split(" ")[0].split(".");if(e[0]<2&&e[1]<9||1==e[0]&&9==e[1]&&e[2]<1)throw new Error("Bootstrap's JavaScript requires jQuery version 1.9.1 or higher")}(jQuery),+function(t){"use strict";function e(e){var a=e.attr("data-target");a||(a=e.attr("href"),a=a&&/#[A-Za-z]/.test(a)&&a.replace(/.*(?=#[^\s]*$)/,""));var n=a&&t(a);return n&&n.length?n:e.parent()}function a(a){a&&3===a.which||(t(r).remove(),t(i).each(function(){var n=t(this),r=e(n),i={relatedTarget:this};r.hasClass("open")&&(a&&"click"==a.type&&/input|textarea/i.test(a.target.tagName)&&t.contains(r[0],a.target)||(r.trigger(a=t.Event("hide.bs.dropdown",i)),a.isDefaultPrevented()||(n.attr("aria-expanded","false"),r.removeClass("open").trigger("hidden.bs.dropdown",i))))}))}function n(e){return this.each(function(){var a=t(this),n=a.data("bs.dropdown");n||a.data("bs.dropdown",n=new o(this)),"string"==typeof e&&n[e].call(a)})}var r=".dropdown-backdrop",i='[data-toggle="dropdown"]',o=function(e){t(e).on("click.bs.dropdown",this.toggle)};o.VERSION="3.3.5",o.prototype.toggle=function(n){var r=t(this);if(!r.is(".disabled, :disabled")){var i=e(r),o=i.hasClass("open");if(a(),!o){"ontouchstart"in document.documentElement&&!i.closest(".navbar-nav").length&&t(document.createElement("div")).addClass("dropdown-backdrop").insertAfter(t(this)).on("click",a);var s={relatedTarget:this};if(i.trigger(n=t.Event("show.bs.dropdown",s)),n.isDefaultPrevented())return;r.trigger("focus").attr("aria-expanded","true"),i.toggleClass("open").trigger("shown.bs.dropdown",s)}return!1}},o.prototype.keydown=function(a){if(/(38|40|27|32)/.test(a.which)&&!/input|textarea/i.test(a.target.tagName)){var n=t(this);if(a.preventDefault(),a.stopPropagation(),!n.is(".disabled, :disabled")){var r=e(n),o=r.hasClass("open");if(!o&&27!=a.which||o&&27==a.which)return 27==a.which&&r.find(i).trigger("focus"),n.trigger("click");var s=" li:not(.disabled):visible a",d=r.find(".dropdown-menu"+s);if(d.length){var l=d.index(a.target);38==a.which&&l>0&&l--,40==a.which&&l<d.length-1&&l++,~l||(l=0),d.eq(l).trigger("focus")}}}};var s=t.fn.dropdown;t.fn.dropdown=n,t.fn.dropdown.Constructor=o,t.fn.dropdown.noConflict=function(){return t.fn.dropdown=s,this},t(document).on("click.bs.dropdown.data-api",a).on("click.bs.dropdown.data-api",".dropdown form",function(t){t.stopPropagation()}).on("click.bs.dropdown.data-api",i,o.prototype.toggle).on("keydown.bs.dropdown.data-api",i,o.prototype.keydown).on("keydown.bs.dropdown.data-api",".dropdown-menu",o.prototype.keydown)}(jQuery),+function(t){"use strict";function e(e){return this.each(function(){var n=t(this),r=n.data("bs.tab");r||n.data("bs.tab",r=new a(this)),"string"==typeof e&&r[e]()})}var a=function(e){this.element=t(e)};a.VERSION="3.3.5",a.TRANSITION_DURATION=150,a.prototype.show=function(){var e=this.element,a=e.closest("ul:not(.dropdown-menu)"),n=e.data("target");if(n||(n=e.attr("href"),n=n&&n.replace(/.*(?=#[^\s]*$)/,"")),!e.parent("li").hasClass("active")){var r=a.find(".active:last a"),i=t.Event("hide.bs.tab",{relatedTarget:e[0]}),o=t.Event("show.bs.tab",{relatedTarget:r[0]});if(r.trigger(i),e.trigger(o),!o.isDefaultPrevented()&&!i.isDefaultPrevented()){var s=t(n);this.activate(e.closest("li"),a),this.activate(s,s.parent(),function(){r.trigger({type:"hidden.bs.tab",relatedTarget:e[0]}),e.trigger({type:"shown.bs.tab",relatedTarget:r[0]})})}}},a.prototype.activate=function(e,n,r){function i(){o.removeClass("active").find("> .dropdown-menu > .active").removeClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded",!1),e.addClass("active").find('[data-toggle="tab"]').attr("aria-expanded",!0),s?(e[0].offsetWidth,e.addClass("in")):e.removeClass("fade"),e.parent(".dropdown-menu").length&&e.closest("li.dropdown").addClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded",!0),r&&r()}var o=n.find("> .active"),s=r&&t.support.transition&&(o.length&&o.hasClass("fade")||!!n.find("> .fade").length);o.length&&s?o.one("bsTransitionEnd",i).emulateTransitionEnd(a.TRANSITION_DURATION):i(),o.removeClass("in")};var n=t.fn.tab;t.fn.tab=e,t.fn.tab.Constructor=a,t.fn.tab.noConflict=function(){return t.fn.tab=n,this};var r=function(a){a.preventDefault(),e.call(t(this),"show")};t(document).on("click.bs.tab.data-api",'[data-toggle="tab"]',r).on("click.bs.tab.data-api",'[data-toggle="pill"]',r)}(jQuery);

/*
    A simple jQuery modal (http://github.com/kylefox/jquery-modal)
    Version 0.5.8
*/
!function(a){var b=null;a.modal=function(c,d){a.modal.close();var e,f;if(this.$body=a("body"),this.options=a.extend({},a.modal.defaults,d),this.options.doFade=!isNaN(parseInt(this.options.fadeDuration,10)),c.is("a"))if(f=c.attr("href"),/^#/.test(f)){if(this.$elm=a(f),1!==this.$elm.length)return null;this.open()}else this.$elm=a("<div>"),this.$body.append(this.$elm),e=function(a,b){b.elm.remove()},this.showSpinner(),c.trigger(a.modal.AJAX_SEND),a.get(f).done(function(d){b&&(c.trigger(a.modal.AJAX_SUCCESS),b.$elm.empty().append(d).on(a.modal.CLOSE,e),b.hideSpinner(),b.open(),c.trigger(a.modal.AJAX_COMPLETE))}).fail(function(){c.trigger(a.modal.AJAX_FAIL),b.hideSpinner(),c.trigger(a.modal.AJAX_COMPLETE)});else this.$elm=c,this.open()},a.modal.prototype={constructor:a.modal,open:function(){var b=this;this.options.doFade?(this.block(),setTimeout(function(){b.show()},this.options.fadeDuration*this.options.fadeDelay)):(this.block(),this.show()),this.options.escapeClose&&a(document).on("keydown.modal",function(b){27==b.which&&a.modal.close()}),this.options.clickClose&&this.blocker.click(a.modal.close)},close:function(){this.unblock(),this.hide(),a(document).off("keydown.modal")},block:function(){var b=this.options.doFade?0:this.options.opacity;this.$elm.trigger(a.modal.BEFORE_BLOCK,[this._ctx()]),this.blocker=a('<div class="jquery-modal blocker"></div>').css({top:0,right:0,bottom:0,left:0,width:"100%",height:"100%",position:"fixed",zIndex:this.options.zIndex,background:this.options.overlay,opacity:b}),this.$body.append(this.blocker),this.options.doFade&&this.blocker.animate({opacity:this.options.opacity},this.options.fadeDuration),this.$elm.trigger(a.modal.BLOCK,[this._ctx()])},unblock:function(){this.options.doFade?this.blocker.fadeOut(this.options.fadeDuration,function(){a(this).remove()}):this.blocker.remove()},show:function(){this.$elm.trigger(a.modal.BEFORE_OPEN,[this._ctx()]),this.options.showClose&&(this.closeButton=a('<a href="#close-modal" rel="modal:close" class="close-modal '+this.options.closeClass+'">'+this.options.closeText+"</a>"),this.$elm.append(this.closeButton)),this.$elm.addClass(this.options.modalClass+" current"),this.center(),this.options.doFade?this.$elm.fadeIn(this.options.fadeDuration):this.$elm.show(),this.$elm.trigger(a.modal.OPEN,[this._ctx()])},hide:function(){this.$elm.trigger(a.modal.BEFORE_CLOSE,[this._ctx()]),this.closeButton&&this.closeButton.remove(),this.$elm.removeClass("current"),this.options.doFade?this.$elm.fadeOut(this.options.fadeDuration):this.$elm.hide(),this.$elm.trigger(a.modal.CLOSE,[this._ctx()])},showSpinner:function(){this.options.showSpinner&&(this.spinner=this.spinner||a('<div class="'+this.options.modalClass+'-spinner"></div>').append(this.options.spinnerHtml),this.$body.append(this.spinner),this.spinner.show())},hideSpinner:function(){this.spinner&&this.spinner.remove()},center:function(){this.$elm.css({position:"fixed",top:"50%",left:"50%",marginTop:-(this.$elm.outerHeight()/2),marginLeft:-(this.$elm.outerWidth()/2),zIndex:this.options.zIndex+1})},_ctx:function(){return{elm:this.$elm,blocker:this.blocker,options:this.options}}},a.modal.prototype.resize=a.modal.prototype.center,a.modal.close=function(a){if(b){a&&a.preventDefault(),b.close();var c=b.$elm;return b=null,c}},a.modal.resize=function(){b&&b.resize()},a.modal.isActive=function(){return b?!0:!1},a.modal.defaults={overlay:"#000",opacity:.75,zIndex:1,escapeClose:!0,clickClose:!0,closeText:"Close",closeClass:"",modalClass:"modal",spinnerHtml:null,showSpinner:!0,showClose:!0,fadeDuration:null,fadeDelay:1},a.modal.BEFORE_BLOCK="modal:before-block",a.modal.BLOCK="modal:block",a.modal.BEFORE_OPEN="modal:before-open",a.modal.OPEN="modal:open",a.modal.BEFORE_CLOSE="modal:before-close",a.modal.CLOSE="modal:close",a.modal.AJAX_SEND="modal:ajax:send",a.modal.AJAX_SUCCESS="modal:ajax:success",a.modal.AJAX_FAIL="modal:ajax:fail",a.modal.AJAX_COMPLETE="modal:ajax:complete",a.fn.modal=function(c){return 1===this.length&&(b=new a.modal(this,c)),this},a(document).on("click.modal",'a[rel="modal:close"]',a.modal.close),a(document).on("click.modal",'a[rel="modal:open"]',function(b){b.preventDefault(),a(this).modal()})}(jQuery);

(function($) {

	function openPopup(e, href) {
		var top = (screen.availHeight - 500) / 2;
		var left = (screen.availWidth - 500) / 2;

		var popup = window.open(
			href,
			'social',
			'width=550,height=420,left='+ left +',top='+ top +',location=0,menubar=0,toolbar=0,status=0,scrollbars=1,resizable=1'
		);

		if(popup) {
			popup.focus();
			e.preventDefault();
			return false;
		}

		return true;
	}

	$('.js-menu').on('click', function(e) {
		if($('.js-wrapper').hasClass('m-show')) {
			$('.js-wrapper').removeClass('m-show');
		} else {
			$('.js-wrapper').addClass('m-show');
		}
	});

	$('.js-excerpt').on('click', function(e) {
		e.preventDefault();
		window.open($(this).attr('href'), 'Excerpt', 'width=600,height=800');
	});

	$('.js-social').on('click', function(e) {
		var href = $(this).attr('href');
		openPopup(e, href);
	});

	$('#showmore-nr').toggle(
	  function() {
	    $( '#new-releases .book-list.m-new-releases' ).addClass( "selected" );
	    $( '#showmore-nr #more' ).css( 'display', 'none' );
	    $( '#showmore-nr #less' ).css( 'display', 'inline' );
	  }, function() {
	    $( '#new-releases .book-list.m-new-releases' ).removeClass( "selected" );
	    $( '#showmore-nr #more' ).css( 'display', 'inline' );
	    $( '#showmore-nr #less' ).css( 'display', 'none' );
	  }
	);

	$('#showmore-bs').toggle(
	  function() {
	    $( '#best-sellers .book-list.m-new-releases' ).addClass( "selected" );
	    $( '#showmore-bs #more' ).css( 'display', 'none' );
	    $( '#showmore-bs #less' ).css( 'display', 'inline' );
	  }, function() {
	    $( '#best-sellers .book-list.m-new-releases' ).removeClass( "selected" );
	    $( '#showmore-bs #more' ).css( 'display', 'inline' );
	    $( '#showmore-bs #less' ).css( 'display', 'none' );
	  }
	);

	$('#showmore-cs').toggle(
	  function() {
	    $( '#coming-soon .book-list.m-new-releases' ).addClass( "selected" );
	    $( '#showmore-cs #more' ).css( 'display', 'none' );
	    $( '#showmore-cs #less' ).css( 'display', 'inline' );
	  }, function() {
	    $( '#coming-soon .book-list.m-new-releases' ).removeClass( "selected" );
	    $( '#showmore-cs #more' ).css( 'display', 'inline' );
	    $( '#showmore-cs #less' ).css( 'display', 'none' );
	  }
	);

	function getQueryString(sParam){
	    var sPageURL = window.location.search.substring(1);
	    var sURLVariables = sPageURL.split('&');
	    for (var i = 0; i < sURLVariables.length; i++){
	        var sParameterName = sURLVariables[i].split('=');
	        if (sParameterName[0] == sParam)
	        {
	            return sParameterName[1];
	        }
	    }
	}


	// Function to activate the tab
	function activateTab() {
		if(window.location.hash) {
			var activeTab = $('[href="' + window.location.hash.replace('/', '') + '"]');
			activeTab && activeTab.tab('show');

		}
	}

	// Change hash when a tab changes
	// Handle shown event using delegation
	$('#tab-hash a').on('click', function (e) {
		var tab = $(this).attr('href').replace('#', '');
	    window.location.hash = tab;

	});

	if(document.getElementById("tab-hash")!=null){
		activateTab();
		setTimeout(function() {
		   window.scrollTo(0, 0);
		 }, 50);

		$(window).on('hashchange', function() {
		    activateTab();
		});
	}

	function setPagination() {
		$('.js-facetpage').on('click', function(e) {
			e.preventDefault();
			var val = $(this).attr('id');
			facetsPlugin.facets("set",
				'URLParams', [ {
						name: "action",
						value: "wbm_facets"
					},
					{
						name: 'search',
						value: searchTerm
					},
					{
						name: 'page',
						value: val
					}
					]);
			facetsPlugin.facets('ajaxReq');
			// $("html, body").animate({ scrollTop: '0' });
		});
	}

	if(document.getElementById("searchCont")!=null){
		var searchTerm;

		var facetsPlugin = $("form#facets").facets({
			ajaxURL : WP_VARS.ajax_url,
			preAJAX : function() {
				searchTerm = $('input#searchTerm').val();
				$('#searchCont').addClass('is-loading');
				return true;
			},
			postAJAX : function(data) {
				$('#searchCont').html(data);
				$('#searchCont').removeClass('is-loading');
				setPagination();
			},
			URLParams : [ {
						name: "action",
						value: "wbm_facets"
						},
						{
							name: 'search',
							value: searchTerm
						}
						]
		});
	}

	var opened;
	$('#toggle-nav').on('click', function(e) {
		e.preventDefault();
		if(!opened) {
			$('#nav').addClass('nav-open');
			opened = true;
		} else {
			$('#nav').removeClass('nav-open');
			opened = false;
		}

	})
})(jQuery);

// var nav = responsiveNav(".js-nav");

(function($) {

	var Events = {
		el: {
			retailerLink : $('.js-retailer'),
			excerptLink : $('.js-excerpt'),
			socialShare : $('.js-social')
		},

		init: function() {
			this.bindUIActions();
		},

		bindUIActions: function() {
			Events.el.retailerLink.on('click', Events.sendRetailerEvent)
		},

		sendRetailerEvent: function(e) {
			var retailer = $(this).html();
		//utag.link({ "event_type":"affiliate_click", "affiliate_name" : retailer });

		}
	}

	Events.init();
})(jQuery);
(function($) {

$( "#invserchbtn" ).click(function() {
  $( ".search-field" ).toggle( "fast", function() {
    $( "#invserchbtn" ).hide();
  });
});

$(document).on('ready', function(e) {
	var hash = window.location.hash;
	if( hash === '#opn' && $('main').hasClass('book') ) {
		var firstDropdown = $('.book-formats').find('.dropdown-toggle').first();
		firstDropdown.trigger('click');
	}
	
	if( hash === '#opnebook' && $('main').hasClass('book') ) {
		var ebookDropdown = $('.book-formats').find('[data-format="ebook"]').first();
		ebookDropdown.trigger('click');
	}
})

})(jQuery);
(function($) {
  window.Newsletter = function(element) {

    on_form_submission_error = function( jqXHR, textStatus, errorThrown ) {
      self.clicked = false;
      var message = 'OOPS! Email subscription is currently not working. Please try again later.';
      display_error(message);
      self.form.css('opacity', 1);
    },

    on_form_submission_success = function( data, textStatus, jqXHR ) {

      var pidObj = JSON.parse(data);
      if(pidObj.redirect) {
        self.display_error('You have been unsubscribed from all newsletters and will be redirected back to Read It Forward.');
        setTimeout(function(){
          window.location.replace(pidObj.redirect.url);
        }, 2000);
      } else {
        if(pidObj.Pid){
          var rifPid_Pid = pidObj.Pid.Pid;
          var rifPid_Subs = pidObj.Pid.Subs;
          utag.link({ "pid": rifPid_Pid, "newsletter_name": rifPid_Subs, "event_type":"newsletter_signup"});
        }
        if( !document.getElementById('pc-wrapper') ) {
          self.form.hide();
        }
        
        self.display_confirmation();
      }

    };

    this.clicked = false;

    this.display_confirmation = function() {
      self.clicked = false;
      self.form.css('opacity', 1);
      self.errorEl.hide();
      self.confirmation.show();
    };

    this.display_error = function(errors) {
      self.clicked = false;
      self.form.css('opacity', 1);
      self.errorEl.html(errors);
      self.errorEl.show();
    };

    this.formSubmission = function(e) {
      self.form.css('opacity', 0.5);

      if(self.clicked) {
        return;
      }
      
      self.clicked = true;
      var errors;

      var email = self.form.find('input[name="Email"]').val();

      if(errors) {
        self.display_error(errors);
        return;
      }

      var ajaxurl = self.form.find('input[name="ajaxurl"]').val();
      var nonce = self.form.find('input[name="nonce"]').val();
      var action = self.form.find('input[name="action"]').val();

      var data = {
        email: email,
        nonce: nonce,
        action: action,
        refurl: encodeURI(document.location.href)
      };


      self.submitForm(ajaxurl, data);
    };

    this.submitForm = function(url, data) {
      self.form.css('opacity', 0.4);
      $.ajax( {
        url: url,
        data: data,
        success: on_form_submission_success,
        error: on_form_submission_error
      } );
    };


    this.form = $(element).find('form[name="subscribe"]');
    this.errorEl = $(element).find('.js-error');
    this.confirmation = $(element).find('.js-success');
    var self = this;

    this.init = function() {
      var validator = new FormValidator( 'subscribe', 
        [
        { name: 'Email', rules: 'valid_email|required'}
        ], 
        function(errors, event) {
        if( errors.length > 0 ) {
          $.each(errors, function(i, err) {
            self.errorEl.html(err.message);
            self.errorEl.show();
          })
        } else {
          event.preventDefault();
          self.errorEl.hide();
          self.formSubmission();
        }
      })

    }

    return this;
  };

  $('.js-subscribe').each(function(index, element) {
     var newsletterInstance = new window.Newsletter(element);
     newsletterInstance.init();
  });
})(jQuery);
(function($) {
  var PreferenceTest = function(element) {
    $.extend(this, window.Newsletter.call(this, element));

    this.subscribed = [];

    var self = this,
        preferenceCheckbox = $('.js-pref'),
        subscribeCheckbox = $('.js-sub'),
        fields = $('.js-field'),
        email = $('.js-email'),
        unsub = $('input[name="unsubscribe"]'),

        state = {
          checked : [],
          fieldsChanged : [],
          email: null
        },

        pushEmail = function() {
          state.email = $(this).val();
        },

        pushCheckbox = function() {
          if( $.inArray(this, state.checked)  === -1) {
            state.checked.push(this);
          }
        },

        pushFields = function() {
          if( $.inArray(this, state.fieldsChanged)  === -1) {
            state.fieldsChanged.push(this);
          }
        },

        removeAllNewsletters = function() {
          var checked = $(this).is(':checked');
          if(checked) {
            self.subscribed = [];
            $('input.js-sub').each(function(i, el) {
              if($(el).is(':checked')) {
                self.subscribed.push($(el));
              }
            });
            $('input.js-sub').removeAttr('checked');  
          } else {
            $.each(self.subscribed, function(i, el) {
              el.prop('checked', true);
            })

          }

        };

    this.form = $(element).find('form[name="preference"]');

    this.formSubmission = function(e) {

      if(self.clicked) {
        return;
      }

      var unsubscribed;
      
      self.clicked = true;
      self.errorEl.hide();
      self.confirmation.hide();

      var listOfSubscribed = {};
      $('.js-sub').each(function(el, i) {
        var checked = $(this).is(':checked') ? '1' : '0';
        var val = $(this).val();
        listOfSubscribed[val] = checked;
      });

      if( $('.js-sub:checked').length <= 0 ) {
        unsubscribed = true;
      }

      $.each(state.fieldsChanged, function(i, el) {
        var name = $(this).attr('id');
        var val = $(this).val();
        state.fieldsChanged[i] = name + '=' + val;
      });

      var ajaxurl = self.form.find('input[name="ajaxurl"]').val();
      var nonce = self.form.find('input[name="nonce"]').val();
      var guid = self.form.find('input[name="guid"]').val();

      var data = {
        email: state.email,
        oldEmail: state.oldEmail,
        nonce: nonce,
        action: 'preference_center',
        guid: guid,
        refurl: encodeURI(document.location.href),
        subscriptions: listOfSubscribed,
        preferences: state.checked,
        fields: state.fieldsChanged,
        unsubscribed: unsubscribed
      };
      self.submitForm(ajaxurl, data);
      
    };

    this.init = function() {
      state.oldEmail = this.form.find('#Email').val();
      preferenceCheckbox.on('change', pushCheckbox);
      fields.on('change', pushFields);
      email.on('change', pushEmail);
      unsub.on('click', removeAllNewsletters);

      var validator = new FormValidator( 'preference', 
        [
        { name: 'Email', rules: 'valid_email'},
        { name: 'FirstName', rules: 'alpha', display : 'First Name'},
        { name: 'Bm', rules: 'numeric|max_length[2]'},
        { name: 'Bd', rules: 'numeric|max_length[2]'},
        { name: 'State', rules: 'alpha|max_length[2]'},
        { name: 'Pc', rules: 'alpha_numeric'}
        ], 
        function(errors, event) {
        if( errors.length > 0 ) {
          $.each(errors, function(i, err) {
            $(err.element).next('.help-block').html(err.message);
          })
        } else {
          event.preventDefault();
          self.formSubmission();
        }
      })

    };
  };

  if(document.getElementById('pc-wrapper')) {
    var preference = new PreferenceTest(document.getElementById('pc-wrapper'));
    preference.init();
  }
})(jQuery);