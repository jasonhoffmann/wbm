<?php if( !is_front_page() ) { ?>

<div class="grid">
	<?php if(is_category('podcasts')) { ?> 
		<a href="https://geo.itunes.apple.com/us/podcast/waterbrook-multnomah-publishers/id373576923?mt=2" style="display:inline-block;overflow:hidden;background:url(http://linkmaker.itunes.apple.com/images/badges/en-us/badge_itunes-lrg.svg) no-repeat;width:165px;height:40px;"></a>
	<?php } ?>

	<?php 
	$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
	if( $url = strpos( $url,'author' ) ) { ?>
		<div class="newsletter-side span_12">
			<?php get_template_part( 'includes/templates/partials/_newsletter', 'author' ); ?>
		</div>
	<?php } else { ?>
		<div class="newsletter-side span_12">
			<?php get_template_part( 'includes/templates/partials/_newsletter', 'form' ); ?>
		</div>
	<?php } ?>

<?php } ?>
	<div class="ad-side span_12">
		<?php
			adsanity_show_ad_group(
				array(
					'group_ids'	=> array(30732), // an array of valid group ids (you can get these from the main list of ad groups
					'num_ads'	=> 999, // number of ads to show total
					'num_columns'	=> 1 // number of ads to show per row
				)
			);
		?>
	</div>
</div>