<?php
/**
 * The template for displaying the footer.
 *
 * @package WaterBrook Multnomah
 */

?>
<footer class="footer">
	<div class="container grid footer-wrap">
		<div class="span_3 footer-item footer-copy">
			<ul class="list-unstyled">
				<li>&copy; <?php echo date('Y'); ?> WaterBrook & Multnomah</li>
				<li>Imprints of Penguin Random House</li>
				<li class="nav-links-social">
					<a target="_blank" href="https://www.facebook.com/WaterBrookMultnomah" class="icon icon-social-facebook">
						<svg viewBox="0 0 100 100" class="icon">
						  <use xlink:href="#social-facebook"></use>
						</svg>
					</a>
					<a target="_blank" href="https://twitter.com/WaterBrookPress" class="icon icon-social-twitter">
						<svg viewBox="0 0 100 100" class="icon">
						  <use xlink:href="#social-twitter"></use>
						</svg>
					</a>
					<a target="_blank" href="https://instagram.com/waterbrookmultnomah/" class="icon icon-social-instagram">
						<svg viewBox="0 0 100 100" class="icon">
						  <use xlink:href="#social-instagram"></use>
						</svg>
					</a>
					<a target="_blank" href="https://www.youtube.com/user/WaterBrookMultnomah" class="icon icon-social-youtube">
						<svg viewBox="0 0 100 100" class="icon">
						  <use xlink:href="#social-youtube"></use>
						</svg>
					</a>
			</ul>
		</div>
		<div class="span_9 footer-item">
			<ul class="list-unstyled footer-links">
				<li><a href="<?php echo home_url( '/about-us/'); ?>">About Us</a></li>
				<li><a href="<?php echo home_url( '/about-us/#contact-us'); ?>">Contact Us</a></li>
				<li><a href="<?php echo home_url( '/about-us/#careers'); ?>">Careers</a></li>
				<li><a href="<?php echo home_url( '/frequently-asked-questions/'); ?>">FAQ</a></li>
				<li><a href="<?php echo home_url( '/category/news/'); ?>">News</a></li>
				<li><a href="<?php echo home_url( '/catalogs/'); ?>">Catalogs</a></li>
				<li><a href="<?php echo home_url( '/purchasing/'); ?>">Purchasing</a></li>
				<li><a href="<?php echo home_url( '/purchasing-churches-ministries/'); ?>">How to Order</a></li>
				<li><a target="_blank" href="http://www.penguinrandomhouse.com/terms/">Terms of Use</a></li>
				<li><a target="_blank" href="http://www.penguinrandomhouse.com/privacy/">Privacy Policy</a></li>
			</ul>
		</div>
	</div>
</footer>
<?php wp_footer(); ?>
</body>
</html>

