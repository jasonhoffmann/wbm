<?php
/**
 * File for the Main Blog Page
 *
 * Corresponds to the "Blog" page
 * Set in Settings -> Reading
 *
 * @package WaterBrook Multnomah
 */

get_header(); ?>
<div class="container bg">

<?php wp_nav_menu( array( 'theme_location' => 'blog-menu', 'container_class' => 'sub-nav', 'menu_class' => 'list-unstyled' ) ); ?>

<main class="main grid">


	<section class="index-blog span_9">

	<?php if( have_posts() ) { 
			while( have_posts() ) {  the_post();

				get_template_part('includes/templates/partials/_blog', 'index');
			
		 	}

		global $wp_query;
		$big = 999999999;

		$paginate_links = paginate_links( array(
			'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
			'format' => '?paged=%#%',
		    'current' => max( 1, get_query_var('paged') ),
		    'total' => $wp_query->max_num_pages,
		    'mid_size' => 5
		) );

		if ( $paginate_links ) {
		    echo '<div class="pagination">';
		    echo $paginate_links;
		    echo '</div>';
		}

	 	} ?>

	</section>

	<aside class="index-side span_3">
		<?php get_sidebar(); ?>
	</aside>

</main>

</div>


<?php get_footer(); ?>
