<?php
/**
 * The header for our theme.
 *
 * @package WaterBrook Multnomah
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
<noscript><link href="<?php echo get_template_directory_uri() . '/assets/svg/output/icons.fallback.css'; ?>" rel="stylesheet"></noscript>
<link rel="stylesheet" type="text/css" href="https://cloud.typography.com/6814294/6171152/css/fonts.css" />
</head>

<body <?php body_class(); ?>>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=1525612354364158";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div style="display:none;">
	<?php echo file_get_contents(get_template_directory() . '/assets/svg/svg/sprite.defs.svg'); ?>
</div>
<?php do_action('wbm_body_top', '', $wp_query); ?>
<div class="header-wrapper">

	<header class="header container container-nav grid">
		<div class="header-logo span_4">
			<a href="<?php echo home_url(); ?>">
				<img class="logo" src="<?php echo get_template_directory_uri(); ?>/assets/images/wm_logo.png">
			</a>
		</div>

		<a href="#nav" id="toggle-nav">Toggle navigation</a>
		<nav id="nav" class="span_8 nav-wrapper cf">
		<?php $url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']; ?>
		<ul class="list-unstyled nav-links nav span_7">
			<li class="nav-links-item nav-links-top allow-hover <?php if (strpos($url,'books') || strpos($url,'book')) { echo 'active'; } ?>">
				<a class="top-link" href="<?php echo home_url('/books/'); ?>">Books</a>
				<ul class="nav-links-children">
					<li class="nav-links-item"><a href="<?php echo home_url('/books/category/new-releases/'); ?>">New Releases</a></li>
					<li class="nav-links-item"><a href="<?php echo home_url('/books/category/best-sellers/'); ?>">Best Sellers</a></li>
					<li class="nav-links-item"><a href="<?php echo home_url('/books/category/coming-soon/'); ?>">Coming Soon</a></li>
					<li class="nav-links-item"><a href="<?php echo home_url('/books/#browse'); ?>">Browse By Category</a></li>
				</ul>
			</li>
			<li class="nav-links-item nav-links-top <?php if (strpos($url,'author') || strpos($url,'authors')) { echo 'active'; } ?>"><a class="top-link" href="<?php echo home_url('/authors/'); ?>">Authors</a></li>
			<li class="nav-links-item nav-links-top allow-hover <?php if (strpos($url,'blog') || is_single()) { echo 'active'; } ?>">
				<a class="top-link" href="<?php echo home_url('/blog/'); ?>">Blog</a>
				<ul class="nav-links-children">
					<li class="nav-links-item"><a href="<?php echo home_url('/category/news/'); ?>">News</a></li>
					<li class="nav-links-item"><a href="<?php echo home_url('/category/podcasts'); ?>">Podcasts</a></li>
					<li class="nav-links-item"><a href="<?php echo home_url('/category/sneak-peek'); ?>">Sneak Peek</a></li>
					<li class="nav-links-item"><a href="<?php echo home_url('/category/video'); ?>">Videos</a></li>
				</ul>
			</li>
			<li class="nav-links-item nav-links-top allow-hover <?php if (is_page('about-us')) { echo 'active'; } ?>">
				<a class="top-link" href="<?php echo home_url('/about-us/'); ?>">About Us</a>
				<ul class="nav-links-children">
					<li class="nav-links-item"><a href="<?php echo home_url('/about-us/#recent-news'); ?>">In the News</a></li>
					<li class="nav-links-item"><a href="<?php echo home_url('/about-us/#contact-us'); ?>">Contact Us</a></li>
					<li class="nav-links-item"><a href="<?php echo home_url('/about-us/#careers'); ?>">Careers</a></li>
					<li class="nav-links-item"><a href="<?php echo home_url('/frequently-asked-questions/'); ?>">FAQ</a></li>
					<li class="nav-links-item"><a href="<?php echo home_url('/purchasing/'); ?>">Purchasing</a></li>
				</ul>
			</li>
		</ul>
		<div class="header-search">
			<div id="invserchbtn"></div>
			<?php echo get_search_form(); ?>
		</div>

		<div class="header-social">
		<a class="cart-btn btn btn-default" href="https://cart.penguinrandomhouse.com/cart.php">
			<svg viewBox="0 0 100 100" class="icon">
			  <use xlink:href="#shopping-cart"></use>
			</svg><span class="visible-lg">My Cart</span>
		</a>
		</div>

		<div class="nav-links-social nav-links-header">
			<a target="_blank" href="https://www.facebook.com/WaterBrookMultnomah" class="icon icon-social-facebook">
				<svg viewBox="0 0 100 100" class="icon">
				  <use xlink:href="#social-facebook"></use>
				</svg>
			</a>
			<a target="_blank" href="https://twitter.com/WaterBrookPress" class="icon icon-social-twitter">
				<svg viewBox="0 0 100 100" class="icon">
				  <use xlink:href="#social-twitter"></use>
				</svg>
			</a>
			<a target="_blank" href="https://instagram.com/waterbrookmultnomah/" class="icon icon-social-instagram">
				<svg viewBox="0 0 100 100" class="icon">
				  <use xlink:href="#social-instagram"></use>
				</svg>
			</a>
			<a target="_blank" href="https://www.youtube.com/user/WaterBrookMultnomah" class="icon icon-social-youtube">
				<svg viewBox="0 0 100 100" class="icon">
				  <use xlink:href="#social-youtube"></use>
				</svg>
			</a>
		</div>


		
		
		</nav>
	</header>
</div>


