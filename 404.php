<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package WaterBrook Multnomah
 */


get_header();

$newreleases_params = array(
	'params' => array(
		'rows' => 5,
		'showNewReleases' => 'true'
		)
	);

$get_books = new RandomHouse;
$books = $get_books->books( $newreleases_params );
 ?>
 <div class="container bg cf">

 <main class="main fourohfour">
		<h3>Unfortunately, the page you were looking for does not exist. You can try searching through our titles, or browse our most recent titles below.</h3>
		
		<div class="fourohfour-search">
			<?php get_search_form(); ?>
		</div>
		<div class="book_col5 fourohfour-list">
			<?php render_template( 'partials/_book-list.php', array( 'books' => $books, 'title' => 'New Releases', 'link' => home_url('books/category/new-releases') ) ); ?>
		</div>
</main>
</div>

<?php get_footer(); ?>
