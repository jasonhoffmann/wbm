<?php
/**
  * Template Name: About Us Page
  *
  * @package WaterBrook Multnomah
  * @since 1.0
  */

get_header();
 ?>
<div class="container bg">

<main class="main grid">

	<div class="single-item span_9 tab-content entry-item">
	<ul id="tab-hash" class="tab-nav list-unstyled push-bottom" role="tablist">
		<li role="presentation" class="active"><a href="#about-wbm" aria-controls="about-wbm" role="tab" data-toggle="tab">About WaterBrook & Multnomah</a></li>
		<li role="presentation"><a href="#recent-news" aria-controls="recent-news" role="tab" data-toggle="tab">Recent News</a></li>
		<li role="presentation"><a href="#contact-us" aria-controls="contact-us" role="tab" data-toggle="tab">Contact Us</a></li>
		<li role="presentation"><a href="#careers" aria-controles="careers" role="tab" data-toggle="tab">Careers</a></li>
	</ul>

	<section role="tabpanel" class="active tab-pane" id="about-wbm">

		<?php while( have_posts() ) {  the_post(); ?>


			<article>
			<h3 class="index-page-title"><?php the_title(); ?></h3>

				<div class="entry-content">
					<?php the_content(); ?>
				</div>
			</article>

		<?php } ?>
	</section>

	<section role="tabpanel" class="tab-pane" id="recent-news">
		<?php
			$news_query = new WP_Query(array(
				'post_type' => 'post',
				'post_status' => 'publish',
				'category_name' => 'in-the-news',
				'posts_per_page' => 5
				));
			if($news_query->have_posts()) {
				while($news_query->have_posts()) {
					$news_query->the_post();
						get_template_part('includes/templates/partials/_blog', 'index');
			}
				}
			?>
		<div class="cf text-center">
			<a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>" class="btn btn-default about-blog-btn">View More From The Blog</a>
		</div>
	</section>

	<section role="tabpanel" class="tab-pane" id="contact-us">
			<h3>Contact Us:</h3>
			<p>Questions that are answered in the <a href="<?php echo home_url('/frequently-asked-questions/'); ?>">FAQ</a> may not receive an answer. We try to answer all the e-mails we receive within 1-2 weeks.</p>
			<?php gravity_form(8, false, false, false, false, true); ?>

			<h4>By Mail</h4>
			<p class="push-bottom">WaterBrook & Multnomah <br />10807 New Allegiance Drive Suite 500 <br />Colorado Springs, CO 80921</p>

			<h4>By Phone</h4>
			<p class="push-bottom">Toll Free: 1-800-603-7051 <br />Tel: 719-590-4999 <br />Fax: 719-590-8977</p>
			<div class="google-map-container">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3100.2255553547748!2d-104.81889838592188!3d39.010167953632184!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x87134d3df2a3b033%3A0xc8e9d3284c8c7034!2sWater+Brook+Press!5e0!3m2!1sen!2sus!4v1447346908035" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>

	</section>

	<section role="tabpanel" class="tab-pane" id="careers">
		<h3>Careers at WaterBrook & Multnomah Publishers</h3>
		<p class="push-bottom">Are you looking for employment with a top Christian book publisher? Are you interested in a career working with some of the best Evangelical Christian authors, agents, editors, marketers and publicists in the Christian book industry?</p>
    <p class="text-center push-bottom">
      <a href="http://careers.penguinrandomhouse.com/" target="_blank" class="btn btn-primary">Search Open Positions</a>
    </p>
		<p>You can connect with WaterBrook & Multnomah on <a href="http://www.facebook.com/WaterBrookMultnomah" target="_blank">Facebook</a>, <a href="http://www.twitter.com/WaterBrookPress" target="_blank">Twitter</a>, <a href="http://www.YouTube.com/waterbrookmultnomah" target="_blank">YouTube</a>, <a href="http://www.godtube.com/waterbrookmultnomah/" target="_blank">GodTube</a>, <a href="http://www.Livestream.com/WaterBrookMultnomah" target="_blank">Livestream</a>, <a href="http://www.Scribd.com/WaterBrook" target="_blank">Scribd</a>, <a href="http://www.linkedin.com/company/429766?trk=tyah" target="_blank">LinkedIn</a> and <a href="https://plus.google.com/105345693052962050225#105345693052962050225/posts" target="_blank">Google+</a></p>

		<p>We also invite you to check out some of these helpful resources:</p>
		<ul>
			<li><a href="<?php echo home_url('/catalogs/'); ?>">WaterBrook Multnomah Catalogs</a></li>
			<li><a href="<?php echo home_url('/blog/category/news/'); ?>">Our authors and books in the news</a></li>
			<li><a href="<?php echo home_url('/books/category/new-releases/'); ?>">New Releases</a></li>
			<li><a href="<?php echo home_url('/books/category/best-sellers/'); ?>">Best Sellers</a></li>
		</ul>
	</section>


	</div>


	<aside class="index-side span_3">
		<?php get_sidebar(); ?>
	</aside>

</main>

</div>


<?php get_footer(); ?>
