<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package _s
 */
get_header(); ?>

<?php 	
$tab = $_GET['source'] ? $_GET['source'] : 'books';
 ?>

 <div class="container bg">

 <main class="main">
 	<h2 class="page-title"><?php printf( esc_html__( 'Search Results for: %s', '_s' ), '<span>' . get_search_query() . '</span>' ); ?></h1>

 <div class="grid span_12">

 <?php if($tab === 'books') { 
 	$term = urlencode(get_search_query());
 	$page = $_GET['page'] ? $_GET['page'] : 1;
 	$rows = 10;
 	$start = ( $page - 1 ) * $rows;
 	if( $start < 0 ) { $start = 0; }
 	$get_books = new RandomHouse;
 	$search_books = $get_books->search(  array(
 			'term' => $term,
 			'start' => $start
 		) );
 	if(!$search_books) { 
 		$redirect_url = add_query_arg( array( $wp->query_string => '', 'source' => 'news'), home_url( $wp->request ) );
 		wp_redirect($redirect_url);
 	}
 	$total = intval($get_books->get_record_count()); ?>

		<aside class="search-side span_3 cf">
			 <form class="filter-sec form form-stacked" id="facets">
				 <?php 
				 	$facets = $get_books->build_facets();
				  ?>
					<div class="facets-group">
				  	<h4>Formats</h4>
					  <?php foreach($facets['format'] as $format => $count) {?>
					  <div class="input-control checkbox">
					  	<label for="format-<?php echo $format; ?>" class="picker-label">
					  		<input id="format-<?php echo $format; ?>" name="formats[]" class="checkbox picker-element" value="<?php echo $format; ?>" type="checkbox">
					  		<span class="control-indicator"></span>
					  		<?php echo $format; ?>
					  	</label>
					  </div>
					  <?php } ?>
					</div>

					<div class="facets-group">
				  	<h4>Categories</h4>
					  <?php foreach($facets['categoryLabel'] as $format => $count) {?>
					  <div class="input-control checkbox">
					  	<label for="<?php echo $format; ?>" class="picker-label">
					  		<input id="<?php echo $format; ?>" name="categories[]" class="checkbox picker-element" value="<?php echo $format; ?>" type="checkbox">
					  		<span class="control-indicator"></span>
					  		<?php echo $format; ?>
					  	</label>
					  </div>
					  <?php } ?>
					</div>

					<div class="facets-group">
				  	<h4>Age Range</h4>
					  <?php foreach($facets['ageRange'] as $format => $count) {?>
					  <div class="input-control checkbox">
					  	<label for="ages-<?php echo $format; ?>" class="picker-label">
					  		<input id="ages-<?php echo $format; ?>" name="ages[]" class="checkbox picker-element" value="<?php echo $format; ?>" type="checkbox">
					  		<span class="control-indicator"></span>
					  		<?php echo $format; ?>
					  	</label>
					  </div>
					  <?php } ?>
					</div>

					<div class="facets-group">
					<h4>Imprints</h4>
					  <?php foreach($facets['imprint'] as $format => $count) {?>
					  <div class="input-control checkbox">
					  	<label for="<?php echo $format; ?>" class="picker-label">
					  		<input id="<?php echo $format; ?>" name="imprint[]" class="checkbox picker-element" value="<?php echo $format; ?>" type="checkbox">
					  		<span class="control-indicator"></span>
					  		<?php echo normalize_imprint( $format ); ?>
					  	</label>
					  </div>
					  <?php } ?>
					</div>

					<div class="facets-group">
					<h4>On-Sale Date</h4>
					  	<div class="input-control checkbox">
						  	<label for="new-release" class="picker-label">
						  		<input id="new-release" name="release[]" class="checkbox picker-element" value="new-release" type="checkbox">
						  		<span class="control-indicator"></span>
						  		New Release
						  	</label>
						</div>
						<div class="input-control checkbox">
						  	<label for="forthcoming" class="picker-label">
						  		<input id="forthcoming" name="release[]" class="checkbox picker-element" value="forthcoming" type="checkbox">
						  		<span class="control-indicator"></span>
						  		Forthcoming Release
						  	</label>
						</div>
					</div>

					<input id="searchTerm" name="searchTerm" value="<?php echo urlencode(get_search_query()); ?>" type="hidden">
			</form>
		</aside>

	 	<section class="span_9 tab-content">
		 	<ul class="tab-nav m-left list-unstyled">
		 		<li class="active"><span>Books</span></li>
		 		<li><a href="<?php echo home_url( add_query_arg(array('s' => get_search_query(), 'source' => 'news' )) ); ?>">News</a></li>
		 	</ul>
		 	<div class="m-border-top search-page" id="searchCont">
				<?php render_template( 'partials/_search-results.php', array( 'books' => $search_books, 'total' => $total) ); ?>
		 		<?php echo base_pagination( $page, ceil( $total / 10), '?page=%#%', add_query_arg('page', '%#%') ); ?>
		 	</div>
	 </section>

<?php } elseif($tab === 'news') { ?>

	<aside class="search-side span_3 search-news-sidebar">
		<?php get_sidebar(); ?>
	</aside>

	<section class="span_9 tab-content search-news-list">
		<ul class="tab-nav m-left list-unstyled">
			<li><a href="<?php echo home_url( add_query_arg(array('s' => get_search_query(), 'source' => 'books' )) ); ?>">Books</a></li>
			<li class="active"><span>News</span></li>
		</ul>

		<div class="m-border-top search-results search-page" id="searchCont">
		 		<?php if( have_posts() ) { 
		 				while( have_posts() ) {  the_post();
		 					get_template_part('includes/templates/partials/_blog', 'index');
		 				
		 			 	}
		 			global $wp_query;
		 			base_pagination(get_query_var('paged'), $wp_query->max_num_pages); 
		 		 	} else { ?>
		 		 		<p>Sorry, there are no results for this search.</p>
		 		 	<?php } ?>
		</div>
	</section>
<?php } ?>
 	</main>
 </div>
<?php get_footer(); ?>