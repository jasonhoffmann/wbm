<?php
/**
 * The main template file.
 *
 * @package WaterBrook Multnomah
 */

get_header(); ?>


<div class="index container bg">
<?php 
$now = time();
$ads = new WP_Query(
			array(
				'post_type' => 'ads',
				'tax_query' => array(
					array(
						'taxonomy' => 'ad-group',
						'field' => 'id',
						'terms' => implode( ',', array('30733') )
					)
				),
				'meta_query' => array(
					array(
						'key' => '_start_date',
						'value' => $now,
						'type' => 'numeric',
						'compare' => '<='
					),
					array(
						'key' => '_end_date',
						'value' => $now,
						'type' => 'numeric',
						'compare' => '>='
					)
				),
				'posts_per_page' => '1',
				'orderby' => 'rand'
			)
		); 
?>
<?php if($ads->have_posts()) { ?>
	<div class="leaderboard">

		<div class="leaderboard-image">
		<?php
			$ads = adsanity_show_ad_group(
					array(
						'group_ids'	=> array(30733),
						'num_ads'	=> 1, 
						'num_columns'	=> 1
					)
				);
		?>
		</div>
	</div>
<?php } ?>
<section class="main">
	<div class="cf book-list-tab-content tab-content">
		<?php get_template_part( 'includes/templates/partials/_book-list', 'tabs' ); ?>
	</div>

	<div class="cf">
		<?php get_template_part( 'includes/templates/partials/_newsletter', 'form' ); ?>
	</div>

	<div class="grid">

	<?php 	$blog_args = array(
					'post_type' => 'post',
					'post_status' => 'publish',
					'posts_per_page' => 5
			);
			$blog_query = new WP_Query( $blog_args );	?>

		<section class="index-blog span_9">

			<?php 	if( $blog_query->have_posts() ) {
						while( $blog_query->have_posts() ) { $blog_query->the_post();

							get_template_part('includes/templates/partials/_blog', 'index');

				 		}
				 	} 	?>

			<a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>" class="btn btn-default index-blog-btn">View More From The Blog</a>
		</section>

		<aside class="index-side span_3">
			<?php get_sidebar(); ?>
		</aside>

	</div>

</section>

</div>


<?php get_footer(); ?>