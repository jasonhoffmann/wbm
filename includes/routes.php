<?php
/**
* Sets up our Routes for book and related pages
* Need to perform a quick conditional check to see if Yoast SEO is installed
* 
* @see Route - https://github.com/Upstatement/routes
* @since 1.0
*/

function change_title($title) {
	if ( function_exists( 'initialize_wpseo_front' ) ) {
		add_filter( 'wpseo_title', function() use($title) {
			return $title . ' - WaterBrook & Multnomah';
		});
	} else {
		add_filter( 'pre_get_document_title', function() use($title) {
			return $title . ' - WaterBrook & Multnomah';
		});
	}

}



/**
* Our basic books page
* Used to show the books index page, with list of categories
* 
* @since 1.0
*
* @see get_genre()
*/
Routes::map('books', function() {
	$newreleases_params = array(
		'params' => array(
			'rows' => 5,
			'showNewReleases' => 'true'
			)
		);

	$bestsellers_params = array(
			'params' => array(
				'rows' => 5,
				'minInPrint' => 60000,
				'sort' => 'printScore'
			)
		);
	$currentDate = date('Y/m/d');
	$comingsoon_params = array(
		'params' => array(
			'rows' => 5,
			'showComingSoon' => true
			)
		);

	$get_books = new RandomHouse;
	$bestsellers = $get_books->books( $bestsellers_params );

	$get_books = new RandomHouse;
	$newreleases = $get_books->books( $newreleases_params );

	$get_books = new RandomHouse;
	$comingsoon = $get_books->books( $comingsoon_params );

	$categories = get_genre();

	change_title('Books');
	Routes::load( get_template_directory() . '/includes/templates/books/index.php', array( 
		'categories' => $categories,
		'bestsellers' => $bestsellers,
		'newreleases' => $newreleases,
		'comingsoon' => $comingsoon
		) 
	);
});



/**
* Our basic books page
* Used to show the books index page, with list of categories
* 
* @since 1.0
*
* @see get_cat_ids_by_slug()
* @see get_genre
*
* @param string :category The name of our category
*/
Routes::map('books/category/:category', function($params) {
	$category_slug = $params['category'];

	// Determine what page we're on, used for pagination
	$page = intval( $_GET['page'] );
	$rows = 20;
	$start = ( $page - 1 ) * $rows;
	$comingsoon = false;
	if( $start < 0 ) { $start = 0; }

	// Set up default parameters
	$params = array(
		'params' => array(
			'suppressRecordCount' => false,
			'start' => $start
		)
	);

	// Do a big check here to see if we're on umbrella category (i.e. New Releases)
	// Add extra parameters
	if( $category_slug == 'new-releases') {
		$extra_params = array(
			'params' => array(
				'showNewReleases' => 'true'
			)
		);

		$title = 'New Releases';
	} elseif($category_slug == 'best-sellers') {
		$extra_params = array(
			'params' => array(
				'minInPrint' => 60000,
				'sort' => 'printScore'
			)
		);

		$title = 'Best Sellers';

	} elseif($category_slug == 'coming-soon') {
		$comingsoon = true;
		$currentDate = date('Y/m/d');
		$extra_params = array(
			'params' => array(
				'onSaleFrom' => $currentDate,
				'dir' => 'asc',
				'rows' => 0
			),
			'newest_date' => true
		);

		$title = 'Coming Soon';

	} elseif($category_slug == 'award-winners') {
		$extra_params = array(
			'params' => array(
				'showAwards' => 'true'
			)
		);

		$title = 'Award Winners';

	} else {
		$cat_ids = get_cat_ids_by_slug( $category_slug );
		$extra_params = array(
			'categories' => $cat_ids
		);

		// Get title
		$title = get_genre($category_slug);
	}

	// Merge in our extra parameters
	$params = array_merge_recursive($params, $extra_params);


	// Perform Request to PRH API
	$get_books = new RandomHouse;
	$books = $get_books->books( $params );
	$record_count = $get_books->get_record_count();
	change_title($title);

	// Load template, pass in parameters
	Routes::load( get_template_directory() . '/includes/templates/books/list.php', array( 
		'category' => $category_slug, 
		'books' => $books, 
		'record_count' => $record_count, 
		'page' => $page, 
		'title' => $title,
		'comingsoon' => $comingsoon
		) 
	);
});



/**
* Series list page
* Used to show the series list page, no pagination
* 
* @since 1.0
*
* @param string :seriesCode The codeID of our series
* @param string :seriesName the slug of our series
*/
Routes::map('series/:seriesCode/:seriesName', function($params) {

	// Set basic parameters
	$series_slug = $params['seriesCode'];
	$page = intval( $_GET['page'] );
	$rows = 0;

	// Perform request to API
	$get_books = new RandomHouse;
	$books = $get_books->series(array(
		'id' => $series_slug,
		'params' => array(
			'suppressRecordCount' => false,
			'rows' => 0
		)
		));
	$record_count = $get_books->get_record_count();
	$title = $get_books->get_series_title();

	change_title($title);

	// Load template, pass in parameters
	Routes::load( get_template_directory() . '/includes/templates/books/series.php', array( 
		'title' => $title, 
		'books' => $books, 
		'record_count' => $record_count 
		) 
	);
});



/**
* Individual Book Page
* Used to show individual book pages
* 
* @since 1.0
*
* @param string :isbn ISBN or WorkID of Title
* @param string :slug book slug
*/
Routes::map('books/:isbn/:slug', function($params) {

	// Collect Parameters
	$isbn = $params['isbn'];
	$slug = $params['slug'];

	// Perform New Request
	$get_book = new RandomHouse;
	$book = $get_book->book(
		array(
			'full' => true,
			'id' => $params['isbn'],
			'params' => array(
				'catSetId' => 'BI'
				)
			)
		);

	if($book) {


		$content = $get_book->content(
			array(
				'id' => $book->isbn
				)
			);


		// Grab Events for the title
		// TODO: Sort out why events are broken
		// $events = $get_book->events(
		// 	array(
		// 		'id' => $params['isbn']
		// 	)
		// );
		$events = false;

		$get_books = new RandomHouse;
		$also_like = $get_books->books(
			array(
				'categories' => $book->catIDs,
				'params' => array(
					'ignoreWork' => $isbn,
					'rows' => 5,
					'showPublishedBooks' => 1
					)
				)
			);
		if($book->series) {
			$series = $get_book->series( array( 'id' => $book->series->code, 'limit' => true ) );
		}
		wp_reset_postdata();
		$related_query = new WP_Query( array(
				'post_type' => 'post',
				'post_status' => 'publish',
				'meta_key' => 'isbn',
				'meta_value' => $book->isbn,
				'category__not_in' => array( 2939 )
				
			) );
			
			wp_reset_postdata();
			$sneak_peek_query = new WP_Query( array(
					'post_type' => 'post',
					'post_status' => 'publish',
					'meta_key' => 'isbn',
					'meta_value' => $book->isbn,
					'category__in' => array( 2939 ),
					'posts_per_page' => 1	
				) );

		$reviews = get_reviews( $book->isbn );

		change_title($book->title . ' by ' . $book->author);
		// Load template, pass in parameters
		Routes::load( get_template_directory() . '/includes/templates/books/single.php', array( 
				'isbn' => $isbn, 
				'slug' => $slug, 
				'book' => $book, 
				'events' => $events, 
				'series' => $series,
				'related_query' => $related_query,
				'sneak_peek' => $sneak_peek_query,
				'also_like' => $also_like,
				'reviews' => $reviews,
				'content' => $content
			) 
		);

	} else {
		Routes::load( get_template_directory() . '/404.php', array() );
	}
});

/**
* Authors Index Page
* Used to show featured authors
* 
* @since 1.0
*
*/
Routes::map('authors', function($params) {
	// For now author IDs will simply be listed.
	// TODO: Move this to admin interface

	$get_authors = new RandomHouse;
	$authors = $get_authors->authors(
		array(
			'authorIDs' => array('2136445', '12930', '117906', '244262', '74956', '162648', '25667', '72213', '223991', '75404', '275', '74998', '306666', '885', '303990', '75043')
		)
	);

	change_title('Authors');
	Routes::load( get_template_directory() . '/includes/templates/authors/index.php', array( 
			'authors' => $authors
		) 
	);
});

/**
* Authors List Page
* Used to show a list of authors
* 
* @since 1.0
*
*/
Routes::map('authors/all', function($params) {
	$page = intval( $_GET['page'] );
	$rows = 24;
	$start = ( $page - 1 ) * $rows;
	if( $start < 0 ) { $start = 0; }

	$get_authors = new RandomHouse;
	$authors = $get_authors->authors(
		array(
			'params' => array(
				'start' => $start,
				'rows' => $rows,
				'sort' => 'authorLast',
				'dir' => 'asc'
			)
		)
	);
	$record_count = $get_authors->get_record_count();

	change_title('All Authors');
	Routes::load( get_template_directory() . '/includes/templates/authors/list.php', array( 
			'authors' => $authors,
			'record_count' => $record_count,
			'page' => $page
		) 
	);
});

/**
* Authors Detail Page
* Used to show individual author
* 
* @since 1.0
*
* @param string :isbn ISBN or WorkID of Title
* @param string :slug book slug
*/
Routes::map('authors/:author_id/:author_slug', function($params) {
	$author_id = $params['author_id'];

	$options = array(
		'authorID' => $author_id
		);

	$get_author = new RandomHouse;
	$author = $get_author->author( $options );

	$related_query = new WP_Query( array(
			'post_type' => 'post',
			'post_status' => 'publish',
			'meta_key' => 'authorid',
			'meta_value' => $author->authorId
		) );

	change_title( $author->name );
	Routes::load( get_template_directory() . '/includes/templates/authors/single.php', array( 
			'author' => $author,
			'news' => $related_query
		) 
	);
});

/**
* Events Index
* Used to show a list of events
* 
* @since 1.0
*
* @param string :isbn ISBN or WorkID of Title
* @param string :slug book slug
*/
// Routes::map('events', function($params) {

// 	$get_events = new RandomHouse;
// 	$events = $get_events->all_events();
// 	change_title( 'Events' );
// 	Routes::load( get_template_directory() . '/includes/templates/authors/single.php' );
// });