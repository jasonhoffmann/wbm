<?php
/*
 * RH Ajax Request Class for Email Preference Center
 *
 * Usage:
 * Create an instance of the object and pass in the three params:
 *   'newsletter' or 'preferences'
 *   nonce associative array key
 *   http host for curly
 *
 * Then add_action() passing in an array where index 0: instance of RHAjax and 'run' as the execution function
 *
 * Example:
 *   $rif_newsletter_ajax = new RHAjax('newsletter', 'rif-newsletter', "readitforward.dev.us.randomhouse.com");
 *   add_action( 'wp_ajax_rif_newsletter', array($rif_newsletter_ajax, 'run') );
 *   add_action( 'wp_ajax_nopriv_rif_newsletter', array($rif_newsletter_ajax, 'run') );
 *
 */
class RHAjax
{
    // Declare object variables.  $ajax_request_type should refe
    private $ajax_request_type;
    private $nonce_request_name;

    /*
     * Acceptable ajax request types:
     *  'newsletter'
     *  'preferences'
     * 
    */

    private $base_endpoint = 'https://services-origin.penguinrandomhouse.com/rest/v1/json';
    private $set_sub = '/subscription/programsubscribejson/set';
    private $set_profile = '/profile/setprofilejsondata/set';
    private $set_change_email = '/subscription/changeemailaddress/set';
    private $get_preferences = '/profile/getprofilejsondata/get';
    private $http_host = 'waterbrook.dev.randomhouse.com';

    private $siteID = '504';
    private $programID = '50401';
    private $programIDs = array('50401', '50402', '50403', '50404');

    // Initialize instance variables.
    public function __construct($ajax_request_type = '', $nonce_request_name = '')
    {
        $this->ajax_request_type = $ajax_request_type;
        $this->nonce_request_name = $nonce_request_name;

    }

    //Depending on the ajax request type, execute request.
    public function run() {
        if ($this->ajax_request_type == 'newsletter') {
            $this->newsletter_request();
        } elseif ($this->ajax_request_type == 'preferences') {
            $this->preferences_request();
        } elseif( $this->ajax_request_type == 'author' ) {
            $this->author_request();
        }
    }

    //Verify the nonce.
    static function verify_nonce($action)
    {
        $nonce = sanitize_text_field($_REQUEST['nonce']);

        if ( ! wp_verify_nonce($nonce, $action) ) {
            wp_send_json_error();
            return false;
        } else {
            return true;
        }
    }

    public function author_request()
    {
        // Verify nonce
        $nonce_status = self::verify_nonce($this->nonce_request_name);

        // If Nonce is correct, assemble the url append and execute curly.
        if ($nonce_status) {

            // Build endpoint and request string
            $newsletter_endpoint = $this->base_endpoint . $this->set_sub;
            $author = $_REQUEST['author'];

            if($author) {
                $author_name = urlencode($_REQUEST['author_name']);
                $author_ID = $_REQUEST['authorID'];
                $subs = '301,30102,1&Prefs=301,30102,30116,' . $author_ID . ',' . $author_name;
                $subs_return = '302|30102';
            } else {
                $subs = $this->siteID . ',' . $this->programID . ',1';
                $subs_return = $this->siteID . '|' . $this->programID;
            }
            
            
            $fields_string = '';
            $new_subscribe = $_REQUEST['newSubscribe'];


            $params = array(
                'Email' => urlencode($_REQUEST['email']),
                'Subs' => $subs,
                'ReturnMcg' => '1',
                'BrowserAgent' => urlencode($_SERVER['HTTP_USER_AGENT']),
                'Refurl' => urlencode($_REQUEST['refurl'])
            );

            foreach ($params as $key => $value) {
                $fields_string .= $key . '=' . $value . '&';
            }
            $fields_string = rtrim($fields_string, '&');

            // Execute curl
            $curl_response = $this->curly($newsletter_endpoint, $fields_string);

            // Pull response and send to front end.
            $request = json_decode($curl_response, true);
            $pid = $request["Pid"];
            $data = array('Pid' => array('Pid' => $pid, 'Subs' => $subs_return)); 
            echo json_encode($data);           
            die();
        } 
    }

    public function newsletter_request()
    {
       $nonce_status = self::verify_nonce($this->nonce_request_name);

       // If Nonce is correct, assemble the url append and execute curly.
       if ($nonce_status) {

           // Build endpoint and request string
           $newsletter_endpoint = $this->base_endpoint . $this->set_sub;
            
            $subs .= $this->siteID . ',' . $this->programID. ',1';
            $subs_return .= $this->siteID . '|' . $this->programID;

           $params = array(
               'Email' => urlencode($_REQUEST['email']),
               'Subs' => $subs,
               'ReturnMcg' => '1',
               'BrowserAgent' => urlencode($_SERVER['HTTP_USER_AGENT']),
               'Refurl' => urlencode($_REQUEST['refurl'])
           );

           foreach ($params as $key => $value) {
               $fields_string .= $key . '=' . $value . '&';
           }

           $fields_string = rtrim($fields_string, '&');

           // Execute curl
           $curl_response = $this->curly($newsletter_endpoint, $fields_string);

           // Pull response and send to front end.
           $request = json_decode($curl_response, true);
           $pid = $request["Pid"];
           $data = array('Pid' => array('Pid' => $pid, 'Subs' => $subs_return ) ); 
           echo json_encode($data);           
           die();
        } 
    }

    public function preferences_request()
    {
        // Verify nonce
        $nonce_status = self::verify_nonce($this->nonce_request_name);

        // If Nonce is correct, assemble the url append and execute curly.
        if ($nonce_status) {

            // Build endpoint
            $profile_endoint = $this->base_endpoint . $this->set_profile;
            $change_email_endpoint = $this->base_endpoint . $this->set_change_email;

            // Assemble request
            $preferences = $_REQUEST['preferences'];
            $settings = $_REQUEST['fields'];
            $guid = $_REQUEST['guid'];
            $email = $_REQUEST['email'];
            $unsubscribed = $_REQUEST['unsubscribed'];
            $subscriptions = $_REQUEST['subscriptions'];

            // // Check if email has been changed, if so, update to new one.
            if ($email) {
                $email = urlencode($email);
                $old_email = urlencode($_REQUEST['oldEmail']);
                $email_string = 'SiteId=' . $siteID . $guid . '&Oemail=' . $old_email . '&Nemail=' . $email;
                $email_response = $this->curly($change_email_endpoint, $email_string);
            }

            $fields_string = 'SiteId=' . $this->siteID . '&MasterContactGuid=' . $guid;

            if ( $unsubscribed ) {
                $fields_string .= '&Subs=';
                foreach( $this->programIDs as $programID ) {
                    $fields_string .= $programID . ',0,1:';
                }
                $curl_response = $this->curly($profile_endoint, $fields_string);
                $redirect = array('redirect' => array('url' => home_url()));
                echo json_encode($redirect);
                die();
            }

            $subs = '&Subs=';
            $subs_return = '';
            foreach( $subscriptions as $subscription => $value ) {
                $subs .= $subscription . ',' . $value . ',1:';
                if( $value == '1' ) {
                    $subs_return .= $this->siteID . '|' . $subscription . ';';
                }
                
            }
            $subs = rtrim($subs, ':');
            $subs_return = rtrim($subs_return, ';');

            $settings_string = '';
            if ($settings) {
                foreach ($settings as $setting) {
                    $setting = str_replace(' ', '+', $setting);
                    $settings_string .= $setting . '&';
                }
            }

            $settings_string = rtrim($settings_string, '&');
            $fields_string .= $subs . '&' . $settings_string;

            // // Execute Curl
            $curl_response = $this->curly($profile_endoint, $fields_string);
            $request = json_decode( $curl_response, true );
            $pid = $request["Pid"];
            // Pass back to front end.
            $data = array('Pid' => array('Pid' => $pid, 'Subs' => $subs_return ) ); 
            echo json_encode($data);
            die();
        }
    }

    public function get_email_preferences( $guid = '' ) {
        $preferences_endpoint = $this->base_endpoint . $this->get_preferences;
        $guid = $guid ? '&MasterContactGuid=' . $_GET['guid'] : '';
        $settings = 'SiteId=' . $this->siteID . $guid; 

        $curl_response = $this->curly($preferences_endpoint, $settings);

        return json_decode( $curl_response );

    }

    // Static helper method to execute curl.
    public function curly($url, $fields_string)
    {
        $ch = curl_init();
        $call = $url . '?' . $fields_string;
        $http_host = 'waterbrook.dev.randomhouse.com';
        curl_setopt($ch, CURLOPT_URL, trim($call));

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Accept: application/json"
        ));

        curl_setopt($ch, CURLOPT_BINARYTRANSFER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);

        if (strpos($_SERVER['HTTP_HOST'], $http_host) === 0) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        }

        $data = curl_exec($ch);

        $info = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $error = curl_error($ch);
        curl_close($ch);
        //$curly_response = ' Data>>>  '.$data .' Info>>>  '.$info.' Error>>>  '.$error.' CallURL>>>  '.$call;
        $curly_response = $data;

        return $curly_response;
    }
}

$author_ajax = new RHAjax('author', 'wbm-author-signup');
add_action( 'wp_ajax_author_signup', array($author_ajax, 'run') );
add_action( 'wp_ajax_nopriv_author_signup', array($author_ajax, 'run') );

$newsletter_request = new RHAjax('newsletter', 'wbm-newsletter-signup');
add_action( 'wp_ajax_newsletter_signup', array($newsletter_request, 'run') );
add_action( 'wp_ajax_nopriv_newsletter_signup', array($newsletter_request, 'run') );

$preference_center_request = new RHAjax('preferences', 'wbm-preferences-signup');
add_action( 'wp_ajax_preference_center', array($preference_center_request, 'run') );
add_action( 'wp_ajax_nopriv_preference_center', array($preference_center_request, 'run') );

