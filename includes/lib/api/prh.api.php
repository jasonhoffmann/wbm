<?php
/**
  * Enhanced PRH API Boilerplate for PHP
  * ====================================
  * These classes can be used as starters 
  * for use of the new Enhanced PRH API in PHP
  * ------------------------------------
  *
  * Contact berobinson@penguinrandomhouse.com
  * with questions about these classes.
**/

require("lib/class.randomhouse.utils.php");
require("lib/class.randomhouse.php");