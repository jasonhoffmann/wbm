<?php
/** 
 * This Random House Utilities class takes data from the API class and 
 * prepares it to be returned. Fitlers through a data Object that was 
 * received and matches it to appropriate parameters.
 * These are passed to templates to be used.
 *
 * For a list of possible fields generated, refer to the bottom of this class
 * @since 1.0.1
 */

class RandomHouse_Utils {

	/**
	 * @access private
	 * @var string  Root URL for linked out cover images
	 */
	private static $images_root = 'http://images.randomhouse.com/cover/700jpg/';

	/**
	 * @access private
	 * @var string  Root URL for linked out author images
	 */
	private static $author_images_root = 'http://images.randomhouse.com/author/';


	/**
	 * Prepare book data
	 * 
	 * Loop through the API request of data and 
	 * return the fields that are necessary. Some will need to be filtered,
	 * some will need default values.
	 * 
	 * @see make_bitly_url
	 * @see trailingslashit
	 * @see search_by_cat_id
	 * @see get_genre
	 * 
	 * @param type|object  $data  Data returned from the API
	 * @return object  $returnedData  Prepared data
	 */
	public static function prepareBookForReturn( $data ) {

		$returnedData = new stdClass();

		// Fields Needed


		// First lets set some standard fields:
		if( isset( $data->workId ) ) {
			$returnedData->workId = $data->workId;
		}

		if( isset( $data->title ) ) {
			$returnedData->title = $data->title;
		}

		if( isset( $data->onsale ) ) {
			$returnedData->onSaleDate = date( 'M j, Y', strtotime( $data->onsale ) );
		}

		if( isset( $data->frontlistiest->onsale ) ) {
			$returnedData->onSaleDate = $data->frontlistiest->onsale ? date( 'M j, Y', strtotime( $data->frontlistiest->onsale ) ) : false;
		}

		if( isset( $data->series ) ) {
			$returnedData->seriesTitle = $data->series;
		}

		if( isset( $data->isbn ) ) {
			$returnedData->isbn = $data->isbn;
		}

		// A few special exceptions for search results
		if( isset( $data->name ) ) {
			$returnedData->title = $data->name;
			$returnedData->description = $data->flapCopy;
			$returnedData->subtitle = $data->subtitle;

			$author_list = array();
			foreach($data->authors as $author) {
				array_push($author_list, '<a href="' . home_url($author->seoFriendlyUrl) . '">' . $author->authorDisplay . '</a>');
			}

			$returnedData->authorLinks = implode(' & ', $author_list);
			$returnedData->author = strip_tags($returnedData->authorLinks);
		}



		if( isset( $data->seoFriendlyUrl ) ) {
			$returnedData->seoFriendlyUrl = home_url( $data->seoFriendlyUrl );
		}


		if( isset( $data->frontlistiestTitle ) ) {
			// Next, let's check to see if we are in a full
			// individual work list "/views/product-display"

			$returnedData->isbn = $data->frontlistiestTitle->isbn;
			$returnedData->subtitle = $data->frontlistiestTitle->subtitle;
			$returnedData->imprint = $data->frontlistiestTitle->imprint->name;
			$returnedData->description = $data->frontlistiestTitle->aboutTheBook ? $data->frontlistiestTitle->aboutTheBook : 'Details Coming Soon...';
			$returnedData->coverImage = self::$images_root . $data->frontlistiestTitle->isbn;
			$returnedData->videoRelatedLinks = get_object_vars( $data->videoRelatedLinks );
			$returnedData->relatedLinks = get_object_vars( $data->relatedLinks );
			$returnedData->series = $data->frontlistiestTitle->series;
			if($returnedData->series) {
				$returnedData->seriesTitle = '<a href="' . $returnedData->series->seoFriendlyUrl . '">' . $returnedData->series->title . '</a>';
			}
			$returnedData->totalPages = $data->frontlistiestTitle->totalPages;
			$returnedData->cartonQty = $data->frontlistiestTitle->cartonQuantity;
			$returnedData->hasInsight = $data->frontlistiestTitle->hasInsight;
			$returnedData->trimSize = $data->frontlistiestTitle->trimSize;
			$returnedData->onSaleDate = date( 'M j, Y', strtotime( $data->frontlistiestTitle->onSaleDate->date ) );
			$returnedData->bitly = urlencode( make_bitly_url($returnedData->seoFriendlyUrl, 'waterbrook') );
            $praises = get_object_vars( $data->praises );
			$praises = array_values( $praises );
			$returnedData->praises = array_key_exists(0, $praises) ? $praises[0] : '';

			if( isset( $data->frontlistiestTitle->contributors ) ) {
				$author_list = array();
				$i = 0;
				foreach ( $data->frontlistiestTitle->contributors as $author ) {
					if( $author->roleCode === 'A' ) {
						array_push($author_list, '<a href="' . home_url($author->seoFriendlyUrl) . '">' . $author->display . '</a>');

						$returnedData->authors[$i] = new StdClass();
						$returnedData->authors[$i]->id = $author->id;
						$returnedData->authors[$i]->name = $author->display;
						$returnedData->authors[$i]->bio = $author->spotlight;
						$returnedData->authors[$i]->url = $author->seoFriendlyUrl;
						$returnedData->authors[$i]->image = 'http://images.randomhouse.com/authors/' . $author->id . ".jpg";
						$returnedData->authors[$i]->hasAuthorPhoto = $author->hasAuthorPhoto;
						$i++;
					}
				}

				$returnedData->authorLinks = implode(' & ', $author_list);
				$returnedData->author = strip_tags($returnedData->authorLinks);
			}

			if( isset( $data->formats ) ) {
				$returnedData->formats = array();

				if( isset( $data->formats->Hardcover ) ) {
					$key = key( $data->formats->Hardcover );
					$format_data = $data->formats->Hardcover->{$key};

					$returnedData->formats['Hardcover']['isbn']= $format_data->isbn;
					$returnedData->formats['Hardcover']['date']=  date( 'M j, Y', strtotime( $format_data->onSaleDate->date ) );
					$returnedData->formats['Hardcover']['price'] = number_format( $format_data->usPrice, 2 );
					$returnedData->formats['Hardcover']['pages'] = $format_data->totalPages;
				}

				if( isset( $data->formats->Paperback) ) {
					$key = key( $data->formats->Paperback );
					$format_data = $data->formats->Paperback->{$key};

					$returnedData->formats['Paperback']['isbn']= $format_data->isbn;
					$returnedData->formats['Paperback']['date']=  date( 'M j, Y', strtotime( $format_data->onSaleDate->date ) );
					$returnedData->formats['Paperback']['price'] = number_format( $format_data->usPrice, 2 );
					$returnedData->formats['Paperback']['pages'] = $format_data->totalPages;
				}

				if( isset( $data->formats->Ebook ) ) {
					$key = key( $data->formats->Ebook );
					$format_data = $data->formats->Ebook->{$key};

					$returnedData->formats['Ebook']['isbn']= $format_data->isbn;
					$returnedData->formats['Ebook']['date']=  date( 'M j, Y', strtotime( $format_data->onSaleDate->date ) );
					$returnedData->formats['Ebook']['price'] = number_format( $format_data->usPrice, 2 );
					$returnedData->formats['Ebook']['pages'] = $format_data->totalPages;
				}

				if( isset( $data->formats->Audio ) ) {
					foreach($data->formats->Audio as $audio) {
						$format = $audio->format->name;
						$returnedData->formats[$format]['isbn']= $audio->isbn;
						$returnedData->formats[$format]['date']=  date( 'M j, Y', strtotime( $audio->onSaleDate->date ) );
						$returnedData->formats[$format]['price'] = number_format( $audio->usPrice, 2 );
						$returnedData->formats[$format]['pages'] = null;
					}
				}
			}

			if( isset( $data->frontlistiestTitle->categories ) ) {
				$returnedData->catIDs = array();
				$returnedData->categories = '';
				$slug_array = array();
				foreach($data->frontlistiestTitle->categories as $category) {
					array_push($returnedData->catIDs, $category->catId);
					$slug = search_by_cat_id(strval($category->catId));
					if( $slug && !in_array($slug, $slug_array)) {
						$title = get_genre($slug);
						$returnedData->categories .= '<a class="book-category" href="';
						$returnedData->categories .= home_url('/books/category/' . $slug) . '">' . $title . '</a>';
					}
					array_push($slug_array, $slug);
				}
			}

		} else {
			// Or, maybe we're in a list of books, 
			// so data needs to be handled a bit differently
			if(isset( $data->isbn ) ) {
				$returnedData->coverImage = self::$images_root . $data->isbn;
			} else {
				$returnedData->coverImage = $data->_links[9]->href;
			}
			

			if( $data->author && is_array($data->author) ) {
				$author_list = array();
				foreach($data->author as $author) {
					if( $author->roleCode === 'A' ) {
						array_push($author_list, '<a href="' . home_url($author->seoFriendlyUrl) . '">' . $author->authorDisplay . '</a>');
					}
				}

				$returnedData->authorLinks = implode(' & ', $author_list);
				$returnedData->author = strip_tags($returnedData->authorLinks);
			}


		}

		return $returnedData;
	}


	/**
	 * Prepare event data
	 * 
	 * Loop through the API request of data and 
	 * return the fields that are necessary. Some will need to be filtered,
	 * some will need default values.
	 * 
	 * 
	 * @param type|object  $data  Data returned from the API
	 * @return object  $returnedData  Prepared data
	 */
	public static function prepareEventForReturn( $data ) {
		$returnedData = new stdClass();

		$returnedData->description = $data->description;
		$returnedData->eventDate = date( 'M j, Y', strtotime( $data->eventDate ) );
		$returnedData->eventTime = $data->eventTime;
		$returnedData->address1 = $data->address1;
		$returnedData->address2 = $data->address2;
		$returnedData->city = $data->city;
		$returnedData->state = $data->state;
		$returnedData->location = $data->location;
		$returnedData->url = 'https://maps.google.com?daddr=' . urlencode( $data->address1 . ' ' . $data->address2 .  ' ' . $data->city . ' ' . $data->state);

		return $returnedData;

	}

	/**
	 * Prepare author data
	 * 
	 * Loop through the API request of data and 
	 * return the fields that are necessary. Some will need to be filtered,
	 * some will need default values.
	 * 
	 * @see injectSocialLinks
	 * @see prepareEventForReturn
	 * 
	 * @param type|object  $data  Data returned from the API
	 * @return object  $returnedData  Prepared data
	 */
	public static function prepareAuthorForReturn( $data ) {
		$returnedData = new stdClass();

		if($data->authorId) {
			$returnedData->authorId = $data->authorId;
			$returnedData->name = $data->display;
			$returnedData->description = $data->spotlight;
			$returnedData->initials = $data->firstInitial . $data->lastInitial;
			$returnedData->coverImage = self::$author_images_root . $data->authorId;
			$returnedData->hasAuthorPhoto = $data->hasAuthorPhoto;
			$returnedData->authorOf = $data->authorOf;
			$returnedData->ontour = $data->ontour;
			$returnedData->seoFriendlyUrl = home_url( $data->seoFriendlyUrl );
		}

		if($data->relatedLinks) {
			$returnedData->relatedLinks = $data->relatedLinks;
			self::injectSocialLinks( $returnedData );
		}

		if($data->events) {
			$returnedData->events = array();
			foreach( $data->events as $event) {
				$prepared_event = self::prepareEventForReturn( $event );
				array_push( $returnedData->events, $prepared_event );
			}
		}
		return $returnedData;
	}

	/**
	 * Inject social links in related links
	 * 
	 * Helper that sorts through Related links, looking for social links.
	 * If it finds them it sticks them in the $social part
	 * of returned Data as an array
	 * 
	 * @param type|object  $model  Full data object
	 * @return object  $model  Full data object + social links
	 */
	public static function injectSocialLinks( $model ){
	    foreach($model->relatedLinks as $key => $relatedLink){
	      if($relatedLink->linkAttr==10000){
	      $model->social['Youtube']=$relatedLink;
	       unset($model->relatedLinks[$key]);
	      }elseif($relatedLink->linkAttr==15000){
	      $model->social['Pinterest']=$relatedLink;
	       unset($model->relatedLinks[$key]);
	      }elseif($relatedLink->linkAttr==17000){
	      $model->social['Facebook']=$relatedLink;
	      unset($model->relatedLinks[$key]);
	      }elseif($relatedLink->linkAttr==19000){
	      $model->social['Twitter']=$relatedLink;
	      unset($model->relatedLinks[$key]);
	      }elseif($relatedLink->linkAttr==21000){
	      $model->social['Google']=$relatedLink;
	       unset($model->relatedLinks[$key]);
	      }elseif($relatedLink->linkAttr==23000){
	      $model->social['Instagram']=$relatedLink;
	       unset($model->relatedLinks[$key]);
	      }elseif($relatedLink->linkAttr==20000){
	      $model->social['Tumblr']=$relatedLink;
	       unset($model->relatedLinks[$key]);
	      }elseif($relatedLink->linkAttr==6000){
	      $model->social['Blog']=$relatedLink;
	       unset($model->relatedLinks[$key]);
	      }elseif($relatedLink->linkAttr==3000){
	      $model->social['Website']=$relatedLink;
	       unset($model->relatedLinks[$key]);
	      }
	    }

	    return $model;
	  }

}

/**
 * Prepared Data Reference:
 * Note, not all fields will be generated on every request.
 * 
 * Fields generated for books or book:
 *  - workID : WorkID
 *  - title : Title
 *  - subtitle : Subtitle
 *  - isbn : ISBN
 *  - description : Flapcopy
 *  - coverImage : Cover Image
 *  - author : list of names, separated by "&"
 *  - authorLinks : list of names, separated by "&" and with anchor tags
 *  - authors : Author List, as an array, in case we need it
 *  - onSaleDate : On Sale Date
 *  - totalPages : Total Pages
 *  - series : Series as an arrayed object
 *  - seriesTitle : Series Title with anchor tag
 *  - seoFriendlyUrl : seoFriendlyUrl
 *  - relatedLinks : Related Links
 *  - videoRelatedLinks : Video Related Links
 *  - formats : List of formats, in an associative array
 *  - cartonQty : Carton Quantity
 *  - categories : List of categories, returned as linked list
 *  - catIDs : array of category IDs
 *  - hasInsight : Has an Insight Widget
 *  - bitly : bitly link
 * 
 * Fields generated for authors or author:
 *  - authorId : Author ID
 *  - name : Author's Name
 *  - description : Spotlight or About the Author
 *  - initials : Concatenated first & last inital
 *  - coverImage : Author photo
 *  - hasAuthorPhoto : boolean of whether author photo is present
 *  - ontour : boolean of whether author has events
 *  - seoFriendlyUrl : link to URL
 *  - relatedLinks : A list of related links, separated out with social
 *  - events : Upcoming events
 * 
 * Fields generated for events:
 *  - description : Event description
 *  - eventDate : Event date
 *  - address1 : Address 1
 *  - address2 : Address 2
 *  - city : Event city
 *  - state : Event state
 *  - location : Event location
 *  - url : Google Maps URL
 */