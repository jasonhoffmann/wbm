<?php

/**
 * This Random House class makes calls to the PRH API and then returns them
 * in various formats. It does so by making a simple cURL request (abstracted by WP)
 * and then iterating through the results and returning them.
 *
 *
 * @since 1.0
 * @since 1.0.1 Revising class to abstract out functionality
 *
 */
class RandomHouse {

  /**
   * The prod API Root, only to be used in production
   */
  private $api_root = 'https://api.penguinrandomhouse.com/resources/v2/title/domains/PRH.US/';

  /**
   * @access private
   * @var $api_root   Root API for test
   */
  //private $api_root = 'http://interapp-prd.us.randomhouse.com/PrhApi/domains/PRH.US/';

  /**
   * @access private
   * @var string  base of API call, default to works, since this is most common
   */
  private $api_section = 'works/';

  /**
   * @access private
   * @var string  base of view, defaults to product display
   */
  private $view = '/views/product-display';

  /**
   * @access private
   * @var string  Any parameters to end to the very of end of the request
   */
  private $request_tail = '';

  /**
   * @access private
   * @var array  Sets up default parameters to merge with custom ones.
   */
  private $params = array(
    'api_key' => 'zt8w7crh4cyqj5kjhkqjzh35',
    'rows' => 20
  );

  /**
   * @access private
   * @var int  Reference to the number of entries in record count
   */
  private $record_count;

  /**
   * @access private
   * @var string  Reference to title when series are pulled
   */
  private $series_title;

  /**
   * @access private
   * @var string  Reference to facets used by search
   */
  private $facets;

  /**
   * @var array  Divisions to limit API calls to
   */
  private $divisions = array(57, 17, 38);

  /**
   * @var array  Imprints to limit API calls to
   */
  private $imprints = array('W1', 'MH', 'SW');

  /**
   * @var string  Reference to the strings used at the end of a URL
   */
  private $divisions_str;
  private $imprints_str;

  /**
   * Construct
   *
   * Not much to put together here, just ensure that division and
   * imprints string are combined properly
   *
   *
   * @param type|array $options
   * @return object  $book  Book or SimpleBook object
   */
  public function __construct() {
    foreach( $this->divisions as $division ) {
      $this->divisions_str .= "&divisionCode=" . $division;
    }

    foreach( $this->imprints as $imprint ) {
      $this->imprints_str .= "&imprintCode=" . $imprint;
    }

  }

  /**
   * API call for individual books
   *
   * Needs an ID to work correctly. Builds an API call to request
   * either an individual work or title, depending on the length of
   * the ID parameter in options.
   *
   * @see RandomHouse_Utils::prepareBookForReturn
   * @see makeRequest
   *
   * @param type|array $options
   * @return object  $book  Prepared data
   */
  public function book( $options = array() ) {
    $id = isset( $options['id'] ) ? $options['id'] : '';
    $full = isset( $options['full'] ) ? $options['full'] : false;
    $params = isset( $options['params'] ) ? $options['params'] : array();

    $id = preg_replace( '/\-/', '', $id );
    $id_length = strlen( $id );

    $isWork = true;

    if ( $id_length == 13) {
      $this->api_section = 'titles/';
      $isWork = false;
    }

    $data = self::makeRequest( $params, $id );
    $data = $data->data;

    $prepared = RandomHouse_Utils::prepareBookForReturn($data);

    return $prepared;
    }

    /**
     * API call for a list of books
     *
     * Doesn't need any parameters to work properly
     * Will retrieve a list of books from the API
     * and prepare each data in an object appropriately
     *
     * @see RandomHouse_Utils::prepareBookForReturn
     * @see mergeString
     * @see makeRequest
     *
     * @param type|array $options
     * @return array  $books  Array of prepared data objects
     */
    public function books( $options = array() ) {
      $params = isset( $options['params'] ) ? $options['params'] : array();
      $newest_date = isset( $options['newest_date'] ) ? $options['newest_date'] : false;

      $isbns = isset( $options['isbns'] ) ? $options['isbns'] : false;
      $categories = isset( $options['categories'] ) ? $options['categories'] : false;

      $this->api_section = 'works/views/list-display';
      $this->view = '';

      $isbns_str = self::mergeString( $isbns, 'isbn' );

      $categories_str = self::mergeString( $categories, 'catId' );

      if( isset( $options['ignoreDivision'] ) && $options['ignoreDivision'] ) {
        $this->divisions_str = '';
      }

      if( isset( $options['ignoreImprint']) && $options['ignoreImprint'] ) {
        $this->imprints_str = '';
      }

      $default_params = array(
        'hideBooksWithNoCover' => true,
        'sort' => 'onsale',
        'dir' => 'desc'
      );

      $request_params = array_merge( $default_params, $params );

        $this->request_tail = $isbns_str . $categories_str . $this->divisions_str . $this->imprints_str;

        $dataset = self::makeRequest( $request_params );

        $this->record_count = $dataset->recordCount;
        $dataset = $dataset->data->works;

        if($newest_date) {
          $dataset = $this->resortWorks( $dataset );
        }

        $books = array();
        foreach ($dataset as $data) {
          $rhUtil = new RandomHouse_Utils;
          $book = $rhUtil->prepareBookForReturn($data);
          array_push($books, $book);
        }

      return $books;

    }

    public function resortWorks( $dataset ) {
      $sortedData = array();

      foreach($dataset as $data) {

        $onsale = strtotime( $data->onsale );
        $current_time = time();

        if( $current_time > $onsale ) {
          $data->frontlistiest->onsale = false;
          $value = array_shift( $dataset );
          array_push($sortedData, $data);
        }

      }

      $newArr = array_merge( $dataset, $sortedData );
      return $newArr;
    }

    /**
     * API call for a content, including press releases and press contacts
     *
     * Needs the ID of the work to return properly
     * Used only when special data, like press releases, are required
     *
     * @see makeRequest
     *
     * @param type|array $options
     * @return object  $content  Object of data
     */
    public function content( $options=array() ) {
      $id = isset( $options['id'] ) ? $options['id'] : '';
      $id = preg_replace( '/\-/', '', $id );

      $this->api_section = 'titles/';
      $this->view = '/content';
      $this->request_tail = '';
      $params = array();

      $dataset = self::makeRequest( $params, $id );

      return $dataset->data->content;

    }

    /**
     * API call for a series list
     *
     * Needs the ID of the series to work properly
     * When this gets passed in, retrieve a list of books
     * from the series and then return it as properly
     * prepared data
     *
     * @see RandomHouse_Utils::prepareBookForReturn
     * @see makeRequest
     *
     * @param type|array $options
     * @return object  $books  Array of prepared data objects
     */
    public function series( $options = array() ) {
      $id = isset( $options['id'] ) ? $options['id'] : '';
      $id = preg_replace( '/\-/', '', $id );
      $limit = isset( $options['limit'] ) ? $options['limit'] : null;

      $default_params = array(
        'sort' => 'onsale',
        'dir' => 'asc'
      );

      if($options['limit']) {
        $default_params['rows'] = 5;
      }

      $this->api_section = 'series/';
      $this->view = '';

      $request_params = array_merge( $this->params, $default_params );
      $this->request_tail = '&zoom=https://api.penguinrandomhouse.com/title/works/definition';

      $dataset = self::makeRequest( $request_params, $id );
        $dataset = $dataset->data->_embeds[0]->works;

      $books = array();
      $i = 0;
      foreach ($dataset as $data) {
        if($i > 3 && $limit) {
          break;
        }
        $book = RandomHouse_Utils::prepareBookForReturn($data);
          array_push($books, $book);
          $i++;
      }

        return $books;

    }

    /**
     * API call for events
     *
     * Needs the ID of work to return properly
     * When this gets passed in, retrieve a list of books
     * from the /works/ endpoint
     * No options necessary
     *
     * @see RandomHouse_Utils::prepareEventForReturn
     * @see makeRequest
     *
     * @return array  $events  Array of prepared data or null
     */
    public function events($options = array()) {
      $id = isset($options['id']) ? $options['id'] : '';
      $id = preg_replace( '/\-/', '', $id );

      $this->api_section = 'works/';
      $this->view = '/events';
      $currentDate = date('Y/m/d');

      $request_params = array(
        'sort' => 'eventdate',
        'dir' => 'desc',
        'eventDateFrom' => $currentDate,
        'rows' => 0
      );

      $dataset = self::makeRequest( $request_params, $id );
      if( isset( $dataset->data->events ) ) {
        $events = array();
        foreach($dataset as $data) {
          $data = RandomHouse_Utils::prepareEventForReturn($data);
          array_push($events, $data);
        }
        return $events;
        } else {
        return '';
      }
    }

    /**
     * API call for all events
     *
     * Needs the ID of work to return properly
     * When this gets passed in, retrieve a list of books
     * from the /works/ endpoint
     * No options necessary
     *
     * @see RandomHouse_Utils::prepareEventForReturn
     * @see makeRequest
     *
     * @return array  $events  Array of prepared data or null
     */
    public function all_events($options = array()) {

      $this->api_section = 'events/';
      $this->request_tail = $this->divisions_str . $this->imprints_str;
      $this->view = '';
      $currentDate = date('Y/m/d');

      $request_params = array(
        'sort' => 'eventdate',
        'dir' => 'desc',
        'eventDateFrom' => $currentDate,
        'rows' => 0
      );

      $dataset = self::makeRequest( $request_params );
      if( isset( $dataset->data->events ) ) {
        $events = array();
        foreach($dataset as $data) {
          $data = RandomHouse_Utils::prepareEventForReturn($data);
          array_push($events, $data);
        }
        return $events;
        } else {
        return '';
      }
    }

    /**
     * API call for a search endpoint
     *
     * Similar to the books method, but requires
     * a lot more configuration to pass to the endpoint
     * Options include facets and search term
     *
     * @see RandomHouse_Utils::prepareBookForReturn
     * @see mergeString
     * @see makeRequest
     *
     * @param type|array $options
     * @return array  $books  Array of prepared data events or null
     */
    public function search( $options = array() ) {
      $start = isset( $options['start'] ) ? $options['start'] : 0;
      $term = isset( $options['term'] ) ? $options['term'] : '';
      $formats = isset( $options['formats'] ) ? $options['formats'] : false;
      $ageRange = isset( $options['ageRange'] ) ? $options['ageRange'] : false;
      $categories = isset( $options['categories'] ) ? $options['categories'] : false;
      $imprints = isset( $options['imprints'] ) ? $options['imprints'] : array('WaterBrook+Press', 'Multnomah+Books', 'Shaw+Books');
      $release = isset( $options['release'] ) ? $options['release'] : false;

      $request_params = array(
        'hideBooksWithNoCover' => true,
        'docType' => 'work',
        'start' => $start,
        'rows' => 10
      );

      $imprints_string = self::mergeString( $imprints, 'imprint' );
      $formats_string = self::mergeString( $formats, 'format' );
      $age_range_string = self::mergeString( $ageRange, 'ageRange' );
      $category_string = self::mergeString( $categories, 'categoryLabel' );

      $this->api_section = 'search/views/search-display';
      $this->view = '';
      $this->request_tail = $imprints_string . $formats_string . $category_string . '&q=' . $term;

      $dataset = self::makeRequest( $request_params );
      $dataset = $dataset->data->results;

      // usort($dataset, array( $this, 'sortSearch'));

      $books = array();
      foreach ($dataset as $data) {
        $book = RandomHouse_Utils::prepareBookForReturn($data);
        array_push($books, $book);
      }

      return $books;

    }

    public function sortSearch( $a, $b ) {
        $t1 = strtotime( $a->onsale );
        $t2 = strtotime( $b->onsale );

        return $t2 - $t1;
    }


    /**
     * Build search facets
     *
     * Simple helper function that sorts through current facets
     * and organizes it for AJAX calls.
     *
     * @return object  $facets  An iterable array of search facets
     */
    public function build_facets() {
      $facets = array();
      foreach($this->facets as $data) {
        $facets[$data->name] = $data->values;
      }
      return $facets;
    }

  /**
   * API call for individual authors
   *
   * Needs an ID to work correctly. Builds an API call to request
   * an author and then prepares the data for reutrn.
   *
   * @see makeRequest
   * @see RandomHouse_Utils::prepareAuthorForReturn
   * @see books
   *
   * @param type|array $options
   * @return object  $author  A template-ready author object
   */
  public function author( $options = array() ) {
    $id = isset( $options['authorID'] ) ? $options['authorID'] : '';
    $id = preg_replace( '/\-/', '', $id );
    $params = array();

    $this->api_section = 'authors/';
    $this->view = '/views/author-display/';

    $data = self::makeRequest( $params, $id );
    $data = $data->data;
    $prepared = RandomHouse_Utils::prepareAuthorForReturn($data);


    $works = self::books( array( 'params' => array( 'rows' => 0, 'authorId' => $id ) ) );
    $prepared->works = $works;

    return $prepared;
    }



    /**
     * API call for authors
     *
     * No options required.
     * Makes a call to retrieve all authors and then prepares
     * the data to be returned.
     *
     * @see RandomHouse_Utils::prepareAuthorForReturn
     * @see makeRequest
     *
     * @param type|array $options
     * @return array  $authors  Array of prepared data objects
     */
    public function authors( $options = array() ) {
        $params = isset( $options['params'] ) ? $options['params'] : array();

        $authorIDs = isset( $options['authorIDs'] ) ? $options['authorIDs'] : false;

        $this->api_section = 'authors/views/list-display';
        $this->view = '';

        $authorIDs_str = self::mergeString( $authorIDs, 'authorId' );

        $default_params = array(
          'hideBooksWithNoCover' => true,
          'sort' => 'onsale',
          'dir' => 'desc'
        );

        $request_params = array_merge( $default_params, $params );

          $this->request_tail = $authorIDs_str . $this->divisions_str . $this->imprints_str;
          $dataset = self::makeRequest( $request_params );
          $this->record_count = $dataset->recordCount;
          $dataset = $dataset->data->authors;



          $authors = array();
          foreach ($dataset as $data) {
            $author = RandomHouse_Utils::prepareAuthorForReturn($data);
            array_push($authors, $author);
          }

        return $authors;
    }

    /**
     * Merge data from string for API
     *
     * Takes an array of data to iterate through, and a base
     * to assemble together a suitable string for API
     * i.e. &catId=20004&catId=20005
     *
     * @param type|array  $dataArray  The array of data to iterate through
     * @param type|string  $base  The base of the API URL
     * @return type|string  $str  The prepared string
     */
    public function mergeString( $dataArray = null, $base = null ) {

      if( $dataArray && is_array($dataArray) ) {
        $str = '';
        foreach( $dataArray as $dataPiece ) {
          $dataPiece = trim( $dataPiece );
          $str .= '&' . $base . '=' . $dataPiece;
        }

        return $str;
      } else {
        return '';
      }

    }

    /**
     * Make cURL request
     *
     * Takes an array of parameters + optional ID and creates a request to the
     * API. It then returns the properly json_decoded data object, and updates
     * private variables
     *
     * @see wp_remote_get
     * @see wp_remote_retrieve_body
     *
     * @param type|array  $params  Parameters object
     * @param type|string  $id  ID to pass to end of API
     * @return type|object  $data  Returned data from the API
     */
  public function makeRequest( $params = array(), $id = null ) {
    $request_params = array_merge($this->params, $params);
    $request_tail = $this->request_tail;

    $request_url = $this->api_root . $this->api_section . $id . $this->view;
    $request_url = $request_url . "?" . http_build_query($request_params) . $request_tail;
    $request = wp_remote_get($request_url);
    $data = wp_remote_retrieve_body($request);
    $data = json_decode( $data );

    if( isset( $data->recordCount ) ) {
      $this->record_count = $data->recordCount;
    }

    if( isset( $data->data->series ) ) {
      $this->series_title = $data->data->series[0]->seriesName;
    }

    if( isset( $data->data->facets) ) {
      $this->facets = $data->data->facets;
    }

    return $data;
  }


  /**
   * Get record count
   *
   * Returns record count for current object
   *
   * @return type|int  $record_count  Number of books / authors / etc. in current object
   */
  public function get_record_count() {
    return $this->record_count;
  }

  /**
   * Get series title
   *
   * Returns series title for current object
   *
   * @return type|string  $series_title  If in a series, returns series title
   */
  public function get_series_title() {
    return $this->series_title;
  }


}
