<article class="entry grid">
<?php 
$isbn = get_post_meta(get_the_ID(), 'isbn', true );

$get_book = new RandomHouse;
$book = $get_book->book( array( 'id' => $isbn ) );
$audio_file = get_post_meta( $post->ID, '_am_wbm_audio_attachment', true );
$audio_class = $audio_file ? 'podcast' : '';
?>

<div class="entry-content span_12 <?php echo $audio_class; ?>">
	<?php if ( has_post_thumbnail() ) { ?>
		<div class="entry-index-image">
			<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
			<?php the_post_thumbnail('cropped_featured'); ?>
			</a>
		</div>
	<?php } ?>
	<h3 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
	<div class="entry-meta">
		<?php the_time( get_option( 'date_format' ) ); ?>
	</div>
	<?php if($isbn) { ?>
		<div class="entry-book">
			<a href="<?php echo $book->seoFriendlyUrl; ?>"><img src="<?php echo $book->coverImage; ?>"></a>
			<div class="button-box">
				<?php echo generate_buy_button( $isbn ); ?>
			</div>
		</div>
	<?php } ?>
		<?php if($video = get_post_meta($post->ID, "video", true)) {
         	// This takes all of the old style embed codes in the database and replaces them with the new YouTube embed
         	if(strpos($video, "youtube.com") !== false) {
         		preg_match('/(?:\/|embed\/)([0-9a-zA-Z-_]+)(?:[&|?]|\")/', $video, $video);
         		$video = $video[1];
         	}
         ?>
         <div class="video-wrapper">
	         <div class="video-container">
	        	<iframe src="http://www.youtube.com/embed/<?= $video ?>" frameborder="0" allowfullscreen></iframe>                                    				
			</div>
		</div>
		<?php } else { 
			the_excerpt(); } ?>

	</div>
</article>