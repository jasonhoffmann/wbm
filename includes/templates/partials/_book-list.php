<?php
/**
 * A partial that renders a book list on index pages
 *
 * @package WaterBrook Multnomah
 */

$books = $this->books;
$title = $this->title;
$link = $this->link;
?>

<section class="book-list">
<?php if($title) { ?>
	<h3 class="book-list-title"><a href="<?php echo $link; ?>"><?php echo $title; ?></a></h3>
<?php } ?>
	<ul class="list-unstyled cf book-list-block">
	<?php foreach($books as $book) { ?>
		<li class="book-list-item">
			<a href="<?php echo $book->seoFriendlyUrl; ?>" class="book-list-link">
				<div class="book-list-image"><img src="<?php echo $book->coverImage; ?>"></div>
				<div class="book-list-item-title"><?php echo $book->title; ?>
				<div class="book-list-item-author"><?php echo $book->author; ?></div>
			</div>
		</a>
	<?php } ?>
	</ul>

	<?php if($this->linkTitle) { ?>
	<div class="book-list-btn">
		<a href="<?php echo $link; ?>" class="btn btn-default">View More <?php echo $this->linkTitle; ?></a>
	</div>
	<?php } ?>
</section>
