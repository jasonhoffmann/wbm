<?php 
$books = $this->books; 
$total = $this->total;

?>

<p class="search-count"><?php echo $total; ?> results</p>
<?php if($books) { ?>
	<ul class="list-unstyled">
		<?php foreach($books as $book) { ?>
			<li class="grid search-item" style="margin-bottom: 4em;">
				<div class="span_3">
					<a href="<?php echo $book->seoFriendlyUrl; ?>"><img src="<?php echo $book->coverImage; ?>"></a>
					<div class="button-box">
						<?php echo generate_buy_button($book->isbn); ?>
					</div>
				</div>
				<div class="span_8">
					<h4 style="margin-top: 0;"><a href="<?php echo $book->seoFriendlyUrl; ?>">
						<?php echo $book->title; ?>
					</a></h4>
				<?php if($book->subtitle) { ?>
					<h5><?php echo $book->subtitle; ?></h5>
				<?php } ?>
				<h5><?php echo $book->authorLinks; ?></h5>
				<h5><?php echo $book->onSaleDate; ?></h5>
				<?php if( $book->seriesTitle ) { ?>
					<h5>Part of the <em><?php echo $book->seriesTitle; ?></em> series</h5>
				<?php } ?>
					<p><?php echo wp_trim_words($book->description); ?></p>
				</div>
			</li>
		<?php } ?>
	</ul>
<?php } else { ?>
	<p>Sorry, there are no results for this search.</p>
<?php } ?>