<?php
/**
* Newsletter signup form
* 
* @since 1.0
*/
?>

<div class="js-subscribe newsletter">
	<h4 class="newsletter-title">Stay in Touch</h4>
	<p class="newsletter-caption">Sign up for our email newsletter to find out about new releases, special promotions, and book related events.</p>
	<div class="alert alert-success js-success" style="display:none;">Great! You've signed up successfully!</div>
	<div class="alert alert-danger js-error" style="display:none;"></div>
	<form name="subscribe" action="" method="POST" target="_top">
		<fieldset class="newsletter-signup">
			<input type="text" placeholder="Enter Your Email Address" id="email-newsletter" name="Email" value="" size="30" maxlength="64" class="textbox">
			<input class="submit" name="subscribe" type="submit" value="Submit Form" />
		</fieldset>
		<input type="hidden" name="ajaxurl" value="http://<?php echo $_SERVER['SERVER_NAME']; ?>/wp-admin/admin-ajax.php">
		<input type="hidden" name="nonce" value="<?php echo wp_create_nonce('wbm-newsletter-signup'); ?>">
		<input type="hidden" name="action" value="newsletter_signup">
	</form>
</div>