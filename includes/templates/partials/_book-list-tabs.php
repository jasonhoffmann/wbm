<?php
$newreleases_params = array(
	'params' => array(
		'rows' => 4,
		'showNewReleases' => 'true'
		)
	);

$bestsellers_params = array(
	'params' => array(
		'rows' => 4,
		'minInPrint' => 60000,
		'sort' => 'printScore'
		)
	);

$comingsoon_params = array(
	'params' => array(
		'rows' => 4,
		'showComingSoon' => true
		)
	);

$get_books = new RandomHouse;
$bestsellers = $get_books->books( $bestsellers_params );

$get_books = new RandomHouse;
$newreleases = $get_books->books( $newreleases_params );

$get_books = new RandomHouse;
$comingsoon = $get_books->books( $comingsoon_params );
?>

<ul class="tab-nav list-unstyled" role="tablist">
	<li role="presentation" class="active"><a href="#new-releases" aria-controls="newreleases" role="tab" data-toggle="tab">New Releases</a></li>
	<li role="presentation"><a href="#best-sellers" aria-controls="best-sellers" role="tab" data-toggle="tab">Best Sellers</a></li>
	<li role="presentation"><a href="#coming-soon" aria-controls="coming-soon" role="tab" data-toggle="tab">Coming Soon</a></li>
</ul>
<div role="tabpanel" class="book-list-tab-pane active" id="new-releases">
	<?php render_template( 'partials/_book-list.php', array( 'books' => $newreleases, 'linkTitle' => 'New Releases', 'link' => home_url('/books/category/new-releases/') ) 	); ?>
</div>
<div role="tabpanel" class="book-list-tab-pane" id="best-sellers">
	<?php render_template( 'partials/_book-list.php', array( 'books' => $bestsellers, 'linkTitle' => 'Bestsellers', 'link' => home_url('/books/category/best-sellers/') ) ); ?>
</div>
<div role="tabpanel" class="book-list-tab-pane" id="coming-soon">
	<?php render_template( 'partials/_book-list.php', array( 'books' => $comingsoon, 'linkTitle' => 'Coming Soon', 'link' => home_url('/books/category/coming-soon/') ) ); ?>
</div> 