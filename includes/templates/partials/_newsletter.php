<?php
/**
* Newsletter signup form
* 
* @since 1.0
*/
?>
	<div class="newsletter">
		<h4 class="newsletter-title">Stay in Touch</h4>
		<p class="newsletter-caption">Sign up for our email newsletter to find out about new releases, special promotions, and book related events.</p>
 
    <!-- BEGIN Newsletter Signup Form -->
	<form method="POST" name="signup_list" action="http://www.randomhouse.com/cgi-bin/newsletters/index.php" onsubmit="return emvSubmit(this);">
		<input type="hidden" name="EXACTTARGET_EMAIL_CUSTOMER_KEY_FIELD" value="1561"/>
		<input type="hidden" name="REDIRECT_URI" value="http://waterbrookmultnomah.com/subscribe-thanks/" />
		<label class="control checkbox newsletter-checkbox">
			<input type="checkbox" id="WATERBROOK_CONTESTS_NEWSLETTER" name="RH_REQUEST_SUBCODES[]" value="WATERBROOK_CONTESTS_NEWSLETTER">
			<span class="control-indicator"></span>
			Contests
		</label>
		<label class="newsletter-checkbox"><input type="checkbox" id="WATERBROOK_FICTION_READS_NEWSLETTER" name="RH_REQUEST_SUBCODES[]" value="WATERBROOK_FICTION_READS_NEWSLETTER">Fiction Reads</label>
		<label class="newsletter-checkbox"><input type="checkbox" id="WATERBROOK_LIBRARIAN_NEWSLETTER" name="RH_REQUEST_SUBCODES[]" value="WATERBROOK_LIBRARIAN_NEWSLETTER">Library Newsletter</label>
		<label class="newsletter-checkbox"><input type="checkbox" id="WATERBROOK_CHURCH_CONNECTIONS_NEWSLETTER" name="RH_REQUEST_SUBCODES[]" value="WATERBROOK_CHURCH_CONNECTIONS_NEWSLETTER">Church Connections</label>
		<label class="newsletter-checkbox"><input type="checkbox" id="WATERBROOK_RETAILER_NEWS" name="RH_REQUEST_SUBCODES[]" value="WATERBROOK_RETAILER_NEWS">Retailer News</label>
		<fieldset class="newsletter-signup">
			<input type="text" placeholder="Enter Your Email Address" id="EMAIL_FIELD" name="EMAIL_FIELD" value="" size="30" maxlength="64" class="textbox">
			<input class="submit" type="submit" value="Submit Form" />
		</fieldset>
	</form>
	<!-- END Newsletter Signup Form -->
	</div>