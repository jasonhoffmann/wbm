<?php
/**
 * The template for the main books page
 *
 * @package WaterBrook Multnomah
 */

get_header();

 ?>

<div class="container bg">
	<main class="main book books book_col5 hide-last cf">

		<?php render_template( 'partials/_book-list.php', array( 'books' => $params['newreleases'], 'title' => 'New Releases', 'link' => home_url('books/category/new-releases') ) ); ?>
		<?php render_template( 'partials/_book-list.php', array( 'books' => $params['bestsellers'], 'title' => 'Best Sellers', 'link' => home_url('books/category/best-sellers') ) ); ?>
		<?php render_template( 'partials/_book-list.php', array( 'books' => $params['comingsoon'], 'title' => 'Coming Soon', 'link' => home_url('books/category/coming-soon') ) ); ?>
 
		<section id="browse" class="browse">
			<h3 class="browse-title">Browse By Category</h3>
			<ul class="list-unstyled">
				<?php foreach($params['categories'] as $slug => $title) { ?>
					<li class="browse-item">
						<a class="pill" href="<?php echo home_url('/books/category/' . $slug ); ?>">
							<?php echo $title; ?>
						</a>
					</li>
				<?php } ?>
			</ul>
		</section>

	</main>
</div>

<?php get_footer(); ?>
