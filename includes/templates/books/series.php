<?php
/**
 * The template for catalog pages
 *
 * @package WaterBrook Multnomah
 */

get_header(); 

?>
<div class="container bg">
<main class="main books-list book_col5 cf">

<?php wp_nav_menu( array( 'theme_location' => 'book-sub-nav', 'container_class' => 'sub-nav', 'menu_class' => 'list-unstyled') ); ?>


	<h2>Browsing <?php echo $params['title']; ?></h2>

	<section class="book-list">
		<ul class="list-unstyled grid">
		<?php foreach($params['books'] as $key => $book) { ?>
			<li class="book-list-item">
				<div class="p-box">
				<a href="<?php echo $book->seoFriendlyUrl; ?>" class="book-list-link">
					<div class="book-list-image"><img src="<?php echo $book->coverImage; ?>"></div>
					<?php if($book->seriesNumber > 0) { ?>
						<p class="book-list-item-title">Book <?php echo $book->seriesNumber ?></p>
					<?php } ?>
					<p class="book-list-item-title"><?php echo $book->title; ?></p>
				</a>
				</div>
			</li>
		<?php } ?>
		</ul>

		<?php
		if( isset( $params['page'] ) ) {
			base_pagination( $params['page'], ceil( $params['record_count'] / 20), '?page=%#%', add_query_arg('page', '%#%') );
		}
		?>
	</section>

</main>

</div>



<?php get_footer(); ?>