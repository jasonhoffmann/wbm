<?php
/**
 * The template for catalog pages
 *
 * @package WaterBrook Multnomah
 */

get_header(); 

?>
<div class="container bg">


<?php wp_nav_menu( array( 'theme_location' => 'book-sub-nav', 'container_class' => 'sub-nav', 'menu_class' => 'list-unstyled') ); ?>
<main class="main books-list book_col5 cf">

	<h2>Browsing <?php echo $params['title']; ?></h2>
	<h4><?php echo $params['record_count']; ?> results</h4>
	
	<section class="book-list">
		<ul class="list-unstyled grid">
		<?php foreach($params['books'] as $book) { ?>
			<li class="book-list-item">
				<div class="p-box">
				<a href="<?php echo $book->seoFriendlyUrl; ?>" class="book-list-link">
					<div class="book-list-image"><img src="<?php echo $book->coverImage; ?>"></div>
					<p class="book-list-item-title"><?php echo $book->title; ?></p>
					<p class="book-list-item-author"> 
						<?php echo $book->author; ?>
					</p>
					<?php if( $params['comingsoon'] && $book->onSaleDate ) { ?> 
						<p class="book-list-item-author">On Sale: <?php echo $book->onSaleDate; ?></p> 
					<?php } ?>
				</a>
				</div>
			</li>
		<?php } ?>
		</ul>

		<?php
		if( isset( $params['page'] ) && $params['title'] !== 'Coming Soon' ) {
			base_pagination( $params['page'], ceil( $params['record_count'] / 20), '?page=%#%', add_query_arg('page', '%#%') );
		}
		?>
	</section>

</main>

</div>



<?php get_footer(); ?>