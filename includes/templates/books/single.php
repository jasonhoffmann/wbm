<?php
/**
 * The template for individual book pages
 *
 * @package WaterBrook Multnomah
 */

get_header();

$book = $params['book'];
$events = $params['events'];
$series = $params['series'];
$related_query = $params['related_query'];
$reviews = $params['reviews'];
$content = $params['content'];
$also_like = $params['also_like'];
$sneak_peek = $params['sneak_peek'];
?>

<div class="container bg">
<main class="main book">
	<section class="book-header span_9 grid">

		<div class="book-image span_4">
			<img src="<?php echo $book->coverImage; ?>">

		</div>

		<div class="book-header-info span_8">
			<h3 class="book-title"><?php echo $book->title; ?></h3>
			<?php if( $book->subtitle ) { ?>
				<h4 class="book-subtitle"><?php echo $book->subtitle; ?></h4>
			<?php } ?>
			<p class="book-author"><?php echo $book->authorLinks; ?></p>

			<div class="book-meta">
				<?php if($book->categories) { ?>
					<h4 class="book-category-title">
						Category: <?php echo $book->categories; ?>
					</h4>
				<?php } ?>
				<?php if( isset( $book->seriesTitle ) ) { ?>
					<h4 class="book-series">
						Part of the <?php echo $book->seriesTitle; ?> Series
					</h4>
				<?php } ?>
			</div>

			<ul class="list-unstyled grid book-formats">
				<?php foreach( $book->formats as $format => $data ) { ?>
					<li class="span_4 book-format grid">
						<div class="span_2 icon icon-format">
							<svg viewBox="0 0 100 100">
							  <use xlink:href="#<?php echo urlencode( $format ); ?>"></use>
							</svg>
						</div>
						<div class="span_8 book-info">
							<h4 class="book-info-title"><?php echo $format; ?></h4>
							<h4 class="book-info-title book-info-price"> $<?php echo $data['price']; ?></h4>
						</div>
							<p class="book-format-info span_12"><?php echo $data['date']; ?> | <?php if( $data['pages'] ) { echo $data['pages'] . ' Pages'; } ?></p>
							<p class="book-format-info span_12">ISBN: <?php echo $data['isbn']; ?></p>
							<?php echo generate_buy_button( $data['isbn'], 'Buy Now', $format, $book->workId); ?>
					</li>

				<?php } ?>
			</ul>

			<div class="book-actions grid">
				<?php if( $book->hasInsight ) { ?>
					<a class="span_3 book-excerpt js-excerpt" target="_blank" href="http://insight.randomhouse.com/widget/v4/?width=600&amp;isbn=<?php echo $book->isbn ?>">
						<svg viewBox="0 0 100 100" class="icon icon-start">
						  <use xlink:href="#start-reading"></use>
						</svg>
						Start Reading
					</a>
				<?php } ?>
					<a class="span_3 book-excerpt" href="http://images.randomhouse.com/cover/tif/<?php echo $book->isbn; ?>">
						<svg viewBox="0 0 100 100" class="icon icon-start">
						  <use xlink:href="#image"></use>
						</svg>
						Download Cover
					</a>

				<div class="social-links book-social-links span_6">
					<a class="social-link js-social" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $book->seoFriendlyUrl; ?>">
						<span class="icon icon-social-facebook">
							<svg viewBox="0 0 100 100" class="icon">
							  <use xlink:href="#social-facebook"></use>
							</svg>
						</span>
						<span class="social-title">Share</span>
					</a>
					<a class="social-link twitter js-social" href="https://twitter.com/home?status=<?php echo $book->title . ' by ' . $book->author . ' - WaterBrook & Multnomah @WaterBrookPress ' . $book->bitly; ?>">
						<span class="icon icon-social-twitter">
							<svg viewBox="0 0 100 100" class="icon">
							  <use xlink:href="#social-twitter"></use>
							</svg>
						</span>
						<span class="social-title">Tweet</span>
					</a>
					<a class="social-link pinterest js-social" href="https://pinterest.com/pin/create/button/?url=<?php echo $book->seoFriendlyUrl; ?>&amp;media=<?php echo $book->coverImage; ?>&amp;description=<?php echo $book->title . ' by ' . $book->author . ' | WaterBrook & Multnomah'; ?>">
						<span class="icon icon-social-pinterest">
							<svg viewBox="0 0 100 100" class="icon">
							  <use xlink:href="#social-pinterest"></use>
							</svg>
						</span>
						<span class="social-title">Pin</span>
					</a>
				</div>

		</div>

		</div>
	</section>

<section class="book-container grid">
	<div class="span_9 tab-content">

		<ul id="tab-hash" class="book-tabs tab-nav list-unstyled" role="tablist">
			<li role="presentation" class="active"><a href="#details" aria-controls="details" role="tab" data-toggle="tab">Details</a></li>
			<li role="presentation"><a class="" href="#press" aria-controls="press" role="tab" data-toggle="tab">Press</a></li>
			<?php if( $reviews ) { ?>
				<li role="presentation"><a class="" href="#reviews" aria-controls="reviews" role="tab" data-toggle="tab">Reviews</a></li>
			<?php } ?>
		</ul>

		<div id="details" class="tab-pane active">

				<div class="book-section">
					<h3>About <em><?php echo $book->title; ?></em></h3>
					<div class="book-flapcopy">
						<?php echo $book->description; ?>
					</div>
				</div>

				<?php if( $book->praises ) { ?>
					<div class="book-section">
						<h3>Praise</h3>
						<p><?php echo $book->praises; ?></p>
					</div>
				<?php } ?>

				<?php foreach($book->authors as $author) { ?>
					<?php if($author->bio) { ?>
						<div class="book-section">
							<h3>About <?php echo $author->name; ?></h3>
							<?php echo $author->bio; ?>
						</div>
					<?php } ?>
				<?php } ?>

				<?php if($book->videoRelatedLinks) { ?>
					<div class="book-section">
						<h3>Featured Videos</h3>
						<div class="grid">
						<?php foreach( $book->videoRelatedLinks as $video ) { ?>
							<div class="span_6">
								<div class="video-container">
									<?php echo $video->embed; ?>
								</div>
								<h4><?php echo $video->linkText; ?></h4>
							</div>
						<?php } ?>
						</div>
					</div>
				<?php } ?>

				<div class="book-section book-product-details">
					<h3>Product Details</h3>
					<p><?php echo $book->totalPages; ?> pages | Published by <?php echo normalize_imprint( $book->imprint ); ?></p>
					<p><strong>On Sale Date:</strong> <?php echo $book->onSaleDate; ?></p>
					<p><strong>Trim Size:</strong> <?php echo $book->trimSize; ?></p>
					<p><strong>Carton Quantity:</strong> <?php echo $book->cartonQty; ?></p>
				</div>

				<?php if( $book->series ) { ?>
					<div class="book-section">
						<h3>Also in the <em><?php echo $book->seriesTitle; ?></em> Series</h3>
						<section class="book-list">
							<ul class="list-unstyled grid">
							<?php foreach($series as $series_book) { ?>
								<li class="book-list-item span_3">
									<a href="<?php echo $series_book->seoFriendlyUrl; ?>" class="book-list-link">
										<div class="book-list-image"><img src="<?php echo $series_book->coverImage; ?>"></div>
										<div class="book-list-item-title"><?php echo $series_book->title; ?>
									</div>
									</a>
								</li>
							<?php } ?>
							</ul>
						</section>
					</div>
				<?php } ?>
				
				<?php if( $events ) { ?>
					<div class="book-section">
						<h3>Upcoming Events</h3>
						
						<table class="table table-responsive table-bordered">
							<tbody>
							<?php foreach($events as $event) { ?>
								<tr>
									<th scope="row"><?php echo $event->description; ?></th>
									<td><?php echo $event->eventDate . ' @ ' . $event->eventTime; ?></td>
									<td><a target="_blank" href="<?php echo $event->url; ?>"><?php echo $event->location . ', ' . $event->city . ' ' . $event->state; ?></a></td>
								</tr>
							<?php } ?>
							</tbody>
						</table>
					</div>
				<?php } ?>

				<?php if( $sneak_peek->have_posts() ) { ?>
					<?php while( $sneak_peek->have_posts() ) { $sneak_peek->the_post(); ?>
					<div class="book-section">
						<h3>Sneak Peek</h3>
						<?php echo strip_images( get_the_content() ); ?>
					</div>
				<?php } } ?>


				<?php if( $related_query->have_posts() ) { ?>
					<div class="book-section book-related">
						<h3>News</h3>
						<?php while( $related_query->have_posts() ) { $related_query->the_post(); ?>
							<article class="entry">
							<div class="entry-content">
								<h3 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
								<div class="entry-meta">
									<?php the_time( get_option( 'date_format' ) ); ?>
								</div>

								<?php if($video = get_post_meta($post->ID, "video", true)) {
						         	// This takes all of the old style embed codes in the database and replaces them with the new YouTube embed
						         	if(strpos($video, "youtube.com") !== false) {
						         		preg_match('/(?:\/|embed\/)([0-9a-zA-Z-_]+)(?:[&|?]|\")/', $video, $video);
						         		$video = $video[1];
						         	}
						         ?>
						         <div class="video-wrapper">
							         <div class="video-container">
							        	<iframe src="http://www.youtube.com/embed/<?= $video ?>" frameborder="0" allowfullscreen></iframe>                                    				
									</div>
								</div>
								<?php } else { 
									the_excerpt(); } ?>
							</div>
							</article>
					<?php } ?>
					</div>
				<?php } ?>

		</div>

		<div id="press" class="tab-pane">
			<div class="book-section">
				<?php $title_param = urlencode( $book->title ); ?>
					<a class="btn btn-default" href="<?php echo home_url('/request-review-copy/?book_title=' . $title_param ); ?>">Request a Review Copy</a>
					<?php 
						$author_names = '';
						foreach($book->authors as $author) {
							$author_names .= urlencode($author->name) . '+';
						}
						$author_names = rtrim($author_names, '+');
					?>
					<a class="btn btn-default" href="<?php echo home_url('/request-interview/?author=' . $author_names ); ?>">Request an Interview</a>
			</div>

			<div class="book-section">
				<h3>Media</h3>
				<a href="http://images.randomhouse.com/cover/tif/<?php echo $book->isbn; ?>">
					<p>Download Hi-Res Book Cover</p> 
				</a>
				<?php foreach($book->authors as $author) {
						if( $author->hasAuthorPhoto ) { ?>
					<a target="_blank" href="http://images.randomhouse.com/author/<?php echo $author->id ?>">
						Download Hi-Res Author Photo
					</a>
					<?php } 
				} ?>
			</div>
			<?php if($content->pressrelease) {  
				 $press_contact = strip_tags($content->pressreleasecontact); 
				 $press_contact_email = str_replace(' ', '.', $press_contact) . '@penguinrandomhouse.com';
			?>
				<div class="book-section">
					<h3>Press Release</h3>
					<?php echo $content->pressrelease; ?>

					<h4 class="book-publicist-title">Contact Publicist</h4>
					<a class="book-publicist-link" href="mailto:<?php echo $press_contact_email; ?>"><?php echo $content->pressreleasecontact; ?></a>
				</div>
			<?php } ?>
		</div>

		<?php if( $reviews ) { ?>
		<div id="reviews" class="tab-pane">
				<?php foreach($reviews as $review) { ?>
				<div class="book-section">
					<h4 class="review-author">
						<a target="_blank" href="<?php echo $review->blog_url; ?>">By <?php echo $review->blogger; ?></a>
						<div class="rating">
							<span style="width: <?php echo ( intval($review->ranking) * 20 ); ?>%" class="rating-stars"></span>
						</div>
					</h4>
		
					<p><?php echo strip_tags( $review->text_preview, '<br>' ); ?></p>
					<p><a target="_blank" href="<?php echo $review->review_url; ?>">Read the Full Review</a></p>
				</div>
				<?php } ?>
			
		</div>
		<?php } ?>

		<div class="book-also-like book_col5 hide-last">
			<h3 class="push-left">Other Books You Might Enjoy</h3>
			<?php render_template( 'partials/_book-list.php', array( 'books' => $also_like ) 	); ?>
		</div>
	</div>

	<aside class="book-side index-side side span_3">
	<?php if($book->relatedLinks) { ?>
		<div>
			<h4>Related Links</h4>
			<ul class="list-unstyled side-links">
			<?php foreach( $book->relatedLinks as $relatedLink ) { ?>
				<li>
					<a target="_blank" class="side-link" href="<?php echo $relatedLink->url; ?>">
						<?php echo $relatedLink->linkText; ?>
					</a>
				</li>
			<?php } ?>
			</ul>
		</div>
	<?php } ?>

	<div class="newsletter-side">
		<?php get_template_part( 'includes/templates/partials/_newsletter', 'form' ); ?>
	</div>

		<?php
			adsanity_show_ad_group(
				array(
					'group_ids'	=> array(30732),
					'num_ads'	=> 1,
					'num_columns'	=> 1
				)
			);
		?>
	</aside>
	</section>

</div>
</main>

<?php get_footer(); ?>