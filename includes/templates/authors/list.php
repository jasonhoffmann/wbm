<?php
/* 
 * Author List Page
 *
 */
get_header();
$authors = $params['authors'];
?>
<div class="container bg">
<main class="main authors books-list cf">

		<ul class="grid authors-index-list">
		<?php foreach($authors as $author) { ?>
			<li class="book-list-item author-list-item span_3">
			<div class=" author-list-box">
			<a href="<?php echo $author->seoFriendlyUrl; ?>">
			<?php if($author->hasAuthorPhoto) { ?>
					<div style="background-image: url(<?php echo $author->coverImage; ?>); background-size: cover;" class="author-image-box"></div>
			<?php } else { ?>
				<div class="author-image-box author-image-initials"><p class="author-initials"><?php echo $author->initials; ?></p></div>
			<?php } ?>
			</a>
				<div>
					<h4 class="book-list-item-title"><a href="<?php echo $author->seoFriendlyUrl; ?>"><?php echo $author->name; ?></a></h4>
					<?php 
					// Hack to change Samuel Rodriguez's author of book. 
					// TODO: Need to change this in MyHouse then fix.
					if( $author->authorId === 244262 ) { ?>
					<p class="book-list-item-author">Author of <em>Be Light</em></p>
					<?php } else { ?>
					<p class="book-list-item-author">Author of <em><?php echo $author->authorOf->title; ?></em></p>
					<?php } ?>
				</div>
			</div>
			</li>
		<?php } ?>
		</ul>

		<?php
			echo base_pagination( $params['page'], ceil( $params['record_count'] / 24), '?page=%#%', add_query_arg('page', '%#%') );
		?>

</main>

</div>


<?php get_footer(); ?>