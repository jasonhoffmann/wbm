<?php
/**
 * The template for individual book pages
 *
 * @package WaterBrook Multnomah
 */

get_header();
$news = $params['news'];
$author = $params['author'];
?>

<div class="container bg">
<main class="main book author">
	<section class="book-header span_9 grid">
		<?php if($author->hasAuthorPhoto) { ?>
			<div class="book-image span_4">
				<img src="<?php echo $author->coverImage; ?>">
			</div>
		<?php } ?>
		<div class="book-header-info span_8">
			<?php if($author->description) { ?>
				<h3 class="author-item-title">About <?php echo $author->name; ?></h3>
				<p><?php echo $author->description; ?></p>
			<?php } else { ?>
				<h3 class="author-item-title"><em><?php echo $author->name; ?></em></h3>
			<?php } ?>
			<?php if($author->social) { ?>
				<div class="social-links book-social-links author-social-links">
				<?php foreach( $author->social as $name => $socialLink ) { $lc = strtolower($name);?>
					<a target="_blank" class="social-link <?php echo $lc; ?>" href="<?php echo $socialLink->url; ?>">
						<span class="icon icon-social-<?php echo $lc; ?>">
							<svg viewBox="0 0 100 100" class="icon">
							  <use xlink:href="#social-<?php echo $lc; ?>"></use>
							</svg>
						</span>
						<span class="social-title"><?php echo $name; ?></span>
					</a>
				<?php } ?>
				</div>
			<?php } ?>
	</section>

<section class="book-container author-container grid">
	<div class="span_9 tab-content">
		<div class="tab-pane active m-border-top" id="details">
			<?php if( $author->works ) { ?>
				<div class="book-section">
					<h3>Books By <?php echo $author->name; ?></h3>
					<section class="book-list">
						<ul class="list-unstyled grid">
						<?php foreach($author->works as $work) { ?>
							<li class="span_3 book-list-item">
								<a href="<?php echo $work->seoFriendlyUrl; ?>" class="book-list-link">
									<div class="book-list-image"><img src="<?php echo $work->coverImage; ?>"></div>
									<div class="book-list-item-title"><?php echo $work->title; ?>
								</div>
							</a>
							</li>
						<?php } ?>
						</ul>
					</section>
				</div>
			<?php } ?>

			<?php if( $author->events ) { ?>
				<div class="book-section">
					<h3>Upcoming Events</h3>
					
					<table class="table table-responsive table-bordered">
						<tbody>
						<?php foreach($author->events as $event) { ?>
							<tr>
								<th scope="row"><?php echo $event->title->title; ?></th>
								<td><?php echo $event->eventDate . ' @ ' . $event->eventTime; ?></td>
								<?php $event_url = 'https://maps.google.com?daddr=' . urlencode( $event->address1 . ' ' . $event->address2 .  ' ' . $event->city . ' ' . $event->state); ?> 
								<td><a target="_blank" href="<?php echo $event_url; ?>"><?php echo $event->location . ', ' . $event->city . ' ' . $event->state; ?></a></td>
							</tr>
						<?php } ?>
						</tbody>
					</table>
				</div>
			<?php } ?>

			<?php if( $news->have_posts() ) { ?>
				<div class="book-section author-related">
				<h3>From the Blog</h3>
				<?php while( $news->have_posts() ) { $news->the_post(); ?>
					<?php get_template_part('includes/templates/partials/_blog', 'index'); ?>
				<?php } ?>
				</div>
			<?php } ?>
		</div>


	</div>

	<aside class="index-side span_3">
	<?php if($author->relatedLinks) { ?>
		<div>
			<h4>Related Links</h4>
			<ul class="list-unstyled side-links">
			<?php foreach( $author->relatedLinks as $relatedLink ) { ?>
				<li>
					<a target="_blank" class="side-link" href="<?php echo $relatedLink->url; ?>">
						<?php echo $relatedLink->linkText; ?>
					</a>
				</li>
			<?php } ?>
			</ul>
		</div>
	<?php } ?>
		<div class="grid">
			<div class="span_12 newsletter-side">
				<h4 class="newsletter-title">Follow <?php echo $author->name; ?></h4>
				<p class="newsletter-caption">Sign up for <?php echo $author->name; ?> related news and releases from Penguin Random House.</p>
			 
			    <div class="js-subscribe">
			    	<div class="alert alert-danger js-error" style="display:none;"></div>
			    	<div class="alert alert-success js-success" style="display:none;">Thanks for signing up!</div>
			    	<form name="subscribe" id="bbl-newsletter" method="POST" target="_top">
			    		<div class="input-append">
			    	    	<input type="email" id="inputName<?php if($newsletter == 0): echo '1'; $newsletter++; else: echo $newsletter; $newsletter++; endif;?>" name="email" placeholder="Email address" class="inputName js-email textbox" />
			    	    	<input type="hidden" name="ajaxurl" value="http://<?php echo $_SERVER['SERVER_NAME']; ?>/wp-admin/admin-ajax.php">
			    	    	<input type="hidden" name="nonce" value="<?php echo wp_create_nonce('wbm-author-signup'); ?>">
			    	    	<input type="hidden" name="action" value="author_signup">
			    	    	<input id="authorField" type="hidden" name="author" value="true">
			    	    	<input id="authorID" type="hidden" name="authorID" value="<?php echo $author->authorId; ?>">
			    	    	<input id="author_name" type="hidden" name="author_name" value="<?php echo $author->name; ?>">
			    	    	<input type="submit" class="submit btn btn-large" type="submit" value="Sign Up" />
			    	    </div>
			    	</form>
			    </div>
			</div>

		</div>
	</aside>
</div>

<?php get_footer(); ?>