<?php
/* 
 * Author Index Page
 *
 */
get_header();
$authors = $params['authors'];
?>
<div class="container bg">
<main class="main authors books-list cf">
	<div class="author-index-title">
		<span class="arrow-down">Featured Authors</span>
	</div>

		<ul class="grid authors-list">
		<?php foreach($authors as $author) { ?>
			<li class="author-index-item span_3">
			<div class=" author-list-box">
			<a href="<?php echo $author->seoFriendlyUrl; ?>">
			<?php if($author->hasAuthorPhoto) { ?>
					<div style="background-image: url(<?php echo $author->coverImage; ?>); background-size: cover;" class="author-image-box"></div>
			<?php } else { ?>
				<div class="author-image-box author-image-initials"><p class="author-initials"><?php echo $author->initials; ?></p></div>
			<?php } ?>
			</a>
				<div>
					<h4 class="book-list-item-title"><a href="<?php echo $author->seoFriendlyUrl; ?>"><?php echo $author->name; ?></a></h4>
					<?php 
					// Hack to change Samuel Rodriguez's author of book. 
					// TODO: Need to change this in MyHouse then fix.
					if( $author->authorId === 244262 ) { ?>
					<p class="book-list-item-author">Author of <em>Be Light</em></p>
					<?php } else { ?>
					<p class="book-list-item-author">Author of <em><?php echo $author->authorOf->title; ?></em></p>
					<?php } ?>
				</div>
			</div>
			</li>
		<?php } ?>
		</ul>

		<div class="book-list-btn author-index-btn">
			<a href="<?php echo home_url('/authors/all'); ?>" class="btn">View All Authors</a>
		</div>

</main>

</div>


<?php get_footer(); ?>