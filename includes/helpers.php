<?php
/**
* A few helpers used by templates and WP functions
* 
* @since 1.0
*/

/**
* Generates pagination on blog + books page
* 
* @since 1.0
*
* @see paginate_links()
*
* @param int $current 		current page
* @param int $total 		total number of pages
* @param string $format 	url parameter format
*
*/
function base_pagination( $current, $total, $format = '/page/%#%', $base = '%_%' ) {
    $paginate_links = paginate_links( array(
    	'base' => $base,
        'format' => $format,
        'current' => max( 1, $current ),
        'total' => $total,
        'mid_size' => 5
    ) );

    if ( $paginate_links ) {
        echo '<div class="pagination">';
        echo $paginate_links;
        echo '</div>';
    }
}


/**
* Generate a list of retailer links
* 
* @since 1.0
*
* @param int $format 		type of book
* @param int $isbn 			must be ISBN not Workid
*
*/
function get_all_retailer_links($format, $isbn) {
	$retailer_links = array();
	if( $format === 'Paperback' || $format === 'Ebook' || $format === 'Hardcover' || $format === 'CD') {
		$retailer_links['Amazon'] = 'http://links.penguinrandomhouse.com/type/affiliate/isbn/' . $isbn . '/siteID/8001/retailerid/7/trackingcode/randohouseinc11082-20';
		$retailer_links['Barnes & Noble'] = 'http://links.penguinrandomhouse.com/type/affiliate/isbn/' . $isbn . '/siteID/8001/retailerid/2/trackingcode/PRH38424DF6D6';
		$retailer_links['Books A Million'] = 'http://links.penguinrandomhouse.com/type/affiliate/isbn/' . $isbn . '/siteID/8001/retailerid/5/trackingcode/PRH38424DF6D6';
		$retailer_links['ChristianBook.com'] = 'http://www.christianbook.com/Christian/Books/easy_find?Ntk=keywords&action=Search&Ntt=' . $isbn;
		$retailer_links['Parable'] = 'http://parable.com/search?keywords=' . $isbn;
	}

	if( $format === 'Paperback' || $format === 'Hardcover' || $format === 'CD') {
		$retailer_links['Indiebound'] = 'http://links.penguinrandomhouse.com/type/affiliate/isbn/'. $isbn . '/siteID/8001/retailerid/6/trackingcode/penguinrandom';
		$retailer_links["Powell's"] = 'http://links.penguinrandomhouse.com/type/affiliate/isbn/' . $isbn . '/siteID/8001/retailerid/9/trackingcode/PRH38424DF6D6';
		$retailer_links['Target'] = 'http://links.penguinrandomhouse.com/type/affiliate/isbn/' . $isbn . '/siteID/8001/retailerid/23/trackingcode/PRH38424DF6D6';
		$retailer_links['Walmart'] = 'http://links.penguinrandomhouse.com/type/affiliate/isbn/' . $isbn . '/siteID/8001/retailerid/4/trackingcode/PRH38424DF6D6';
		$retailer_links['Cokesbury'] = 'https://www.cokesbury.com/forms/search.aspx?ddlSearchScope=&txtSearchQuery=' . $isbn;
		$retailer_links['Lifeway Christian Stores'] = 'http://www.lifeway.com/Keyword/' . $isbn;
		$retailer_links['Book Passage'] = 'http://www.bookpassage.com/book/' . $isbn;
		$retailer_links['Family Christian Stores'] = 'http://www.familychristian.com/catalogsearch/result/?q=' . $isbn . '&skey=effc375292556732fecb7e36ae43d94f';
		$retailer_links['Mardel'] = 'http://www.mardel.com/search/?text=' . $isbn;
		$retailer_links['Tattered Cover'] = 'http://www.tatteredcover.com/book/' . $isbn . '/&affiliateId=000266';
	}

	if( $format === 'Ebook' || $format === 'Audiobook Download' ) {
		$retailer_links['iBooks'] = 'http://links.penguinrandomhouse.com/type/affiliate/isbn/' . $isbn . '/siteID/8001/retailerid/3/trackingcode/PRH38424DF6D6';
	}

	if( $format === 'Ebook' ) {
		$retailer_links['Google Play'] = 'http://links.penguinrandomhouse.com/type/affiliate/isbn/' . $isbn . '/siteID/8001/retailerid/22/trackingcode/PRH38424DF6D6';
		$retailer_links['Kobo'] = 'http://links.penguinrandomhouse.com/type/affiliate/isbn/' . $isbn . '/siteID/8001/retailerid/1/trackingcode/PRH38424DF6D6';
	}

	if( $format === 'Audiobook Download' ) {
		$retailer_links['Audible'] = 'http://links.penguinrandomhouse.com/type/affiliate/isbn/' . $isbn . '/siteID/8001/retailerid/15/trackingcode/PRH38424DF6D6';
	}
	ksort($retailer_links);
	return $retailer_links;
}


/**
* Create the HTML for a buy button
* Includes a list of all retailer links
* 
* @since 1.0
*
* @see get_all_retailer_links
*
* @param int $isbn 			must be ISBN not Workid
* @param int $status 		title of the button
*
*/
function generate_buy_button( $isbn, $status = 'Buy', $format = 'Paperback', $work_id = '' ) {
	global $wp;
	$retailer_links = get_all_retailer_links( $format, $isbn );
	$current_url = home_url( $wp->request );
	$form_action = $format === 'Audiobook Download' || $format === 'CD' ? 
								'http://www.penguinrandomhouseaudio.com/book/' . $work_id . '/' : 
								'https://cart.penguinrandomhouse.com/cart.php';

	$output  = '<button type="button" data-format="' . strtolower( urlencode( $format ) ) . '" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
	$output .= 'Buy Now';
	$output .= '</button>';

	$output .= '<ul class="dropdown-menu">';
	$output .= '<form target="_blank" action="' . $form_action . '" method="post" name="cartform" id="cartform" class="buy_button">
		<input type="hidden" name="cartCount" value="1">
		<input type="hidden" id="siteName" name="siteName" value="waterbrook">
		<input type="hidden" id="referringSite" name="referringSite" value="waterbrook">
		<input type="hidden" id="itemISBN" name="itemISBN" value="' . $isbn . '">
	    <input type="hidden" id="referringSite" name="referringSite" value="waterbrookmultnomah">
		<input type="hidden" id="siteReturnURL" name="siteReturnURL" value="' . $current_url . '">
		<input type="hidden" id="itemReturnURL" name="itemReturnURL" value="' . $current_url . '">
	    <input id="itemQuantity" type="hidden" name="itemQuantity" value="1">
	    <div class="add-to-cart row">
	    	<img src="' . get_template_directory_uri() . '/assets/images/logo-square.png">
	    	<input class="btn" type="submit" name="addToCartSubmit" id="modal_cart_button" class="add_submit" alt="Add to Cart" value="Add To Cart">
		</div>
	</form>';

	foreach( $retailer_links as $retailer => $link) {
		$output .= '<li><a class="js-retailer" target="_blank" href="' . $link . '">' . $retailer . '</a></li>';
	}

	$output .= '</ul>';

	return $output;
}
//     add_filter( 'the_content', 'my_autoembed_adjustments', 7 );


function filter_excerpt( $content ) {
	global $post;

	$text = get_the_content( '' );
	$text = apply_filters( 'the_content', $text);
	$tags_to_strip = array('table', 'img', 'strong');


	$video = get_post_meta( get_the_ID(), 'video', true );
	if($video) {
		$text = $video . $text;
	}
	
	$excerpt_length = apply_filters('excerpt_length', 140);
	$excerpt_more = apply_filters('excerpt_more', ' <a href="'. get_permalink($post->ID) . '">[ &hellip; ]</a>' );

		    $words = preg_split("/[\n\r\t ]+/", $text, $excerpt_length + 1, PREG_SPLIT_NO_EMPTY);
		    if ( count($words) > $excerpt_length ) {
		        array_pop($words);
		        $text = implode(' ', $words);
		        $text = force_balance_tags( $text . $excerpt_more );
		    } else {
		        $text = implode(' ', $words);
		    }
		    $processed = htmLawed($text); 

	return $processed;
}
remove_all_filters( 'get_the_excerpt' );
remove_all_filters( 'the_excerpt' );
add_filter( 'the_excerpt', 'filter_excerpt' );


function strip_images($content) {
	$content = preg_replace("/<img[^>]+\>/i", "", $content); 
	return $content;
}

function make_bitly_url($url,$login,$appkey='R_a15af236123832b64966ac24c05fb587',$format = 'json',$version = '2.0.1'){
	//create the URL
	$bitly = 'http://api.bit.ly/shorten?version='.$version.'&longUrl='.urlencode($url).'&login='.$login.'&apiKey='.$appkey.'&format='.$format;
	//get the url
	//could also use cURL here
	$response = file_get_contents($bitly);

	
	//parse depending on desired format
	if(strtolower($format) == 'json')
	{
		$json = @json_decode($response,true);
		return $json['results'][$url]['shortUrl'];
	}
	else //xml
	{
		$xml = simplexml_load_string($response);
		return 'http://bit.ly/'.$xml->results->nodeKeyVal->hash;
	}
}

/**
* Normalize Imprint Titles
* Takes Waterbrook Press + Multnomah Books and changes it
* to match appropriate style
* 
* @since 1.0
*
* @param int $title 			Must be of the types above
*
*/
function normalize_imprint( $imprint ) {
	if( $imprint === 'Multnomah Books' ) {
		$imprint = 'Multnomah';
	} elseif( $imprint === 'WaterBrook Press') {
		$imprint = 'WaterBrook';
	} elseif( $imprint === 'The WaterBrook Multnomah Publishing Group' ) {
		$imprint = 'WaterBrook & Multnomah';
	}

	return $imprint;
}

// /**
// * Filter excerpt shown on blog pages
// * 
// * @since 1.0
// *
// * @param string $content 	content of page
// *
// */
// function filter_excerpt( $content ) {
// 	global $post;

// 	$text = get_the_content( '' );
// 	$text = apply_filters( 'the_content', $text );

// 	$text = str_replace( ']]>', ']]&gt;', $text );

// 	$text = strip_tags( $text, '<p>, <em>, <strong>, <a>, <iframe>, <object>, <param>, <embed>, <video>, <audio>, <div>, <h3>' );

// 	// $text = text_excerpt( $text, '150', $length_type, $finish );
// 	$excerpt_length = apply_filters('excerpt_length', 150);

// 		    $excerpt_more = apply_filters('excerpt_more', '<a class="read-more" href="'. get_permalink($post->ID) . '"> Read More ></a>' );

// 		    $words = preg_split("/[\n\r\t ]+/", $text, $excerpt_length + 1, PREG_SPLIT_NO_EMPTY);
// 		    if ( count($words) > $excerpt_length ) {
// 		        array_pop($words);
// 		        $text = implode(' ', $words);
// 		        $text = force_balance_tags( $text );
// 		        $text = $text . $excerpt_more;
// 		    } else {
// 		        $text = implode(' ', $words);
// 		    }

// 	return $text;
// }
// remove_all_filters( 'get_the_excerpt' );
// remove_all_filters( 'the_excerpt' );
// add_filter( 'get_the_excerpt', 'filter_excerpt' );