<?php

/**
* Code based on WordPress' pagination, used to generate links
* for AJAX requests
* 
* @since 1.0
*
* @see paginate_links
* 
* @param arr $args 			custom arguements
*
*/
function custom_links( $args = '' ) {
 
    $defaults = array(
        'total' => $total,
        'current' => $current,
        'end_size' => 1
    );
 
    $args = wp_parse_args( $args, $defaults );
 

    $total = (int) $args['total'];
    if ( $total < 2 ) {
        return;
    }
    $current  = (int) $args['current'];
    $end_size = (int) $args['end_size'];
    if ( $end_size < 1 ) {
        $end_size = 1;
    }
    $r = '';
    $page_links = array();

    for ( $n = 1; $n <= $total; $n++ ) {
    	if ( $n == $current ) {
        	$page_links[] = "<span class='page-numbers current'>" . $args['before_page_number'] . number_format_i18n( $n ) . $args['after_page_number'] . "</span>";
        } else {
        	$page_links[] = "<a id='" . number_format_i18n( $n ) . "' class='js-facetpage page-numbers' href='#'>" . $args['before_page_number'] . number_format_i18n( $n ) . $args['after_page_number'] . "</a>";
        }
    }
    $r = join("\n", $page_links);
    return $r;
}


/**
* AJAX call for search facts
*
* Gets the parameters, grabs appropriate books from PRH API
* and returns properly formatted HTML
* 
* @since 1.0
*
*/
function wbm_facets() {
	
	$imprint = $_GET['imprint'];
	$search = $_GET['searchTerm'];
	$age_range = $_GET['ages'] ? $_GET['ages'] : '';
	$formats = $_GET['formats'] ? $_GET['formats'] : '';
	$categories = $_GET['categories'] ? $_GET['categories'] : '';
	$page = $_GET['page'] ? $_GET['page'] : 1;
	$release = $_GET['release'];

	$rows = 10;
	$start = ( $page - 1 ) * $rows;
	if( $start < 0 ) { $start = 0; }
	
	if($imprint) {
		$imprints_array = array();
		foreach($imprint as $imprint) {
			$imprint = urlencode($imprint);
			array_push($imprints_array, $imprint);
		}
	}

	$release_string = '';
	if($release) {
		if( in_array( 'new-release', $release) ) {
			$release_string .= '&newRelease=true';
		}
		if( in_array( 'forthcoming', $release ) ) {
			$release_string .='&forthcomingRelease=true';
		}
	}
	$params = array(
		'formats' => $formats,
		'ageRange' => $age_range,
		'imprints' => $imprints_array,
		'release' => $release_string,
		'categories' => $categories,
		'term' => $search,
		'params' => array(
			'rows' => 10,
			'start' => $start
			)
		);

	$get_books = new RandomHouse;
	$search_books = $get_books->search( $params );
	$total = $get_books->get_record_count();
	$output = render_template( 'partials/_search-results.php', array( 'books' => $search_books, 'total' => $total) );

	$total = ceil( $total / 10 );
	$paginate_links = custom_links( array(
		'base' => 'http://wbm.dev/%_%',
	    'current' => $page,
	    'total' => $total
	) );

	if ( $paginate_links ) {
	    $output .= '<div class="pagination">';
	    $output .= $paginate_links;
	    $output .= '</div>';
	}
    echo $output;
	die();
}
add_action( 'wp_ajax_wbm_facets', 'wbm_facets' );
add_action( 'wp_ajax_nopriv_wbm_facets', 'wbm_facets' );
