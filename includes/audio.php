<?php

/**
* Used to retrieve the ID of a post based on a URL
* 
* @since 1.0
*
* @param int $url 			the URL of the post to select
*
*/
function get_audio_id($url) {
	global $wpdb;
	$attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $url )); 
        return $attachment[0]; 
}

/**
* Customize the audio shortcode output with custom HTML
* 
* @since 1.0
*
* @param str $html 			input HTML
* @param str $attr 			shortcode attributes
* @param str $content 		content to output
* @param str $instance
*/
// function edit_audio_output( $html, $attr, $content, $instance ) {
// 	$audio = null;
	
// 	$default_types = wp_get_audio_extensions();
// 	$defaults_atts = array(
// 	    'src'      => '',
// 	    'loop'     => '',
// 	    'autoplay' => '',
// 	    'preload'  => 'none'
// 	);
// 	foreach ( $default_types as $type ) {
// 	    $defaults_atts[$type] = '';
// 	}
	
// 	$atts = shortcode_atts( $defaults_atts, $attr, 'audio' );

// 	$audio_url = $atts['mp3'];
// 	$audio_id = get_audio_id($audio_url);
// 	$title = get_the_title($audio_id);

// 	$primary = false;
// 	if ( ! empty( $atts['src'] ) ) {
// 	    $type = wp_check_filetype( $atts['src'], wp_get_mime_types() );
// 	    if ( ! in_array( strtolower( $type['ext'] ), $default_types ) ) {
// 	        return sprintf( '<a class="wp-embedded-audio" href="%s">%s</a>', esc_url( $atts['src'] ), esc_html( $atts['src'] ) );
// 	    }
// 	    $primary = true;
// 	    array_unshift( $default_types, 'src' );
// 	} else {
// 	    foreach ( $default_types as $ext ) {
// 	        if ( ! empty( $atts[ $ext ] ) ) {
// 	            $type = wp_check_filetype( $atts[ $ext ], wp_get_mime_types() );
// 	            if ( strtolower( $type['ext'] ) === $ext ) {
// 	                $primary = true;
// 	            }
// 	        }
// 	    }
// 	}

// 	$library = apply_filters( 'wp_audio_shortcode_library', 'mediaelement' );
// 	if ( 'mediaelement' === $library && did_action( 'init' ) ) {
// 	    wp_enqueue_style( 'wp-mediaelement' );
// 	    wp_enqueue_script( 'wp-mediaelement' );
// 	}
	
// 	$html_atts = array(
// 	    'class'    => apply_filters( 'wp_audio_shortcode_class', 'wp-audio-shortcode' ),
// 	    'id'       => sprintf( 'audio-%d-%d', $post_id, $instance ),
// 	    'loop'     => wp_validate_boolean( $atts['loop'] ),
// 	    'autoplay' => wp_validate_boolean( $atts['autoplay'] ),
// 	    'preload'  => $atts['preload'],
// 	    'style'    => 'width: 100%;',
// 	);
	
// 	// These ones should just be omitted altogether if they are blank
// 	foreach ( array( 'loop', 'autoplay', 'preload' ) as $a ) {
// 	    if ( empty( $html_atts[$a] ) ) {
// 	        unset( $html_atts[$a] );
// 	    }
// 	}
	
// 	$attr_strings = array();
// 	foreach ( $html_atts as $k => $v ) {
// 	    $attr_strings[] = $k . '="' . esc_attr( $v ) . '"';
// 	}

// 	$html = '<div class="wbm-audio-player">';
// 	if ( 'mediaelement' === $library && 1 === $instance ) {
// 	    $html .= "<!--[if lt IE 9]><script>document.createElement('audio');</script><![endif]-->\n";
// 	}
// 	$html .= '<h3>' . $title . '</h3>';

// 	$html .= sprintf( '<audio %s controls="controls">', join( ' ', $attr_strings ) );
	
// 	$fileurl = '';
	
// 	$source = '<source type="%s" src="%s" />';
// 	foreach ( $default_types as $fallback ) {
// 	    if ( ! empty( $atts[ $fallback ] ) ) {
// 	        if ( empty( $fileurl ) ) {
// 	            $fileurl = $atts[ $fallback ];
// 	        }
// 	        $type = wp_check_filetype( $atts[ $fallback ], wp_get_mime_types() );
// 	        $url = add_query_arg( '_', $instance, $atts[ $fallback ] );
// 	        $html .= sprintf( $source, $type['type'], esc_url( $url ) );
// 	    }
// 	}
	
// 	if ( 'mediaelement' === $library ) {
// 	    $html .= wp_mediaelement_fallback( $fileurl );
// 	}
// 	$html .= '</audio>';
// 	$html .= '<a class="download-audio btn btn-default" href="' . $audio_url . '">Download (MP3)</a>';
// 	$html .= '</div>';

// 	return $html;
// }

// add_filter('wp_audio_shortcode_override', 'edit_audio_output', 10, 4);