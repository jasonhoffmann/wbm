<?php
/**
 * The template for displaying all single posts.
 *
 * @package WaterBrook Multnomah
 */

get_header();
$get_book = new RandomHouse;
 ?>
<div class="container bg">

<main class="main grid">
	<?php while( have_posts() ) {  the_post(); ?>


		<section class="index-post entry-item span_9">
		<?php 
		$isbn = get_post_meta(get_the_ID(), 'isbn', true );
		$book = $get_book->book( array( 'id' => $isbn ) ); ?>
		<article class="cf">

		<div class="entry-index-image">
			<?php if ( has_post_thumbnail() ) : ?>
				<?php the_post_thumbnail('large'); ?>
			<?php endif; ?>
		</div>

			<h3 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
			<div class="entry-meta">
				<?php the_time( get_option( 'date_format' ) ); ?>
			</div>
			<?php if($isbn) { ?>
				<div class="entry-book">
					<a href="<?php echo $book->seoFriendlyUrl; ?>"><img src="<?php echo $book->coverImage; ?>"></a>
					<div class="button-box">
						<?php echo generate_buy_button( $isbn ); ?>
					</div>
				</div>
			<?php } ?>
				<?php if($video = get_post_meta($post->ID, "video", true)) {
		         	// This takes all of the old style embed codes in the database and replaces them with the new YouTube embed
		         	if(strpos($video, "youtube.com") !== false) {
		         		preg_match('/(?:\/|embed\/)([0-9a-zA-Z-_]+)(?:[&|?]|\")/', $video, $video);
		         		$video = $video[1];
		         	}
		         ?>
		         <div class="video-container">
		        	<iframe src="http://www.youtube.com/embed/<?= $video ?>" frameborder="0" allowfullscreen></iframe>                                    				
				</div>
				<?php } else { 
					the_content(); } ?>
		</article>

		<?php
			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;
		?>

		</section>

	<?php } ?>

	



</section>

<aside class="index-side span_3">
	<?php get_sidebar(); ?>
</aside>

</main>

</div>


<?php get_footer(); ?>
