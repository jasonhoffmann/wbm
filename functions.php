<?php
/**
 * WaterBrook Multnomah functions and definitions.
 *
 * @link https://codex.wordpress.org/Functions_File_Explained
 *
 * @package WaterBrook Multnomah
 */

if ( ! function_exists( 'wbm_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function wbm_setup() {

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 */
	add_theme_support( 'post-thumbnails' );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	add_image_size('cropped_featured', 850, 250, true);
}
endif; // wbm_setup
add_action( 'after_setup_theme', 'wbm_setup' );


/**
 * Enqueue scripts and styles.
 */
function wbm_scripts() {
	wp_enqueue_style( 'wbm-style', get_stylesheet_uri(), '', '0.0.4' );

	wp_register_script( 'app', get_template_directory_uri() . '/assets/js/scripts.js', array('jquery'), '0.0.4', true );
	wp_localize_script('app', 'WP_VARS', array( 'template_dir' => get_template_directory_uri(), 'ajax_url' => admin_url('admin-ajax.php') ) );

	wp_enqueue_script( 'app');

	if (in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1', '::1'))) {
	  wp_register_script('livereload', 'http://localhost:35729/livereload.js?snipver=1', null, false, true);
	  wp_enqueue_script('livereload');
	}

	wp_dequeue_style('wp-mediaelement');
	wp_deregister_style('wp-mediaelement');
}
add_action( 'wp_enqueue_scripts', 'wbm_scripts' );

/**
 * Register menu shown on blog index page
 */
function register_blog_menu() {
  register_nav_menu('blog-menu',__( 'Blog Page Menu' ));
  register_nav_menu('book-sub-nav',__( 'Books Sub Nav' ));
}
add_action( 'init', 'register_blog_menu' );


/**
 * Add Tealium Tracking to the header
 */
function add_tealium() {
	if ($_SERVER['HTTP_HOST'] == 'dev.us.randomhouse.com'){
		$output = '';
	} else {
		$output = '<script type="text/javascript">var utag_data = {}</script>
		<script type="text/javascript">
	    	(function(a,b,c,d){
	    		a=\'//tags.tiqcdn.com/utag/random/rh-crown/prod/utag.js\';
	    		b=document;c=\'script\';d=b.createElement(c);d.src=a;d.type=\'text/java\'+c;d.async=true;
	    		a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);
	    	})();
		</script>';
	}

	echo $output;
}
add_action('wp_head', 'add_tealium');

add_filter( 'post_thumbnail_html', 'remove_width_attribute', 10 );
function remove_width_attribute( $html ) {
   $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
   return $html;
}

/**
 * Makes sure that the top of the page is shown on Gravity Forms confirmations
 */
add_filter( 'gform_confirmation_anchor', function() {
    return 0;
} );

/**
 * Getting rid of website form field
 */
function alter_comment_form_fields($fields){
    $fields['url'] = '';  //removes website field

    return $fields;
}

add_filter('comment_form_default_fields','alter_comment_form_fields');

function redirect_catalog_pages() {
	$url = $_SERVER['REQUEST_URI'];

	if(strpos($url,'catalog.php?work=')) {
		$catalog_page = $_GET['work'];
		$get_book = new RandomHouse;
		$book = $get_book->book(array( 'id' => $catalog_page) );
		wp_redirect( $book->seoFriendlyUrl, 301 );
		exit;
	}

	if(strpos($url,'catalog.php?isbn=')) {
		$catalog_page = $_GET['isbn'];
		$get_book = new RandomHouse;
		$book = $get_book->book(array( 'id' => $catalog_page) );
		wp_redirect( $book->seoFriendlyUrl, 301 );
		exit;
	}

	if(strpos($url,'author-spotlight.php?authorid=')) {
		$author_id = $_GET['authorid'];
		$get_author = new RandomHouse;
		$author = $get_author->author(array( 'authorID' => $author_id) );
		wp_redirect( $author->seoFriendlyUrl, 301 );
		exit;
	}


}
add_action('template_redirect', 'redirect_catalog_pages', 1);

function wpsites_exclude_latest_post($query) {
if( is_home()) {
	$offset = 5;
		if ( $query->is_paged ) {
			$offset = 5;
			$ppp = get_option('posts_per_page');
		    $offset = $offset + ( ($query->query_vars['paged'] - 1) * $ppp );
		}

		$query->set('offset',$offset);
	}
}
add_action('pre_get_posts', 'wpsites_exclude_latest_post');

function add_newsletter_form() {
	$output = '<form method="POST" name="signup_list" action="http://www.randomhouse.com/cgi-bin/newsletters/index.php" onsubmit="return emvSubmit(this);">
		<input type="hidden" name="EXACTTARGET_EMAIL_CUSTOMER_KEY_FIELD" value="1561">
		<input type="hidden" name="REDIRECT_URI" value="http://waterbrookmultnomah.com/subscribe-thanks/">
		<input type="hidden" id="WATERBROOK_GLOBAL" value="WATERBROOK_GLOBAL" name="RH_REQUEST_SUBCODES[]">
		<fieldset class="newsletter-signup">
			<input type="text" placeholder="Enter Your Email Address" id="EMAIL_FIELD" name="EMAIL_FIELD" value="" size="30" maxlength="64" class="textbox">
			<input class="submit" type="submit" value="Submit Form">
		</fieldset>
	</form>';

	return $output;
}
add_shortcode( 'newsletter-form', 'add_newsletter_form' );

function shortcode_add_to_cart( $atts ) {
	global $wp;
	$current_url = home_url( $wp->request );

	$a = shortcode_atts( array(
		'isbn' => ''
	), $atts );
	$output .= '<form target="_blank" action="https://cart.penguinrandomhouse.com/cart.php" method="post" name="cartform" id="cartform" class="buy_button">
		<input type="hidden" name="cartCount" value="1">
		<input type="hidden" id="siteName" name="siteName" value="waterbrook">
		<input type="hidden" id="referringSite" name="referringSite" value="waterbrook">
		<input type="hidden" id="itemISBN" name="itemISBN" value="' . $a['isbn'] . '">
	    <input type="hidden" id="referringSite" name="referringSite" value="waterbrookmultnomah">
		<input type="hidden" id="siteReturnURL" name="siteReturnURL" value="' . $current_url . '">
		<input type="hidden" id="itemReturnURL" name="itemReturnURL" value="' . $current_url . '">

	    <div class="add-to-cart row">
	    	<p class="span_12">Quantity</p>
	    	<div class="span_4">
	     		<input class= id="itemQuantity" type="text" name="itemQuantity" value="1" size="6">
	     	</div>
	     	<div class="span_7">
	    		<input class="btn" type="submit" name="addToCartSubmit" id="modal_cart_button" class="add_submit" alt="Add to Cart" value="Add To Cart">
	    	</div>
		</div>
	</form>';

	return $output;
}
add_shortcode( 'add-to-cart', 'shortcode_add_to_cart' );

require get_template_directory() . '/includes/lib/htmLawed.php';
require get_template_directory() . '/includes/helpers.php';
require get_template_directory() . '/includes/template.php';

require get_template_directory() . '/includes/lib/routes/Routes.php';

require get_template_directory() . '/includes/route-helpers.php';
require get_template_directory() . '/includes/routes.php';
require get_template_directory() . '/includes/lib/api/prh.api.php';

require get_template_directory() . '/includes/audio.php';

require get_template_directory() . '/includes/ajax.php';

require get_template_directory() . '/includes/lib/preference-center/email.php';

require get_template_directory() . '/includes/redirector.php';
