<?php
/**
 * The template for displaying all single pages
 *
 * @package WaterBrook Multnomah
 */

get_header();
 ?>
 <div class="container bg">

 <main class="main grid">
 	<?php while( have_posts() ) {  the_post(); ?>


 		<section class="index-post span_9 entry-item">
 		<article class="cf">
 			<h3 class="entry-title page-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
 				<?php the_content(); ?>
 		</article>

 		</section>

 	<?php } ?>

 	



 </section>

 <aside class="index-side span_3">
 	<?php get_sidebar(); ?>
 </aside>

 </main>

 </div>


 <?php get_footer(); ?>
